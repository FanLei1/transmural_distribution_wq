from dolfin import *
import sys

class Forms(object):

    	def __init__(self, params):        

		self.parameters = self.default_parameters()
         	self.parameters.update(params)

        def default_parameters(self):
	        return {"bff"  : 29.0,
			"bfx"  : 13.3,
			"bxx"  : 26.6,
			"Kappa": 1e5,
			"incompressible" : True,
			};
	

	def Fmat(self):
	
		u = self.parameters["displacement_variable"]
	    	d = u.ufl_domain().geometric_dimension()
	    	I = Identity(d)
	    	F = I + grad(u)
	    	return F

	def Fe(self):
		Fg = self.parameters["growth_tensor"]
	    	F = self.Fmat() 
		if (Fg is None):
                	Fe = F
        	else:
                	Fe = as_tensor(F[i,j]*inv(Fg)[j,k], (i,k))

		return Fe
	

	
	def Emat(self):
	 
		u = self.parameters["displacement_variable"]
	    	d = u.ufl_domain().geometric_dimension()
	    	I = Identity(d)
	    	#F = self.Fmat() # LCL CHANGE
	    	F = self.Fe()
	    	#return 0.5*(F.T*F-I)
	    	return 0.5*(as_tensor(F[k,i]*F[k,j] - I[i,j], (i,j)))


	def J(self):
	    	#F = self.Fmat() # LCL CHANGE
	    	F = self.Fe()
		return det(F)	
	
	
	def LVcavityvol(self):
	
		u = self.parameters["displacement_variable"]
		N = self.parameters["facet_normal"]
		mesh = self.parameters["mesh"]
	    	X = SpatialCoordinate(mesh)
		ds = dolfin.ds(subdomain_data = self.parameters["facetboundaries"])
	    	
	    	F = self.Fmat()
	    	
	    	vol_form = -Constant(1.0/3.0) * inner(det(F)*dot(inv(F).T, N), X + u)*ds(self.parameters["LVendoid"])
	    	
	    	return assemble(vol_form, form_compiler_parameters={"representation":"uflacs"})

	def RVcavityvol(self):
	
		u = self.parameters["displacement_variable"]
		N = self.parameters["facet_normal"]
		mesh = self.parameters["mesh"]
	    	X = SpatialCoordinate(mesh)
		ds = dolfin.ds(subdomain_data = self.parameters["facetboundaries"])
	    	
	    	F = self.Fmat()
	    	
	    	vol_form = -Constant(1.0/3.0) * inner(det(F)*dot(inv(F).T, N), X + u)*ds(self.parameters["RVendoid"])
	    	
	    	return assemble(vol_form, form_compiler_parameters={"representation":"uflacs"})


        def LVcavitypressure(self):
    
		W = self.parameters["mixedfunctionspace"]
		w = self.parameters["mixedfunction"]
		mesh = self.parameters["mesh"]

		comm = W.mesh().mpi_comm()
    	        dofmap =  W.sub(self.parameters["LVendo_comp"]).dofmap()
        	val_dof = dofmap.cell_dofs(0)[0]

	        # the owner of the dof broadcasts the value
	        own_range = dofmap.ownership_range()
    
	        try:
	            val_local = w.vector()[val_dof][0]
	        except IndexError:
	            val_local = 0.0


    		pressure = MPI.sum(comm, val_local)

        	return pressure



        def RVcavitypressure(self):
    
		W = self.parameters["mixedfunctionspace"]
		w = self.parameters["mixedfunction"]
		mesh = self.parameters["mesh"]

		comm = W.mesh().mpi_comm()
    	        dofmap =  W.sub(self.parameters["RVendo_comp"]).dofmap()
        	val_dof = dofmap.cell_dofs(0)[0]

	        # the owner of the dof broadcasts the value
	        own_range = dofmap.ownership_range()
    
	        try:
	            val_local = w.vector()[val_dof][0]
	        except IndexError:
	            val_local = 0.0


    		pressure = MPI.sum(comm, val_local)

        	return pressure


	def PassiveMatSEF(self):

		Ea = self.Emat()
		f0 = self.parameters["fiber"]
		s0 = self.parameters["sheet"]
		n0 = self.parameters["sheet-normal"]
 		bff = self.parameters["bff"]
 		bfx = self.parameters["bfx"]
 		bxx = self.parameters["bxx"]
		Kappa = self.parameters["Kappa"]
		isincomp = self.parameters["incompressible"]

		if(isincomp):
			p = self.parameters["pressure_variable"]

		C = self.parameters["C_param"]


		Eff = f0[i]*Ea[i,j]*f0[j]
		Ess = s0[i]*Ea[i,j]*s0[j]
		Enn = n0[i]*Ea[i,j]*n0[j]
		Efs = f0[i]*Ea[i,j]*s0[j]
		Efn = f0[i]*Ea[i,j]*n0[j]
		Ens = n0[i]*Ea[i,j]*s0[j]
	
		
		
		#QQ = bff*pow(Eff,2.0) + bfx*(pow(Ess,2.0)+ pow(Enn,2.0)+ 2.0*pow(Ens,2.0)) + bxx*(2.0*pow(Efs,2.0) + 2.0*pow(Efn,2.0))
		QQ = bff*Eff**2.0 + bfx*(Ess**2.0 + Enn**2.0 + 2.0*Ens**2.0) + bxx*(2.0*Efs**2.0 + 2.0*Efn**2.0)

		if(isincomp):
			Wp = C/2.0*(exp(QQ) -  1.0) - p*(self.J() - 1.0)
		else:
			Wp = C/2.0*(exp(QQ) -  1.0) + Kappa/2.0*(self.J() - 1.0)**2.0

		return Wp


	def PassiveMatSEF_J(self):

		F = self.Fmat()
		B = F*F.T
		I1 = tr(B)
		#C = 1.0e2
		C = self.parameters["C_param"]
		
		Kappa = self.parameters["Kappa"]
		isincomp = self.parameters["incompressible"]

		if(isincomp):
			p = self.parameters["pressure_variable"]

		if(isincomp):
			Wp = C/2.0*(I1 -  3.0) - p*(self.J() - 1.0)
		else:
			Wp = C/2.0*(I1 -  3.0) + Kappa/2.0*(self.J() - 1.0)**2.0

		return Wp


	def LVV0constrainedE(self):


		mesh = self.parameters["mesh"]
		u = self.parameters["displacement_variable"]
		ds = dolfin.ds(subdomain_data = self.parameters["facetboundaries"])
		dsendo = ds(self.parameters["LVendoid"], domain = self.parameters["mesh"], subdomain_data = self.parameters["facetboundaries"])
		pendo = self.parameters["lv_volconst_variable"] 
		V0= self.parameters["lv_constrained_vol"] 

	    	X = SpatialCoordinate(mesh)
		x = u + X

	    	F = self.Fmat()
		N = self.parameters["facet_normal"]
        	n = cofac(F)*N

		area = assemble(Constant(1.0) * dsendo, form_compiler_parameters={"representation":"uflacs"})
        	V_u = - Constant(1.0/3.0) * inner(x, n)
		Wvol = (Constant(1.0/area) * pendo  * V0 * dsendo) - (pendo * V_u *dsendo)

		return Wvol


	def RVV0constrainedE(self):


		mesh = self.parameters["mesh"]
		u = self.parameters["displacement_variable"]
		ds = dolfin.ds(subdomain_data = self.parameters["facetboundaries"])
		dsendo = ds(self.parameters["RVendoid"], domain = self.parameters["mesh"], subdomain_data = self.parameters["facetboundaries"])
		pendo = self.parameters["rv_volconst_variable"] 
		V0= self.parameters["rv_constrained_vol"] 

	    	X = SpatialCoordinate(mesh)
		x = u + X

	    	F = self.Fmat()
		N = self.parameters["facet_normal"]
        	n = cofac(F)*N

		area = assemble(Constant(1.0) * dsendo, form_compiler_parameters={"representation":"uflacs"})
        	V_u = - Constant(1.0/3.0) * inner(x, n)
		Wvol = (Constant(1.0/area) * pendo  * V0 * dsendo) - (pendo * V_u *dsendo)

		return Wvol


	# Cauchy stress based on PassiveMatSEF ##############################################################
        def sigma(self):
                J = self.J()
		F = self.Fmat()
                Ea = self.Emat()

		u = self.parameters["displacement_variable"]
		f0 = self.parameters["fiber"]
		s0 = self.parameters["sheet"]
		n0 = self.parameters["sheet-normal"]
 		bff = self.parameters["bff"]
 		bfx = self.parameters["bfx"]
 		bxx = self.parameters["bxx"]
		C = self.parameters["C_param"]
		Kappa = self.parameters["Kappa"]
		isincomp = self.parameters["incompressible"]

		if(isincomp):
			p = self.parameters["pressure_variable"]


		#### For some reason to use dolfin.diff, you need to declare everything starting from u #############################
		d = u.geometric_dimension()
	    	I = Identity(d)
	    	F = I + grad(u)
		J = det(F)

		#Fg = self.parameters["growth_tensor"]
		#Fe = as_tensor(F[i,j]*inv(Fg)[j,k], (i,k))
		#Fe = dolfin.variable(Fe)

		F = self.Fe()

		Ea = 0.5*(as_tensor(Fe[k,i]*Fe[k,j] - I[i,j], (i,j)))

		Eff = f0[i]*Ea[i,j]*f0[j]
		Ess = s0[i]*Ea[i,j]*s0[j]
		Enn = n0[i]*Ea[i,j]*n0[j]
		Efs = f0[i]*Ea[i,j]*s0[j]
		Efn = f0[i]*Ea[i,j]*n0[j]
		Ens = n0[i]*Ea[i,j]*s0[j]
	
		QQ = bff*Eff**2.0 + bfx*(Ess**2.0 + Enn**2.0 + 2.0*Ens**2.0) + bxx*(2.0*Efs**2.0 + 2.0*Efn**2.0)

		if(isincomp):
			Wp = C/2.0*(exp(QQ) -  1.0) - p*(self.J() - 1.0)
		else:
			Wp = C/2.0*(exp(QQ) -  1.0) + Kappa/2.0*(self.J() - 1.0)**2.0
		####################################################################################################################
			

                PK1 = (1.0/J)*dolfin.diff(Wp,Fe)*Fe.T               # Strictly should be defined by d(Wp)/dFe (i.e. derivative using the elastic tensor) but after ALE move Fe should be equal to F (LCL)
		Tca = (1.0/J)*PK1*Fe.T
                return Tca

	def PK1(self):
		u = self.parameters["displacement_variable"]
                #J = self.J()
		#F = self.Fmat()
                #Ea = self.Emat()
		f0 = self.parameters["fiber"]
		s0 = self.parameters["sheet"]
		n0 = self.parameters["sheet-normal"]
 		bff = self.parameters["bff"]
 		bfx = self.parameters["bfx"]
 		bxx = self.parameters["bxx"]
		C = self.parameters["C_param"]
		p = self.parameters["pressure_variable"]
		#### For some reason to use dolfin.diff, you need to declare everything starting from u #############################
		d = u.geometric_dimension()
	    	I = Identity(d)
	    	F = I + grad(u)
		F = dolfin.variable(F)
		J = det(F)
		Ea = 0.5*(as_tensor(F[k,i]*F[k,j] - I[i,j], (i,j)))
		Eff = inner(f0, Ea*f0)
		Ess = inner(s0, Ea*s0)
		Enn = inner(n0, Ea*n0)
		Efs = inner(f0, Ea*s0)
		Efn = inner(f0, Ea*n0)
		Ens = inner(n0, Ea*s0)
		QQ = bff*Eff**2.0 + bxx*(Ess**2.0 + Enn**2.0 + 2.0*Ens**2.0) + bfx*(2.0*Efs**2.0 + 2.0*Efn**2.0)
		Wp = C/2.0*(exp(QQ) -  1.0) - p*(J - 1.0)
                PK1 = dolfin.diff(Wp,F)               
                return PK1

        def PK1a(self):
                u = self.parameters["displacement_variable"]
                #J = self.J()
                #F = self.Fmat()
                #Ea = self.Emat()
                f0 = self.parameters["fiber"]
                s0 = self.parameters["sheet"]
                n0 = self.parameters["sheet-normal"]
                bff = self.parameters["bff"]
                bfx = self.parameters["bfx"]
                bxx = self.parameters["bxx"]
                C = self.parameters["C_param"]
                p = self.parameters["pressure_variable"]
                #### For some reason to use dolfin.diff, you need to declare everything starting from u #############################
                d = u.geometric_dimension()
                I = Identity(d)
                F = I + grad(u)
                F = dolfin.variable(F)
                J = det(F)
                Ea = 0.5*(as_tensor(F[k,i]*F[k,j] - I[i,j], (i,j)))
                Eff = inner(f0, Ea*f0)
                Ess = inner(s0, Ea*s0)
                Enn = inner(n0, Ea*n0)
                Efs = inner(f0, Ea*s0)
                Efn = inner(f0, Ea*n0)
                Ens = inner(n0, Ea*s0)
                QQ = bff*Eff**2.0 + bxx*(Ess**2.0 + Enn**2.0 + 2.0*Ens**2.0) + bfx*(2.0*Efs**2.0 + 2.0*Efn**2.0)
                Wp = C/2.0*(exp(QQ) -  1.0) 
                PK1a = dolfin.diff(Wp,F)
                return PK1a

	def fiberstress(self):

		F = self.Fmat()
		J = self.J()
		PK1 = self.PK1a()

		Tca = (1.0/J)*PK1*F.T
		Sca = inv(F)*PK1

		f0 = self.parameters["fiber"]

		#f = F*f0/sqrt(inner(F*f0, F*f0))
		#return f[i]*Tca[i,j]*f[j]
		return f0[i]*Sca[i,j]*f0[j]

	def fiberstrain(self, F_ref):


		#f0 = self.parameters["fiber"]
		#Emat = self.Emat()

		u = self.parameters["displacement_variable"]
	    	d = u.ufl_domain().geometric_dimension()
	    	I = Identity(d)

                F = self.Fe()*inv(F_ref)
		f0 = self.parameters["fiber"]
		Emat = 0.5*(as_tensor(F[k,i]*F[k,j] - I[i,j], (i,j)))

		return f0[i]*Emat[i,j]*f0[j]


	def fiberwork(self, F_ref):

                F = self.Fe()*inv(F_ref)

		J = self.J()
		PK1 = self.PK1()

		Tca = (1.0/J)*PK1*F.T 
		Sca = inv(F)*PK1

		f0 = self.parameters["fiber"]
		Emat = self.Emat()

		f = f0[i]*Sca[i,j]*f0[j]
		s = f0[i]*Emat[i,j]*f0[j]

		return dot(f,s)


	def IMP(self):
		u = self.parameters["displacement_variable"]
                J = self.J()
		f0 = self.parameters["fiber"]
		s0 = self.parameters["sheet"]
		n0 = self.parameters["sheet-normal"]

		#F = self.Fmat()
		F = self.Fe()
		PK1 = self.PK1()

		Tca = (1.0/J)*PK1*F.T

		s = F*s0/sqrt(inner(F*s0, F*s0))
		n = F*n0/sqrt(inner(F*n0, F*n0))

		Ipressure = s[i]*Tca[i,j]*s[j]

                return Ipressure

	def IMP2(self):
		u = self.parameters["displacement_variable"]
                J = self.J()
		f0 = self.parameters["fiber"]
		s0 = self.parameters["sheet"]
		n0 = self.parameters["sheet-normal"]

		#F = self.Fmat()
		F = self.Fe()
		PK1 = self.PK1()

		Tca = (1.0/J)*PK1*F.T

		s = F*s0/sqrt(inner(F*s0, F*s0))
		n = F*n0/sqrt(inner(F*n0, F*n0))

		Ipressure = 0.5*(s[i]*Tca[i,j]*s[j] + n[i]*Tca[i,j]*n[j])

                return Ipressure

	def IMPendo(self):

		u = self.parameters["displacement_variable"]
		N = self.parameters["facet_normal"]
		F = self.Fe()
		PK1 = self.PK1()

		ds = dolfin.ds(subdomain_data = self.parameters["facetboundaries"])
		n = F*N/sqrt(inner(F*N, F*N))

		Ipressure = -n[i]*PK1[i,j]*N[j]*ds(self.parameters["LVendoid"])

		return Ipressure

	def IMPepi(self):

		u = self.parameters["displacement_variable"]
		N = self.parameters["facet_normal"]
		F = self.Fe()
		PK1 = self.PK1()

		ds = dolfin.ds(subdomain_data = self.parameters["facetboundaries"])
		n = F*N/sqrt(inner(F*N, F*N))

		Ipressure = -n[i]*PK1[i,j]*N[j]*ds(self.parameters["epiid"])

		return Ipressure

	def areaendo(self):
		u = self.parameters["displacement_variable"]
		f0 = self.parameters["fiber"]
		s0 = self.parameters["sheet"]
		n0 = self.parameters["sheet-normal"]


		d = u.geometric_dimension()
	    	I = Identity(d)
	    	F = I + grad(u)
		J = det(F)

		s = F*s0/sqrt(inner(F*s0, F*s0))

		N = self.parameters["facet_normal"]
		ds = dolfin.ds(subdomain_data = self.parameters["facetboundaries"])
		n = F*N/sqrt(inner(F*N, F*N))

		return (J*inv(F.T)*N)[i]*n[i]*ds(self.parameters["endoid"])

	def areaepi(self):
		u = self.parameters["displacement_variable"]
		f0 = self.parameters["fiber"]
		s0 = self.parameters["sheet"]
		n0 = self.parameters["sheet-normal"]


		d = u.geometric_dimension()
	    	I = Identity(d)
	    	F = I + grad(u)
		J = det(F)

		s = F*s0/sqrt(inner(F*s0, F*s0))

		N = self.parameters["facet_normal"]
		ds = dolfin.ds(subdomain_data = self.parameters["facetboundaries"])
		n = F*N/sqrt(inner(F*N, F*N))

		
		return (J*inv(F.T)*N)[i]*n[i]*ds(self.parameters["epiid"])


