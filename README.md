**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

#This repository contains code for cardiac-coronary perfusion coupling model as well as code for post-processing the results. The experimental and clinical data is also included for comparison in baseline case.

---

## instruction to run code

#1. git clone repository to hpcc

#2. follow the instruction in Singularity_fenics.txt to create Singularity image

#3. complile CVODE script 

#4. change the path in case_baseline.py to your local path

#5. run the baseline case case_baseline.py

#6. in sensitivity analysis, change the corresponding parameters following the paper

#7. postprocess results using postprocess.py 
