import sys
sys.path.append("/mnt/home/fanlei1")
from postprocessdatalibA import *

isexpt = True
isparallel = True#False

BCLs = [
	800,\
       ]
cycle = 3

directories = [
	       '../outputs/HFpEF/',\
		]

casenames  = [
	      'baseline_comb11_tv_new1v_emptycav_varPart',\
	     ]


caseno = 0
for casename, directory, BCL,  in zip(casenames, directories, BCLs):

   for ncycle in range(cycle-1,cycle):

	filename = directory+casename+"/"+"BiV_PV.txt"
	homo_tptt, homo_LVP, homo_LVV, homo_Qmv = extract_PV(filename, BCL, ncycle)

	filename = directory+casename+"/"+"BiV_Q.txt"
 	#homo_tpt, homo_Qao, homo_Qmv, homo_Qper, homo_Qla, homo_Qlad = extract_Q(filename, BCL, ncycle)
        homo_tpt, homo_Qlad = extract_Q(filename, BCL, ncycle)
	filename = directory+casename+"/"+"BiV_P.txt"
 	#homo_tpt, homo_Pven, homo_LVPP, homo_Part, homo_PLA = extract_P(filename, BCL, ncycle)
        homo_tpt, homo_Part = extract_P(filename, BCL, ncycle)
	filename = directory+casename+"/"+"BiV_IMP_InC.txt"
	homo_tpt_IMP, homo_IMP = extract_probe(filename, BCL, ncycle)

	filename = directory+casename+"/"+"BiV_fiberStrain.txt"
	homo_tpt_Eff, homo_Eff = extract_probe(filename, BCL, ncycle)

	filename = directory+casename+"/"+"BiV_fiberStress.txt"
	homo_tpt_Sff, homo_Sff = extract_probe(filename, BCL, ncycle)

	ESP, ESV = extractESP(homo_LVP, homo_LVV)

	EDP, EDV = extractEDP(homo_LVP, homo_LVV)

	SBP = max(homo_Part)*0.0075
	DBP = min(homo_Part)*0.0075

	print filename
	print "EF = ", (max(homo_LVV) - min(homo_LVV))/max(homo_LVV), " EDV = ", max(homo_LVV), " ESV = ", min(homo_LVV),\
			" EDP = ", EDP, " SBP = ", SBP, " DBP = ", DBP

	print "Peak LV pressure = ", max(homo_LVP)

	cnt = 0
	Qtotal = []
	for homo_Qlad_ in homo_Qlad:
		if(cnt == 0):
			loc = " Endo"
			linestyle="-"
		elif(cnt == len(homo_Qlad)-1):
			loc = " Epi"
			linestyle="--"
		else:
			loc = " Midwall " + str(cnt+1)
			if(cnt == 1):
				linestyle="-."
			if(cnt == 2):
				linestyle=":"


		Qtotal.append(np.trapz(homo_Qlad_*1000, 1e-3*(homo_tpt - homo_tpt[0]))/(BCL/1000.0))

		print "Total coronary flow ", loc, " = ", np.trapz(homo_Qlad_*1000, 1e-3*(homo_tpt - homo_tpt[0]))/(BCL/1000.0),  "ml/min"
		cnt += 1

	homo_directory = directory+casename+"/"

	tpt_array = readtpt(homo_directory + "tpt.txt")
	ind = np.where((tpt_array>(ncycle-1)*BCL)*(tpt_array<(ncycle)*BCL))
	tpt = tpt_array[ind]
	nbins = 8
	
	## Get Point cloud for probing
	ptcloud, radialpos, vtkradialpos = getpointclouds(homo_directory + "active", "fstress", "fstress", isparallel)
	vtk_py.writeXMLPData(vtkradialpos, casename+".vtp")

	# Get transmural variation of IMP
	imp = probeqty(homo_directory + "IMP_Constraint", "IMP_Constraint", "p_",  ptcloud, isparallel, ind, 100)
	imp = imp*0.0075
	

	## Get transmural variation of WD
	Sff = probetimeseries(homo_directory + "active", "fstress", "fstress", ptcloud, isparallel, ind)
	Eff = probetimeseries(homo_directory + "active", "Eff", "Eff", ptcloud, isparallel, ind)
	WD = np.array([-1.0*np.trapz(Sff[:,i]*0.0075, Eff[:,i]) for i in range(0,len(Sff[1,:]))])


	## Get Ecc
	Ecc = probetimeseries(homo_directory + "active", "Ecc", "Ecc", ptcloud, isparallel, ind)
	peakEcc = np.max(np.abs(np.mean(Ecc, axis=1)*100))
	print "Peak Ecc = ", peakEcc
	
	# Get Ell
	Ell = probetimeseries(homo_directory + "active", "Ell", "Ell", ptcloud, isparallel, ind)
	peakEll = np.max(np.abs(np.mean(Ell, axis=1)*100))
	print "Peak Ell = ", peakEll

	np.savez(casename+".npz", \
		 homo_tptt    = homo_tptt,\
		 homo_LVP     = homo_LVP,\
		 homo_LVV     = homo_LVV,\
		 #homo_Qmv     = homo_Qmv,\
		 homo_tpt     = homo_tpt,\
		 #homo_Qao     = homo_Qao,\
		 #homo_Qper    = homo_Qper,\
		 #homo_Qla     = homo_Qla,\
		 homo_Qlad    = homo_Qlad,\
		 #homo_Pven    = 0.0075*homo_Pven,\
		 #homo_LVPP    = 0.0075*homo_LVPP,\
		 homo_Part    = 0.0075*homo_Part,\
		 #homo_PLA     = 0.0075*homo_PLA,\
	         homo_tpt_IMP = 0.0075*homo_tpt_IMP,\
		 homo_IMP     = homo_IMP,\
		 homo_tpt_Eff = homo_tpt_Eff,\
		 homo_Eff     = homo_Eff,\
	         homo_tpt_Sff = homo_tpt_Sff,\
		 homo_Sff     = homo_Sff,\
		 ESP = ESP, ESV = ESV, EDP = EDP, EDV = EDV, SBP = SBP, DBP = DBP,\
		 Qtotal       = Qtotal,\
		 imp          =imp,\
		 radialpos    =radialpos,\
		 Eff	      =Eff, \
		 Sff	      =Sff, \
                 WD           =WD,\
		 Ecc          =Ecc,\
		 Ell          =Ell,\
		 BCL	      =BCL,\
                 tpt          =tpt,\
		 ncycle       =ncycle
		)



	caseno += 1
	
