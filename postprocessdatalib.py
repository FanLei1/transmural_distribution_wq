import vtk
import vtk_py as vtk_py
import glob
import numpy as np
import csv
import math

def extract_PV(filename, BCL, ncycle):

	reader = csv.reader(open(filename), delimiter=" ")
	tpt_array = []
	LVP_array = []
	LVV_array = []
	Qmv_array = []
	for row in reader:
		tpt_array.append(float(row[0]))
		LVP_array.append(float(row[1]))
		LVV_array.append(float(row[2]))
		try:
			Qmv_array.append(float(row[6]))
		except IndexError:
			Qmv_array.append(0)
	
	
	tpt_array = np.array(tpt_array)
	LVP_array = np.array(LVP_array)
	LVV_array = np.array(LVV_array)
	Qmv_array = np.array(Qmv_array)
	
	ind = np.where(np.logical_and(tpt_array >= ncycle*BCL, tpt_array <= (ncycle+1)*BCL))
	
	return tpt_array[ind], LVP_array[ind], LVV_array[ind], Qmv_array[ind]

def extract_Q(filename, BCL, ncycle):

	reader = csv.reader(open(filename), delimiter=" ")
	tpt_array = []
	Qao_array = []
	Qmv_array = []
	Qper_array = []
	Qla_array = []
	Qlad1_array = []
	Qlad2_array = []
	Qlad3_array = []
	Qlad4_array = []
	for row in reader:
		tpt_array.append(float(row[0]))
		Qao_array.append(float(row[1]))
		Qmv_array.append(float(row[2]))
		Qper_array.append(float(row[3]))
		Qla_array.append(float(row[4]))
		Qlad1_array.append(float(row[5]))
		Qlad2_array.append(float(row[6]))
		Qlad3_array.append(float(row[7]))
		Qlad4_array.append(float(row[8]))
		
	
	tpt_array = np.array(tpt_array)
	Qao_array = np.array(Qao_array)
	Qmv_array = np.array(Qmv_array)
	Qper_array = np.array(Qper_array)
	Qla_array = np.array(Qla_array)
	Qlad1_array = np.array(Qlad1_array)
	Qlad2_array = np.array(Qlad2_array)
	Qlad3_array = np.array(Qlad3_array)
	Qlad4_array = np.array(Qlad4_array)
		
	ind = np.where(np.logical_and(tpt_array >= ncycle*BCL, tpt_array <= (ncycle+1)*BCL))
	
	return tpt_array[ind], Qao_array[ind], Qmv_array[ind], Qper_array[ind], Qla_array[ind], [Qlad1_array[ind], Qlad2_array[ind], Qlad3_array[ind], Qlad4_array[ind]]

def extract_P(filename, BCL, ncycle):

	reader = csv.reader(open(filename), delimiter=" ")
	tpt_array = []
	Pven_array = []
	PLV_array = []
	Part_array = []
	PLA_array = []
	for row in reader:
		tpt_array.append(float(row[0]))
		Pven_array.append(float(row[1]))
		PLV_array.append(float(row[2]))
		Part_array.append(float(row[3]))
		PLA_array.append(float(row[4]))
	
	
	tpt_array = np.array(tpt_array)
	Pven_array = np.array(Pven_array)
	PLV_array = np.array(PLV_array)
	Part_array = np.array(Part_array)
	PLA_array = np.array(PLA_array)
	
	ind = np.where(np.logical_and(tpt_array >= ncycle*BCL, tpt_array <= (ncycle+1)*BCL))
	
	return tpt_array[ind], Pven_array[ind], PLV_array[ind], Part_array[ind], PLA_array[ind]

def extract_probe(filename, BCL, ncycle):

	reader = csv.reader(open(filename), delimiter=" ")
	tpt_array = []
	array_1 = []
	array_2= []
	array_3 = []
	array_4 = []
	for row in reader:
		tpt_array.append(float(row[0]))
		array_1.append(float(row[1]))
		array_2.append(float(row[2]))
		array_3.append(float(row[3]))
		array_4.append(float(row[4]))
	
	
	tpt_array = np.array(tpt_array)
	array_1 = np.array(array_1)
	array_2 = np.array(array_2)
	array_3 = np.array(array_3)
	array_4 = np.array(array_4)
	
	ind = np.where(np.logical_and(tpt_array >= ncycle*BCL, tpt_array <= (ncycle+1)*BCL))
	
	return tpt_array[ind], [array_1[ind], array_2[ind], array_3[ind], array_4[ind]]



def extractESP(LVP, LVV):

	# Get LVV associated with isovolumic relaxation
	ind = np.where(np.logical_and(LVV >= min(LVV)-0.05, LVV < min(LVV)+0.05))
	
	# Get LVP associated with isovolumic relaxation
	isoLVP = LVP[ind]
	
	# Get ind associated with ES
	ESind = ind[0][isoLVP.argmax(axis=0)]

	return LVP[ESind], LVV[ESind]

def extractEDP(LVP, LVV):

	# Get LVV associated with isovolumic contraction
	ind = np.where(np.logical_and(LVV >= max(LVV)-0.05, LVV < max(LVV)+0.05))
	
	# Get LVP associated with isovolumic contraction
	isoLVP = LVP[ind]
	
	# Get ind associated with ED
	EDind = ind[0][isoLVP.argmin(axis=0)]

	return LVP[EDind], LVV[EDind]




def findESPVR(ESP, ESV):

 	pfit = np.polyfit(ESV, ESP, 1)
	ESPVR = np.poly1d(pfit)

	return pfit, ESPVR




def readcsv(filename, ncolstart, ncolend, skip=1):

	reader = csv.reader(open(filename), delimiter=",")
	array = []
	nrow = 0
	for row in reader:
		if(nrow >= skip):
			try:
				array.append([(float(row[p])) for p in range(ncolstart,ncolend+1)])
			except ValueError:
				break;
		nrow += 1
		

	return np.array(array)

def extract_P(filename, BCL, ncycle):

	reader = csv.reader(open(filename), delimiter=" ")
	tpt_array = []
	Pven_array = []
	PLV_array = []
	Part_array = []
	PLA_array = []
	for row in reader:
		tpt_array.append(float(row[0]))
		Pven_array.append(float(row[1]))
		PLV_array.append(float(row[2]))
		Part_array.append(float(row[3]))
		PLA_array.append(float(row[4]))
	
	
	tpt_array = np.array(tpt_array)
	Pven_array = np.array(Pven_array)
	PLV_array = np.array(PLV_array)
	Part_array = np.array(Part_array)
	PLA_array = np.array(PLA_array)
	
	ind = np.where(np.logical_and(tpt_array >= ncycle*BCL, tpt_array <= (ncycle+1)*BCL))
	
	return tpt_array[ind], Pven_array[ind], PLV_array[ind], Part_array[ind], PLA_array[ind]


def getradialposition(ugrid, endo, epi):

  	points = ugrid.GetPoints()

  	endo_ptlocator = vtk.vtkPointLocator()
  	endo_ptlocator.SetDataSet(endo)
  	endo_ptlocator.BuildLocator()

  	epi_ptlocator = vtk.vtkPointLocator()
  	epi_ptlocator.SetDataSet(epi)
  	epi_ptlocator.BuildLocator()

  	radialpos = vtk.vtkFloatArray()
  	radialpos.SetName("radial position")
  	radialpos.SetNumberOfComponents(1)

  	endoids = []
  	epiids = []

  	for ptid in range(0, ugrid.GetNumberOfPoints()):
  	      point = np.array([points.GetPoint(ptid)[k] for k in range(0,3)])
  	      closestendopt = np.array(endo.GetPoints().GetPoint(endo_ptlocator.FindClosestPoint(point)))
  	      closestepipt = np.array(epi.GetPoints().GetPoint(epi_ptlocator.FindClosestPoint(point)))
  	      wallthickness = vtk.vtkMath.Norm(closestepipt - closestendopt)
  	      dist = math.sqrt(vtk.vtkMath.Distance2BetweenPoints(point, closestendopt))
  	      radialpos.InsertNextValue(dist/wallthickness)

  	      if(dist < 0.5):
  	      	endoids.append(ptid)
  	      else:
  	      	epiids.append(ptid)

  	ugrid.GetPointData().AddArray(radialpos)
  	
  	return ugrid, endoids, epiids

def getpointclouds(directory, filebasename, fieldvariable, isparallel):


	filenames = glob.glob(directory + "/" + filebasename + "*")
	filenames.sort()
	filenames = [filename for filename in filenames if filename[-3:] != "pvd"]

	if(filenames[0][-4:] == "pvtu" and isparallel):
		ugrid = vtk_py.readXMLPUGrid(filenames[0])
	
	elif(filenames[0][-4:] == ".vtu" and (not isparallel)):
		ugrid = vtk_py.readXMLUGrid(filenames[0])


	# Merge the subdomain into 1 unstructuredgrid
	merge = vtk.vtkExtractUnstructuredGrid()
	merge.SetInputData(ugrid)
	merge.MergingOn()
	merge.Update()
	ugrid = merge.GetOutput()


	# Generate point cloud
	cx = 0.5*(ugrid.GetBounds()[0] + ugrid.GetBounds()[1])
	cy = 0.5*(ugrid.GetBounds()[2] + ugrid.GetBounds()[3])
	cz = 0.5*(ugrid.GetBounds()[4] + ugrid.GetBounds()[5])
	bds = max([abs(ugrid.GetBounds()[0] - ugrid.GetBounds()[1]),\
	           abs(ugrid.GetBounds()[2] - ugrid.GetBounds()[3]),\
	           abs(ugrid.GetBounds()[4] - ugrid.GetBounds()[5])])

	ptsource = vtk.vtkPointSource()
	ptsource.SetCenter([cx,cy,cz])
	ptsource.SetRadius(bds/2.0*1.2)
	ptsource.SetNumberOfPoints(10000)
	ptsource.Update()

	selectEnclosed = vtk.vtkSelectEnclosedPoints()
	selectEnclosed.SetInputData(ptsource.GetOutput())
	selectEnclosed.SetSurfaceData(vtk_py.convertUGridtoPdata(ugrid))
	selectEnclosed.SetTolerance(1e-9)
	selectEnclosed.Update()

	thresh = vtk.vtkFloatArray()
  	thresh.SetNumberOfComponents(1);
  	thresh.InsertNextValue(0.5);
  	thresh.InsertNextValue(2.0);
  	thresh.SetName("SelectedPoints");

	selectionNode = vtk.vtkSelectionNode()
    	selectionNode.SetFieldType(1) # POINT
    	selectionNode.SetContentType(7) # INDICES
    	selectionNode.SetSelectionList(thresh) # INDICES
    	selection = vtk.vtkSelection()
    	selection.AddNode(selectionNode)

    	extractSelection = vtk.vtkExtractSelection()
    	extractSelection.SetInputData(0, selectEnclosed.GetOutput())
        extractSelection.SetInputData(1, selection)
    	extractSelection.Update()

	points = extractSelection.GetOutput().GetPoints()

	# Get radial position
	probepointpdata = vtk.vtkPolyData()
	probepointpdata.SetPoints(points)

	pdata = vtk_py.convertUGridtoPdata(ugrid)
	ztop = ugrid.GetBounds()[5]
	clippedpdata = vtk_py.clipheart(pdata,[0,0,ztop-1e-5], [0,0,1],1)
	epi, endo = vtk_py.splitDomainBetweenEndoAndEpi(clippedpdata)
	epi = vtk_py.clean_pdata(epi)
        endo = vtk_py.clean_pdata(endo)
        ugrid, endoids, epiids = getradialposition(ugrid, endo, epi)

	radpos = probe(ugrid, probepointpdata)
	radpos_array = [radpos.GetPointData().GetArray("radial position").GetValue(p) for p in range(0, radpos.GetNumberOfPoints())]

	vtk_py.writeXMLPData(radpos, "radialposition.vtp")
	
	return points, radpos_array, radpos
		 
	
def probe(ugrid, pdata):

  	probeFilter = vtk.vtkProbeFilter();
  	probeFilter.SetSourceData(ugrid);
  	if(vtk.vtkVersion.GetVTKMajorVersion <= 5):
  		probeFilter.SetInput(pdata); 
  	else:
  		probeFilter.SetInputData(pdata); 

  	probeFilter.Update()

  	return probeFilter.GetOutput()

def probeqty(directory, filebasename, fieldvariable, points, isparallel, ind, index):

	filenames = glob.glob(directory + "/" + filebasename + "*")
	filenames.sort()
	filenames = [filename for filename in filenames if filename[-3:] != "pvd"]
	
	probepointpdata = vtk.vtkPolyData()
	probepointpdata.SetPoints(points)
	npts = points.GetNumberOfPoints()

	ugrid  = vtk.vtkUnstructuredGrid()
	cnt = 1

	file_ =  filenames[ind[0][index]] 
	 
	if(file_[-4:] == "pvtu" and isparallel):
		ugrid = vtk_py.readXMLPUGrid(file_, verbose=False)
	
	elif(file_[-4:] == ".vtu" and (not isparallel)):
		ugrid = vtk_py.readXMLUGrid(file_, verbose=False)
	
	
	probvar = probe(ugrid,probepointpdata).GetPointData().GetArray(fieldvariable)
	
	point_fieldvararray = [probvar.GetValue(p) for p in range(0, npts)]
	
	cnt += 1

	return np.array(point_fieldvararray)



def probetimeseries(directory, filebasename, fieldvariable, points, isparallel, ind):


	filenames = glob.glob(directory + "/" + filebasename + "*")
	filenames.sort()
	filenames = [filename for filename in filenames if filename[-3:] != "pvd"]
	
	probepointpdata = vtk.vtkPolyData()
	probepointpdata.SetPoints(points)
	npts = points.GetNumberOfPoints()
	
	point_fieldvararray = []

	ugrid  = vtk.vtkUnstructuredGrid()
	cnt = 1
	for file_ in [filenames[p] for p in ind[0]]:
	 
		if(file_[-4:] == "pvtu" and isparallel):
			ugrid = vtk_py.readXMLPUGrid(file_, verbose=False)
	
		elif(file_[-4:] == ".vtu" and (not isparallel)):
			ugrid = vtk_py.readXMLUGrid(file_, verbose=False)
	
		else:
			continue;
	
		probvar = probe(ugrid,probepointpdata).GetPointData().GetArray(fieldvariable)
	
		point_fieldvararray.append([probvar.GetValue(p) for p in range(0, npts)])
		
		cnt += 1

	return np.array(point_fieldvararray)

def readtpt(filename):

	reader = csv.reader(open(filename), delimiter=" ")
	tpt_array = []
	for row in reader:
		tpt_array.append(int(float(row[0])))

	return np.array(tpt_array)





