#ifndef POSTPROCESS_FLOW_H
#define POSTPROCESS_FLOW_H

#include <vtkSmartPointer.h>
#include <vtkVersion.h>

#include <vtkPoints.h>
#include <vtkDoubleArray.h>
#include <vtkPolyData.h>
#include <vtkCleanPolyData.h>
#include <vtkPolyDataWriter.h>
#include <vtkLine.h>
#include <vtkCellArray.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkCellDataToPointData.h>
#include <vtkUnstructuredGrid.h>
#include <vtkUnstructuredGridWriter.h>
#include <vtkTubeFilter.h>
#include <nvector/nvector_serial.h>    /* access to serial N_Vector            */
#include <sunmatrix/sunmatrix_dense.h> /* access to dense SUNMatrix            */
#include "readnetwork.h"


class postprocess_flow
{
  public:
    postprocess_flow();
    ~postprocess_flow();
    void write_grid(const char *outfilename, void *user_data);
};

#endif
