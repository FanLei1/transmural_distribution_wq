/* example.i */
%module readnetwork 
%{
  #define SWIG_FILE_WITH_INIT
  /* Put header files here or function declarations like below */
  #include "/usr/include/stdio.h"
  #include "/usr/include/stdlib.h"
  #include "/usr/include/math.h"
  #include "/usr/include/time.h"
  #include "/usr/local/include/cvode/cvode.h"
  #include "/usr/local/include/nvector/nvector_serial.h"
  #include "/usr/local/include/sunmatrix/sunmatrix_dense.h" /* access to dense SUNMatrix            */
  
  #include "/usr/local/include/sunlinsol/sunlinsol_dense.h" /* access to dense SUNLinearSolver      */
  #include "/usr/local/include/cvode/cvode_direct.h"        /* access to CVDls interface            */
  #include "/usr/local/include/sundials/sundials_types.h"   /* defs. of realtype, sunindextype      */
  
  #include "/usr/local/include/sunmatrix/sunmatrix_sparse.h"
  #include "/usr/local/include/sunmatrix/sunmatrix_dense.h"
  #include "/usr/local/include/cvode/cvode_spils.h"         /* access to CVDspils interface            */
  #include "/usr/local/include/sunlinsol/sunlinsol_spgmr.h"  /* access to GMRES Krylov linear solver    */
  #include "/usr/local/include/sunlinsol/sunlinsol_spbcgs.h" /* access to BiCGS linear solver    */
  
  #include "/usr/include/gsl/gsl_errno.h"             /* access to GSL cubic spline library     */
  #include "/usr/include/gsl/gsl_spline.h"            /* access to GSL cubic spline library     */

  #include "/usr/local/include/sundials/sundials_nvector.h"   /* defs. of realtype, sunindextype      */
  #include "readnetwork.h"
%}
%extend UserData { 
     UserData(){
        UserData * data = (UserData *) malloc(sizeof(UserData));
        return data;
     };
    ~UserData() { 
     free($self); 
   }; 
};
%include "/usr/local/lib/python2.7/dist-packages/instant/swig/numpy.i"
%init
%{
    import_array(); 
%}



%apply int *OUTPUT {int *};
%apply (int** ARGOUTVIEW_ARRAY1, int* DIM1) {(int** outArray1, int* outLen1)}

/*%apply (double* IN_ARRAY1, int DIM1) {(double* array, int array_len)}*/
%include "readnetwork.h"
