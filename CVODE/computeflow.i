/* example.i */
%module computeflow 
%{
  #define SWIG_FILE_WITH_INIT
  /* Put header files here or function declarations like below */
  #include "/usr/local/include/sundials/sundials_nvector.h"   /* defs. of realtype, sunindextype      */
  #include "computeflow.h"
%}
%include "/usr/local/lib/python2.7/dist-packages/instant/swig/numpy.i"

%init
%{
    import_array(); 
%}


%apply (double* IN_ARRAY1, int DIM1) {(double* inArray1, int inLen1)}
%apply (double* IN_ARRAY1, int DIM1) {(double* inArray2, int inLen2)}
%apply (double** ARGOUTVIEW_ARRAY1, int* DIM1) {(double** outArray1, int* outLen1)}
%apply (double** ARGOUTVIEW_ARRAY1, int* DIM1) {(double** outArray2, int* outLen2)}
%include "computeflow.h"
