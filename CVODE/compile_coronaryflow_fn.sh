#!/bin/sh
swig -python readnetwork.i 
g++ -fPIC -O3 -ffast-math -ftree-vectorizer-verbose=2 -march=native -c readnetwork.c
g++ -fPIC -O3 -ffast-math -ftree-vectorizer-verbose=2 -march=native -I/usr/include/python2.7 -c readnetwork_wrap.c   
g++ -O3 -shared -ffast-math -ftree-vectorizer-verbose=2 -march=native \
readnetwork.o \
readnetwork_wrap.o \
-L/usr/lib/x86_64-linux-gnu -lgsl -L/usr/local/lib -lsundials_cvode -lsundials_nvecparallel -lcblas \
-o _readnetwork.so

swig -python computeflow.i 
g++ -fPIC -O3 -ffast-math -ftree-vectorizer-verbose=2 -march=native -c computeflow.c
g++ -fPIC -O3 -ffast-math -ftree-vectorizer-verbose=2 -march=native -w -I/usr/include/python2.7 -c computeflow_wrap.c   
g++ -O3 -shared -ffast-math -ftree-vectorizer-verbose=2 -march=native \
computeflow.o \
computeflow_wrap.o \
-L/usr/lib/x86_64-linux-gnu -lgsl -L/usr/local/lib -lsundials_cvode -lsundials_nvecparallel -lcblas \
-o _computeflow.so


swig -python coronaryflow_asymmetric_fn.i 
g++ -fPIC -O3 -ffast-math -ftree-vectorizer-verbose=2 -march=native -std=c++11 -I/usr/include/python2.7 -c coronaryflow_asymmetric_fn.c  
g++ -fPIC -O3 -ffast-math -ftree-vectorizer-verbose=2 -march=native -std=c++11 -w -I/usr/include/python2.7 -c coronaryflow_asymmetric_fn_wrap.c   
g++ -O3 -shared -ffast-math -ftree-vectorizer-verbose=2 -march=native \
coronaryflow_asymmetric_fn.o \
coronaryflow_asymmetric_fn_wrap.o \
-L/usr/lib/x86_64-linux-gnu -lgsl -L/usr/local/lib -lsundials_cvode -lsundials_nvecparallel -lcblas  -L/usr/local/lib -lvtkCommonCore-8.1 -lvtkCommonDataModel-8.1 -lvtkIOCore-8.1 -lvtkIOLegacy-8.1 -lvtkFiltersCore-8.1 -lvtkCommonExecutionModel-8.1 -lvtkFiltersGeneral-8.1 -o _coronaryflow_asymmetric_fn.so


swig -c++ -python  postprocess_flow.i 
g++ -fPIC -O3 -ffast-math -ftree-vectorizer-verbose=2 -march=native -std=c++11 -I/usr/local/include/vtk-8.1 -c postprocess_flow.cpp  
g++ -fPIC -O3 -ffast-math -ftree-vectorizer-verbose=2 -march=native -std=c++11 -I/usr/include/python2.7 -I/usr/local/include/vtk-8.1 -c postprocess_flow_wrap.cxx
g++ -O3 -shared -ffast-math -ftree-vectorizer-verbose=2 -march=native \
postprocess_flow.o \
postprocess_flow_wrap.o \
-L/usr/lib/x86_64-linux-gnu -lgsl -L/usr/local/lib -lsundials_cvode -lsundials_nvecparallel -lcblas  -L/usr/local/lib -lvtkCommonCore-8.1 -lvtkCommonDataModel-8.1 -lvtkIOCore-8.1 -lvtkIOLegacy-8.1 -lvtkFiltersCore-8.1 -lvtkCommonExecutionModel-8.1 -lvtkFiltersGeneral-8.1 -o _postprocess_flow.so




