#ifndef CORONARYFLOW_ASYMMETRIC_FN_OMP_H
#define CORONARYFLOW_ASYMMETRIC_FN_OMP_H
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include <cvode/cvode.h>               /* prototypes for CVODE fcts., consts.  */
#include <nvector/nvector_openmp.h>    /* added by Ce Xi */
#include <sunmatrix/sunmatrix_dense.h> /* access to dense SUNMatrix            */

#include <sunlinsol/sunlinsol_dense.h> /* access to dense SUNLinearSolver      */
#include <cvode/cvode_direct.h>        /* access to CVDls interface            */
#include <sundials/sundials_types.h>   /* defs. of realtype, sunindextype      */

#include <sunmatrix/sunmatrix_sparse.h>
#include <cvode/cvode_spils.h>         /* access to CVDspils interface            */
#include <sunlinsol/sunlinsol_spgmr.h>  /* access to GMRES Krylov linear solver    */
#include <sunlinsol/sunlinsol_spbcgs.h> /* access to BiCGS linear solver    */

#include <gsl/gsl_errno.h>             /* access to GSL cubic spline library     */
#include <gsl/gsl_spline.h>            /* access to GSL cubic spline library     */
#include "readnetwork_omp.h"

#ifdef _OPENMP
#include <omp.h>                        /* added by Ce Xi */
#endif



void* createODE(UserData *data);
void advanceODE(UserData *data, void* cvode_mem, double tstart, double t1,  double* inArray2, int inLen2, double* pt, int size_pt, double* dptdt, int size_dptdt, double** outArray1, int* outLen1);

#endif 
