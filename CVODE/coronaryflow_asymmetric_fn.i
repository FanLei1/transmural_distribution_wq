/* example.i */
%module coronaryflow_asymmetric_fn
%{
  #define SWIG_FILE_WITH_INIT
  /* Put header files here or function declarations like below */
  #include "coronaryflow_asymmetric_fn.h"
  #include "/usr/include/stdio.h"
  #include "/usr/include/stdlib.h"
  #include "/usr/include/math.h"
  #include "/usr/include/time.h"
  #include "/usr/local/include/cvode/cvode.h"
  #include "/usr/local/include/nvector/nvector_serial.h"
  #include "/usr/local/include/sunmatrix/sunmatrix_dense.h" /* access to dense SUNMatrix            */
  
  #include "/usr/local/include/sunlinsol/sunlinsol_dense.h" /* access to dense SUNLinearSolver      */
  #include "/usr/local/include/cvode/cvode_direct.h"        /* access to CVDls interface            */
  #include "/usr/local/include/sundials/sundials_types.h"   /* defs. of realtype, sunindextype      */
  
  #include "/usr/local/include/sunmatrix/sunmatrix_sparse.h"
  #include "/usr/local/include/cvode/cvode_spils.h"         /* access to CVDspils interface            */
  #include "/usr/local/include/sunlinsol/sunlinsol_spgmr.h"  /* access to GMRES Krylov linear solver    */
  #include "/usr/local/include/sunlinsol/sunlinsol_spbcgs.h" /* access to BiCGS linear solver    */
  
  #include "/usr/include/gsl/gsl_errno.h"             /* access to GSL cubic spline library     */
  #include "/usr/include/gsl/gsl_spline.h"            /* access to GSL cubic spline library     */


%}

%include "/usr/local/lib/python2.7/dist-packages/instant/swig/numpy.i"

%init
%{
    import_array(); 
%}


%apply (double* IN_ARRAY1, int DIM1) {(double* inArray2, int inLen2)}
%apply (double* IN_ARRAY1, int DIM1) {(double* pt, int size_pt)}
%apply (double* IN_ARRAY1, int DIM1) {(double* dptdt, int size_dptdt)}
%apply (double** ARGOUTVIEW_ARRAY1, int* DIM1) {(double** outArray1, int* outLen1)}
%include "coronaryflow_asymmetric_fn.h"
