#include "postprocess_flow.h"

postprocess_flow::postprocess_flow(){
};

postprocess_flow::~postprocess_flow(){
};

void postprocess_flow::write_grid(const char *outfilename, void *user_data){

  //printf("%s", outfilename);

  UserData* data;
  data = (UserData*) user_data;

  double coordtocm = 1e-4;
  double vesselradtocm = 1e-6;

  // Read in nodes and vessels
  realtype **network_matdata;
  realtype *startx, *starty, *startz;
  realtype *endx, *endy, *endz;
  realtype *vesseldia, *vesselord, *d1, *d2, *d3; 
  int nvessels;

  network_matdata = SUNDenseMatrix_Cols(data->network_mat);
  nvessels = SUNDenseMatrix_Rows(data->network_mat);

  startx = SUNDenseMatrix_Column(data->network_mat, 17);
  starty = SUNDenseMatrix_Column(data->network_mat, 18);
  startz = SUNDenseMatrix_Column(data->network_mat, 19);

  endx = SUNDenseMatrix_Column(data->network_mat, 20);
  endy = SUNDenseMatrix_Column(data->network_mat, 21);
  endz = SUNDenseMatrix_Column(data->network_mat, 22);
  
  vesseldia = SUNDenseMatrix_Column(data->network_mat, 2);
  vesselord = SUNDenseMatrix_Column(data->network_mat, 3);
  d1 = SUNDenseMatrix_Column(data->network_mat, 10);
  d2 = SUNDenseMatrix_Column(data->network_mat, 11);
  d3 = SUNDenseMatrix_Column(data->network_mat, 12);

  vtkSmartPointer<vtkPoints> points_ = vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkCellArray> lines_ = vtkSmartPointer<vtkCellArray>::New();
  vtkSmartPointer<vtkLine> line_ = vtkSmartPointer<vtkLine>::New();
  vtkSmartPointer<vtkPolyData> pdata_ = vtkSmartPointer<vtkPolyData>::New();
  vtkSmartPointer<vtkCellDataToPointData> celldatatoptdata_ = vtkSmartPointer<vtkCellDataToPointData>::New();
  vtkSmartPointer<vtkTubeFilter> tubefilter_ = vtkSmartPointer<vtkTubeFilter>::New();
  vtkSmartPointer<vtkCleanPolyData> cleanpdata_ = vtkSmartPointer<vtkCleanPolyData>::New();


  vtkSmartPointer<vtkDoubleArray> vesseldia_ = vtkSmartPointer<vtkDoubleArray>::New();
  vesseldia_->SetName("TubeRadius");
  vesseldia_->SetNumberOfValues(nvessels);

  vtkSmartPointer<vtkIntArray> vesselord_ = vtkSmartPointer<vtkIntArray>::New();
  vesselord_->SetName("Vessel order");
  vesselord_->SetNumberOfValues(nvessels);

  vtkSmartPointer<vtkIntArray> istermvessel_ = vtkSmartPointer<vtkIntArray>::New();
  istermvessel_->SetName("Is terminal vessel");
  istermvessel_->SetNumberOfValues(nvessels);




  for(int i = 0; i < nvessels; ++i){
  	points_->InsertNextPoint(startx[i]*coordtocm, starty[i]*coordtocm, startz[i]*coordtocm);
  	points_->InsertNextPoint(endx[i]*coordtocm, endy[i]*coordtocm, endz[i]*coordtocm);
        line_->GetPointIds()->SetId(0, 2*i);	
        line_->GetPointIds()->SetId(1, 2*i+1);	
	lines_->InsertNextCell(line_);
	if(int(d1[i]) == 0 && int(d2[i]) == 0 && int(d3[i]) == 0){
		istermvessel_->InsertValue(i, 1);
	}else{
		istermvessel_->InsertValue(i, 0);
	};
	vesseldia_->InsertValue(i, vesseldia[i]*vesselradtocm);
	vesselord_->InsertValue(i, int(vesselord[i]));
  };

  pdata_->SetPoints(points_);
  pdata_->SetLines(lines_);
  pdata_->GetCellData()->AddArray(vesseldia_);
  pdata_->GetCellData()->AddArray(vesselord_);
  pdata_->GetCellData()->AddArray(istermvessel_);

  cleanpdata_->SetInputData(pdata_);
  cleanpdata_->Update();

  celldatatoptdata_->SetInputData(cleanpdata_->GetOutput());
  celldatatoptdata_->SetPassCellData(1);
  celldatatoptdata_->Update();

  vtkPolyData* newpdata_ = vtkPolyData::SafeDownCast (celldatatoptdata_->GetOutput());
  newpdata_->GetCellData()->RemoveArray("TubeRadius");
  newpdata_->GetPointData()->SetActiveScalars("TubeRadius");

  tubefilter_->SetInputData(newpdata_);
  tubefilter_->SetNumberOfSides(16);
  //tubefilter_->SetVaryRadiusToVaryRadiusByAbsoluteScalar();
  tubefilter_->SetVaryRadiusToVaryRadiusByScalar();
  tubefilter_->SetRadiusFactor(1.0);
  tubefilter_->CappingOn();
  tubefilter_->Update();

  vtkSmartPointer<vtkPolyDataWriter> pdata_writer_ = vtkSmartPointer<vtkPolyDataWriter>::New();
  pdata_writer_->SetFileName(outfilename);
  //pdata_writer_->SetInputData(tubefilter_->GetOutput());
  pdata_writer_->SetInputData(newpdata_);
  pdata_writer_->Update();
  pdata_writer_->Write();

  //printf("%s", newpdata_->GetCellData()->GetArrayName(0));

  //printf("%d", int(vesseldia_->GetNumberOfValues()));
  //printf("%d", int(vesseldia_->GetNumberOfTuples()));

  //printf("%g %g %g %g %g %g %d", startx[0], starty[0], startz[0], endx[0], endy[0], endz[0], nvessels);

};
