/* -----------------------------------------------------------------
 * Programmer(s): Scott D. Cohen, Alan C. Hindmarsh and
 *                Radu Serban @ LLNL
 * -----------------------------------------------------------------
 * Example problem:
 * 
 * The following is a simple example problem, with the coding
 * needed for its solution by CVODE. The problem is from
 * chemical kinetics, and consists of the following three rate
 * equations:         
 *    dy1/dt = -.04*y1 + 1.e4*y2*y3
 *    dy2/dt = .04*y1 - 1.e4*y2*y3 - 3.e7*(y2)^2
 *    dy3/dt = 3.e7*(y2)^2
 * on the interval from t = 0.0 to t = 4.e10, with initial
 * conditions: y1 = 1.0, y2 = y3 = 0. The problem is stiff.
 * While integrating the system, we also use the rootfinding
 * feature to find the points at which y1 = 1e-4 or at which
 * y3 = 0.01. This program solves the problem with the BDF method,
 * Newton iteration with the SUNDENSE dense linear solver, and a
 * user-supplied Jacobian routine.
 * It uses a scalar relative tolerance and a vector absolute
 * tolerance. Output is printed in decades from t = .4 to t = 4.e10.
 * Run statistics (optional outputs) are printed at the end.
 * -----------------------------------------------------------------*/

#include "coronaryflow_asymmetric_fn_omp.h"
//#include "postprocess_flow.h"


/* User-defined vector and matrix accessor macros: Ith, IJth */

/* These macros are defined in order to write code which exactly matches
   the mathematical problem description given above.

   Ith(v,i) references the ith component of the vector v, where i is in
   the range [1..NEQ] and NEQ is defined below. The Ith macro is defined
   using the N_VIth macro in nvector.h. N_VIth numbers the components of
   a vector starting from 0.

   IJth(A,i,j) references the (i,j)th element of the dense matrix A, where
   i and j are in the range [1..NEQ]. The IJth macro is defined using the
   SM_ELEMENT_D macro in dense.h. SM_ELEMENT_D numbers rows and columns of 
   a dense matrix starting from 0. */

#define Ith(v,i)    NV_Ith_OMP(v,i-1)         /* Ith numbers components 1..NEQ */
#define IJth(A,i,j) SM_ELEMENT_D(A,i-1,j-1) /* IJth numbers rows,cols 1..NEQ */
#define COLS(A)     SM_COLS_D(A)            /* Get Column Data of a Dense Matrix */


/* Problem Constants */

#define Y1     RCONST(1.0)      /* initial y components */
#define Y2     RCONST(0.0)
#define Y3     RCONST(0.0)
#define RTOL   RCONST(1.0e-9)   /* scalar relative tolerance            */
#define ATOL   RCONST(1.0e-6)   /* vector absolute tolerance components */
#define T0     RCONST(0.0)      /* initial time           */
#define T1     RCONST(0.01)     /* first output time      */
#define DT     RCONST(1.00E-3)    /* delta t                */
#define DT1    RCONST(1.00E-3)    /* delta t                */
#define IT     RCONST(0.0)      /* initial time step (0 for default)  */
#define MXSTEP 100000
#define NOUT   10             /* number of output times */
#define PI     RCONST(3.141592653589793)

#define ZERO  RCONST(0.0)


/* Functions Called by the Solver */
static int f(realtype t, N_Vector y, N_Vector ydot, void *user_data);

static int g(realtype t, N_Vector y, realtype *gout, void *user_data);

static int Jacvec(N_Vector v, N_Vector Jy, realtype t,
               N_Vector y, N_Vector fy, void *user_data, N_Vector tmp);

/* Private functions to output results */

static void PrintOutput(realtype t, realtype y1, realtype y2, realtype y3);

/* Private function to print final statistics */

static void PrintFinalStats(void *cvode_mem);

/* Private function to check function return values */
static int check_flag(void *flagvalue, const char *funcname, int opt);

/* Set up ODE for coronary flow analysis */
void* createODE(UserData *data)
{
  realtype reltol, t;
  N_Vector y, abstol;
  SUNLinearSolver LS;
  void *cvode_mem;
  int flag, flagr, iout;
  realtype hcur;

  y = NULL;
  abstol = NULL;
  LS = NULL;
  cvode_mem = NULL;

  ///* Initialize out vector */
  int _outLen1 = data->nvessels;
  double* _outArray1 = (double*)malloc(_outLen1*sizeof(double));


  /* Create openMP vector of length NEQ for I.C. and abstol */
  y = N_VNew_OpenMP(data->nvessels, data->nthreads);  /* Allocate u vector */
  //if (check_flag((void *)y, "N_VNew_Serial", 0)) return(1);
  abstol = N_VNew_OpenMP(data->nvessels, data->nthreads);   
  //if (check_flag((void *)abstol, "N_VNew_Serial", 0)) return(1);

  /* Initialize y and set the vector absolute tolerance */
  int j;
#pragma omp parallel for shared(y, abstol) private(j) num_threads(data->nthreads) schedule(static, 4) 
  for(j = 0; j<(data->nvessels); ++j){
        int tid=omp_get_thread_num();
        int nThreads=omp_get_num_threads();
  	Ith(y,j+1) = 0.0;
  	Ith(abstol,j+1) = ATOL;
  };

  /* Set the scalar relative tolerance */
  reltol = RTOL;
 
  /* Call CVodeCreate to create the solver memory and specify the 
   * Backward Differentiation Formula and the use of a Newton iteration */
  cvode_mem = CVodeCreate(CV_BDF, CV_NEWTON);
  //if (check_flag((void *)cvode_mem, "CVodeCreate", 0)) return(1);

  flag = CVodeSetUserData(cvode_mem, data);
  //if(check_flag(&flag, "CVodeSetUserData", 0)) return(1);

  /* Call CVodeInit to initialize the integrator memory and specify the
   * user's right hand side function in y'=f(t,y), the inital time T0, and
   * the initial dependent variable vector y. */
  flag = CVodeInit(cvode_mem, f, T0, y);
  //if (check_flag(&flag, "CVodeInit", 1)) return(1);

  /* Call CVodeSVtolerances to specify the scalar relative tolerance
   * and vector absolute tolerances */
  flag = CVodeSVtolerances(cvode_mem, reltol, abstol);
  //if (check_flag(&flag, "CVodeSVtolerances", 1)) return(1);

  /* Call CVodeSetMaxNumSteps to specify total number of steps */
  flag = CVodeSetMaxNumSteps(cvode_mem, MXSTEP);
  //if (check_flag(&flag, "CVodeSetMaxNumSteps", 1)) return(1);

  /* Call CVodeSetMaxConvFails to specify miaxmum number of nonlinear solver convergence*/
  flag = CVodeSetMaxConvFails(cvode_mem, 20);
  //if (check_flag(&flag, "CVodeSetMaxConvFails", 1)) return(1);

  /* Turn on stability limit detection */
  flag = CVodeSetStabLimDet(cvode_mem, SUNTRUE);
  //if (check_flag(&flag, "CVodeSetStabLimDet", 1)) return(1);

  /* Set BDF order */
  flag = CVodeSetMaxOrd(cvode_mem, 5);
  //if (check_flag(&flag, "CVodeSetMaxOrd", 1)) return(1);

  /* Call CVodeSetInitStep to specify initial time step */
  flag = CVodeSetInitStep(cvode_mem, IT);
  //if (check_flag(&flag, "CVodeSetMaxNumSteps", 1)) return(1);

  /** USE GMRES ITERATIVE SOLVER WITH MATRIX-FREE *********************************/
  /* Create SUN SPBCGS object for use by CVode */
  LS = SUNSPBCGS(y, PREC_NONE, 0);
  //if(check_flag((void *)LS, "SUNSPBCGS", 0)) return(1);

  /* Call CVDSpilsSetLinearSolver to attach the matrix and linear solver to CVode */
  flag = CVSpilsSetLinearSolver(cvode_mem, LS);
  //if(check_flag(&flag, "CVSpilsSetLinearSolver", 1)) return(1);

  /* Set the user-supplied Jacobian multiply vector Jv (OPTIONAL)*/
  flag = CVSpilsSetJacTimes(cvode_mem, NULL, Jacvec);
  //if(check_flag(&flag, "CVSpilsSetJacTimes", 1)) return(1);

  printf(" \nFinished Setting Coronary flow ODE\n\n");
  N_VDestroy_OpenMP(y);

  return cvode_mem;
};

/* Advance timestep in coronary flow analysis */
void advanceODE(UserData *data, void* cvode_mem, double tstart, double t1,  double* inArray2, int inLen2, double* pt, int size_pt, double* dptdt, int size_dptdt, double** outArray1, int* outLen1)
{
     realtype reltol, t, tout;
     N_Vector y;
     SUNLinearSolver LS;
     int flag;
     realtype hcur;

     tout = t1;

     int _outLen1 = data->nvessels;
     double* _outArray1 = (double*)malloc(_outLen1*sizeof(double));

     y = N_VNew_OpenMP(data->nvessels, data->nthreads);
     /* Initialize y (Manually do it now)*/
     int j;
#pragma omp parallel for shared(y, data) private(j) num_threads(data->nthreads) schedule(static, 4) 
     for(j = 0; j<(data->nvessels); ++j){
     	Ith(y,j+1) = inArray2[j];
     	Ith(data->pt,j+1) = pt[j];
     	Ith(data->dptdt,j+1) = dptdt[j];
     };


     flag = CVodeInit(cvode_mem, f, RCONST(tstart), y);

     ///* Create SUN SPBCGS object for use by CVode */
     LS = SUNSPBCGS(y, PREC_NONE, 0);
     //if(check_flag((void *)LS, "SUNSPBCGS", 0)) return(1);

     /* Call CVDSpilsSetLinearSolver to attach the matrix and linear solver to CVode */
     flag = CVSpilsSetLinearSolver(cvode_mem, LS);

     //if(check_flag(&flag, "CVSpilsSetLinearSolver", 1)) return(1);
     flag = CVSpilsSetJacTimes(cvode_mem, NULL, Jacvec);

     flag = CVode(cvode_mem, tout, y, &t, CV_NORMAL);
     flag = CVodeGetCurrentStep(cvode_mem, &hcur);
     //printf("%0.4e %14.6e  %14.6e  %14.6e  %14.6e %14.6e\n", t, hcur, Ith(y,1), Ith(y,2), Ith(y,150), Ith(y,300));


     /* Print some final statistics */
     //PrintFinalStats(cvode_mem);


     /* Copy y to output */
     int i;
#pragma omp parallel for shared(y, _outArray1) private(i) num_threads(data->nthreads) schedule(static, 4) 
     for(i = 0; i < _outLen1; ++i){
         _outArray1[i] = Ith(y,i+1);
     };

     // Assign outputs
     *outLen1 = _outLen1;
     *outArray1 = _outArray1;

     /* Free y and abstol vectors */
     //N_VDestroy_OpenMP(y);

};





/*
 * f routine. Compute function f(t,y). 
 */


static int f(realtype t, N_Vector y, N_Vector ydot, void *user_data)
{

  /* Extract needed problem constants from data */

  UserData* data;
  data = (UserData*) user_data;

  double Pin = data->pa_const;
  double Pout = data->pv_const;

  //double Pt = data->pt_const;
  //double dPtdt = data->dptdt_const;

  realtype *G_vessels_v, *C_vessels_v, *ydot_v, *y_v;
  realtype *deltaP_v, *Rpt_v, *dRpdP_v;
  realtype *bp_v, *ap_v, *cp_v, *php_v, *vessel_length_v;

  realtype *pt_v, *dptdt_v;

  realtype **network_matdata;
  network_matdata = SUNDenseMatrix_Cols(data->network_mat);

  G_vessels_v = NV_DATA_OMP(data->G_vessels);
  C_vessels_v = NV_DATA_OMP(data->C_vessels); 
  deltaP_v = NV_DATA_OMP(data->deltaP);

  Rpt_v = NV_DATA_OMP(data->Rpt); 
  dRpdP_v = NV_DATA_OMP(data->dRpdP); 

  ydot_v = NV_DATA_OMP(ydot); 
  y_v = NV_DATA_OMP(y); 

  bp_v = NV_DATA_OMP(data->long_dist_bp);
  ap_v = NV_DATA_OMP(data->long_dist_ap);
  cp_v = NV_DATA_OMP(data->long_dist_cp);
  php_v = NV_DATA_OMP(data->long_dist_php);
  vessel_length_v = NV_DATA_OMP(data->vessel_length);

  pt_v = NV_DATA_OMP(data->pt);
  dptdt_v  = NV_DATA_OMP(data->dptdt);

  N_VConst(0.0, ydot);

  int i;
#pragma omp parallel for shared(deltaP_v,y_v,network_matdata, pt_v) private(i) num_threads(data->nthreads) schedule(static, 4)  
  for(i = 0; i < data->nvessels; ++i){
	//deltaP_v[i] = y_v[i] - Pt*network_matdata[16][i]; 
	deltaP_v[i] = y_v[i] - pt_v[i]*network_matdata[16][i]; 
  };

#pragma omp parallel for shared(deltaP_v,Rpt_v,dRpdP_v,bp_v,ap_v,cp_v,php_v) private(i) num_threads(data->nthreads) schedule(static, 4)  
  for(i = 0; i < data->nvessels; ++i){
  	/* Compute Radius */
	Rpt_v[i] = bp_v[i] + (ap_v[i] - bp_v[i])/PI*(PI/2.0 + atan((deltaP_v[i] - php_v[i])/cp_v[i]));
        //printf("Rpt_v[%d] = %lg\n", i, Rpt_v[i]);

	dRpdP_v[i] = (ap_v[i] - bp_v[i])/(PI*cp_v[i]*(1.0 + pow((deltaP_v[i] - php_v[i])/cp_v[i], 2.0)));
        //printf("dRpdP[%d] = %lg\n", i, dRpdP_v[i]);

  };

#pragma omp parallel for shared(Rpt_v,dRpdP_v,vessel_length_v,C_vessels_v,G_vessels_v,dptdt_v) private(i) num_threads(data->nthreads) schedule(static, 4)  
  for(i = 0; i < data->nvessels; ++i){
        /* Compute hydraulic conductance at each time point */
	G_vessels_v[i] = PI/(8.0*data->viscosity*1.0e-3)*(Rpt_v[i]*Rpt_v[i]*Rpt_v[i]*Rpt_v[i])/vessel_length_v[i];
        //printf("G_vessels[%d] = %lg\n", i, G_vessels_v[i]);

        /* Compute capacitance at each time point */
	C_vessels_v[i] = PI*2.0*1.0e-3*Rpt_v[i]*dRpdP_v[i]*vessel_length_v[i];
        //printf("C_vessels[%d] = %lg\n", i, C_vessels_v[i]);

	//ydot_v[i] = dPtdt*network_matdata[16][i] ;
	ydot_v[i] = dptdt_v[i]*network_matdata[16][i] ;
  };




  double matvalue;
  double matval1, matval2, matval3, matval4, matval5, matval6;
  double work1, work2;

  work1 = (G_vessels_v[0] + G_vessels_v[int(network_matdata[11][0])-1] + G_vessels_v[int(network_matdata[12][0])-1]);

  matvalue = (-4.0*G_vessels_v[0] + 2.0*pow(G_vessels_v[0],2)/work1);
  matval1 = matvalue*y_v[0] ;

  matvalue = (2.0*G_vessels_v[0]*G_vessels_v[int(network_matdata[11][0])-1]/work1);
  matval2 = matvalue*y_v[int(network_matdata[11][0])-1] ;

  matvalue = (RCONST(2.0)*G_vessels_v[0]*G_vessels_v[int(network_matdata[12][0])-1]/work1);
  matval3 = matvalue*y_v[int(network_matdata[12][0])-1] ;

  matvalue = RCONST(2.0)*G_vessels_v[0]*Pin/C_vessels_v[0];

  ydot_v[0] += matvalue + (matval1 + matval2 + matval3)/C_vessels_v[0];



  int k;
#pragma omp parallel for shared(G_vessels_v,C_vessels_v,ydot_v,y_v, network_matdata, data) private(k,matvalue,work1,matval1,matval2,matval3) num_threads(data->nthreads) schedule(static, 4)   
  for (k = 0; k < data->ind1_cnt; ++k) {

        work1 = (G_vessels_v[data->ind1[k]] + G_vessels_v[int(network_matdata[6][data->ind1[k]])-1] + G_vessels_v[int(network_matdata[7][data->ind1[k]])-1]);

        matvalue = (-4.0*G_vessels_v[data->ind1[k]] + 2.0*pow(G_vessels_v[data->ind1[k]],2)/work1);
        matval1 = matvalue*y_v[data->ind1[k]] ;

        matvalue = (2.0*G_vessels_v[data->ind1[k]]*G_vessels_v[int(network_matdata[6][data->ind1[k]])-1]/work1);
        matval2 = matvalue*y_v[int(network_matdata[6][data->ind1[k]])-1]; 

        matvalue = (2.0*G_vessels_v[data->ind1[k]]*G_vessels_v[int(network_matdata[7][data->ind1[k]])-1]/work1);
        matval3 = matvalue*y_v[int(network_matdata[7][data->ind1[k]])-1]; 

        //matvalue = dPtdt*network_matdata[16][data->ind1[k]] + 2.0*G_vessels_v[data->ind1[k]]*Pout*network_matdata[16][data->ind1[k]]/C_vessels_v[data->ind1[k]];
        matvalue =  2.0*G_vessels_v[data->ind1[k]]*Pout*network_matdata[16][data->ind1[k]]/C_vessels_v[data->ind1[k]];
  	ydot_v[data->ind1[k]] += matvalue + (matval1 + matval2 + matval3)/C_vessels_v[data->ind1[k]];

   };


#pragma omp parallel for shared(G_vessels_v,C_vessels_v,ydot_v,y_v, network_matdata, data) private(k,matvalue,work1,matval1,matval2,matval3,matval4) num_threads(data->nthreads) schedule(static, 4)   
  for (k = 0; k < data->ind2_cnt; ++k) {

         work1 = (G_vessels_v[data->ind2[k]] + G_vessels_v[int(network_matdata[6][data->ind2[k]])-1] + G_vessels_v[int(network_matdata[7][data->ind2[k]])-1]+
            		      G_vessels_v[int(network_matdata[8][data->ind2[k]])-1]);

         matvalue = (-4.0*G_vessels_v[data->ind2[k]] + 2.0*pow(G_vessels_v[data->ind2[k]],2)/work1);
         matval1 = matvalue*y_v[data->ind2[k]] ;

         matvalue = (2.0*G_vessels_v[data->ind2[k]]*G_vessels_v[int(network_matdata[6][data->ind2[k]])-1]/work1);
         matval2 = matvalue*y_v[int(network_matdata[6][data->ind2[k]])-1] ;

         matvalue = (2.0*G_vessels_v[data->ind2[k]]*G_vessels_v[int(network_matdata[7][data->ind2[k]])-1]/work1);
         matval3 = matvalue*y_v[int(network_matdata[7][data->ind2[k]])-1] ;
 
         matvalue = (2.0*G_vessels_v[data->ind2[k]]*G_vessels_v[int(network_matdata[8][data->ind2[k]])-1]/work1);
         matval4 = matvalue*y_v[int(network_matdata[8][data->ind2[k]])-1] ;
 
  	 //matvalue = dPtdt*network_matdata[16][data->ind2[k]] + 2.0*G_vessels_v[data->ind2[k]]*Pout*network_matdata[16][data->ind2[k]]/C_vessels_v[data->ind2[k]];
  	 matvalue =  2.0*G_vessels_v[data->ind2[k]]*Pout*network_matdata[16][data->ind2[k]]/C_vessels_v[data->ind2[k]];
         ydot_v[data->ind2[k]] += matvalue + (matval1 + matval2 + matval3 + matval4)/C_vessels_v[data->ind2[k]];



   };

#pragma omp parallel for shared(G_vessels_v,C_vessels_v,ydot_v,y_v, network_matdata, data) private(k,matvalue,work1,matval1,matval2) num_threads(data->nthreads) schedule(static, 4)   
  for (k = 0; k < data->ind3_cnt; ++k) {

         work1 = (G_vessels_v[data->ind3[k]] + G_vessels_v[int(network_matdata[6][data->ind3[k]])-1]);

         matvalue = (-4.0*G_vessels_v[data->ind3[k]] + 2.0*pow(G_vessels_v[data->ind3[k]],2)/work1);
         matval1 = matvalue*y_v[data->ind3[k]];
  
         matvalue = (2.0*G_vessels_v[data->ind3[k]]*G_vessels_v[int(network_matdata[6][data->ind3[k]])-1]/work1);
         matval2 = matvalue*y_v[int(network_matdata[6][data->ind3[k]])-1]; 
 
         //matvalue = dPtdt*network_matdata[16][data->ind3[k]]  + 2.0*G_vessels_v[data->ind3[k]]*Pout*network_matdata[16][data->ind3[k]]/C_vessels_v[data->ind3[k]];
         matvalue = 2.0*G_vessels_v[data->ind3[k]]*Pout*network_matdata[16][data->ind3[k]]/C_vessels_v[data->ind3[k]];
  	 ydot_v[data->ind3[k]] += matvalue + (matval1 + matval2)/C_vessels_v[data->ind3[k]];

   };

#pragma omp parallel for shared(G_vessels_v,C_vessels_v,ydot_v,y_v, network_matdata, data) private(k,matvalue,work1,work2,matval1,matval2,matval3,matval4,matval5,matval6) num_threads(data->nthreads) schedule(static, 4)   
  for (k = 0; k < data->ind8_cnt; ++k) {

         work1 = (G_vessels_v[data->ind8[k]] + G_vessels_v[int(network_matdata[6][data->ind8[k]])-1] + G_vessels_v[int(network_matdata[7][data->ind8[k]])-1]);
         work2 = (G_vessels_v[data->ind8[k]] + G_vessels_v[int(network_matdata[11][data->ind8[k]])-1] + G_vessels_v[int(network_matdata[12][data->ind8[k]])-1] 
                  + G_vessels_v[int(network_matdata[13][data->ind8[k]])-1]);

         matvalue = (-4.0*G_vessels_v[data->ind8[k]] + 2.0*pow(G_vessels_v[data->ind8[k]],2)*(1.0/work1 + 1.0/work2));
         matval1 = matvalue*y_v[data->ind8[k]];

         matvalue = (2.0*G_vessels_v[data->ind8[k]]*G_vessels_v[int(network_matdata[6][data->ind8[k]])-1]/work1);
         matval2 = matvalue*y_v[int(network_matdata[6][data->ind8[k]])-1]; 

         matvalue = (2.0*G_vessels_v[data->ind8[k]]*G_vessels_v[int(network_matdata[7][data->ind8[k]])-1]/work1);
         matval3 = matvalue*y_v[int(network_matdata[7][data->ind8[k]])-1]; 

         matvalue = (2.0*G_vessels_v[data->ind8[k]]*G_vessels_v[int(network_matdata[11][data->ind8[k]])-1]/work2);
         matval4 = matvalue*y_v[int(network_matdata[11][data->ind8[k]])-1]; 

         matvalue = (2.0*G_vessels_v[data->ind8[k]]*G_vessels_v[int(network_matdata[12][data->ind8[k]])-1]/work2);
         matval5 = matvalue*y_v[int(network_matdata[12][data->ind8[k]])-1]; 

         matvalue = (2.0*G_vessels_v[data->ind8[k]]*G_vessels_v[int(network_matdata[13][data->ind8[k]])-1]/work2);
         matval6 = matvalue*y_v[int(network_matdata[13][data->ind8[k]])-1]; 
        
         ydot_v[data->ind8[k]] += (matval1 + matval2 + matval3 + matval4 + matval5 + matval6)/C_vessels_v[data->ind8[k]];

   };

#pragma omp parallel for shared(G_vessels_v,C_vessels_v,ydot_v,y_v, network_matdata, data) private(k,matvalue,work1,work2,matval1,matval2,matval3,matval4,matval5,matval6) num_threads(data->nthreads) schedule(static, 4)   
  for (k = 0; k < data->ind9_cnt; ++k) {

         work1  = (G_vessels_v[data->ind9[k]] + G_vessels_v[int(network_matdata[6][data->ind9[k]])-1] + G_vessels_v[int(network_matdata[7][data->ind9[k]])-1] 
                  + G_vessels_v[int(network_matdata[8][data->ind9[k]])-1]);
         work2 =  (G_vessels_v[data->ind9[k]] + G_vessels_v[int(network_matdata[11][data->ind9[k]])-1] + G_vessels_v[int(network_matdata[12][data->ind9[k]])-1]);

         matvalue = (-4.0*G_vessels_v[data->ind9[k]] + 2.0*pow(G_vessels_v[data->ind9[k]],2)*(1.0/work1 + 1.0/work2));
         matval1 = matvalue*y_v[data->ind9[k]];

         matvalue = (2.0*G_vessels_v[data->ind9[k]]*G_vessels_v[int(network_matdata[6][data->ind9[k]])-1]/work1);
         matval2 = matvalue*y_v[int(network_matdata[6][data->ind9[k]])-1];

         matvalue = (2.0*G_vessels_v[data->ind9[k]]*G_vessels_v[int(network_matdata[7][data->ind9[k]])-1]/work1);
         matval3 = matvalue*y_v[int(network_matdata[7][data->ind9[k]])-1];

         matvalue = (2.0*G_vessels_v[data->ind9[k]]*G_vessels_v[int(network_matdata[8][data->ind9[k]])-1]/work1);
         matval4 = matvalue*y_v[int(network_matdata[8][data->ind9[k]])-1];

         matvalue = (2.0*G_vessels_v[data->ind9[k]]*G_vessels_v[int(network_matdata[11][data->ind9[k]])-1]/work2);
         matval5 = matvalue*y_v[int(network_matdata[11][data->ind9[k]])-1];

         matvalue = (2.0*G_vessels_v[data->ind9[k]]*G_vessels_v[int(network_matdata[12][data->ind9[k]])-1]/work2);
         matval6 = matvalue*y_v[int(network_matdata[12][data->ind9[k]])-1];

         ydot_v[data->ind9[k]] += (matval1 + matval2 + matval3 + matval4 + matval5 + matval6)/C_vessels_v[data->ind9[k]];
  };


#pragma omp parallel for shared(G_vessels_v,C_vessels_v,ydot_v,y_v, network_matdata, data) private(k,matvalue,work1,work2,matval1,matval2,matval3,matval4,matval5) num_threads(data->nthreads) schedule(static, 4)   
  for (k = 0; k < data->ind10_cnt; ++k) {
             
         work1 = (G_vessels_v[data->ind10[k]] + G_vessels_v[int(network_matdata[6][data->ind10[k]])-1] + G_vessels_v[int(network_matdata[7][data->ind10[k]])-1]);
         work2 = (G_vessels_v[data->ind10[k]] + G_vessels_v[int(network_matdata[11][data->ind10[k]])-1] + G_vessels_v[int(network_matdata[12][data->ind10[k]])-1]);
        
         matvalue = (-4.0*G_vessels_v[data->ind10[k]] + 2.0*pow(G_vessels_v[data->ind10[k]],2)*(1.0/work1 + 1.0/work2));
         matval1 = matvalue*y_v[data->ind10[k]];

         matvalue = (2.0*G_vessels_v[data->ind10[k]]*G_vessels_v[int(network_matdata[6][data->ind10[k]])-1]/work1);
         matval2 = matvalue*y_v[int(network_matdata[6][data->ind10[k]])-1];

         matvalue = (2.0*G_vessels_v[data->ind10[k]]*G_vessels_v[int(network_matdata[7][data->ind10[k]])-1]/work1);
         matval3 = matvalue*y_v[int(network_matdata[7][data->ind10[k]])-1];

         matvalue = (2.0*G_vessels_v[data->ind10[k]]*G_vessels_v[int(network_matdata[11][data->ind10[k]])-1]/work2);
         matval4 = matvalue*y_v[int(network_matdata[11][data->ind10[k]])-1];

         matvalue = (2.0*G_vessels_v[data->ind10[k]]*G_vessels_v[int(network_matdata[12][data->ind10[k]])-1]/work2);
         matval5 = matvalue*y_v[int(network_matdata[12][data->ind10[k]])-1];

         ydot_v[data->ind10[k]] += (matval1 + matval2 + matval3 + matval4 + matval5)/C_vessels_v[data->ind10[k]];


   };
  return(0);
}


/*
 * Jacobian routine. Compute J(t,y)*y = df/dy*y. *
 */


static int Jacvec(N_Vector v, N_Vector Jy, realtype t,
               N_Vector y, N_Vector fy, void *user_data, N_Vector tmp)
{

  UserData* data;
  data = (UserData*) user_data;

  //double Pt = data->pt_const;

  realtype *G_vessels_v, *C_vessels_v, *ydot_v, *y_v;
  realtype *deltaP_v, *Rpt_v, *dRpdP_v, *v_v, *Jy_v; 
  realtype *bp_v, *ap_v, *cp_v, *php_v, *vessel_length_v;

  realtype **network_matdata;
  network_matdata = SUNDenseMatrix_Cols(data->network_mat);

  G_vessels_v = NV_DATA_OMP(data->G_vessels);
  C_vessels_v = NV_DATA_OMP(data->C_vessels); 
  deltaP_v = NV_DATA_OMP(data->deltaP);

  Rpt_v = NV_DATA_OMP(data->Rpt); 
  dRpdP_v = NV_DATA_OMP(data->dRpdP); 

  Jy_v = NV_DATA_OMP(Jy); 
  y_v = NV_DATA_OMP(y); 
  v_v = NV_DATA_OMP(v); 

  bp_v = NV_DATA_OMP(data->long_dist_bp);
  ap_v = NV_DATA_OMP(data->long_dist_ap);
  cp_v = NV_DATA_OMP(data->long_dist_cp);
  php_v = NV_DATA_OMP(data->long_dist_php);
  vessel_length_v = NV_DATA_OMP(data->vessel_length);


  N_VConst(0.0, Jy);

  double matvalue;
  double matval1, matval2, matval3, matval4, matval5, matval6;
  double work1, work2;

  work1 = (G_vessels_v[0] + G_vessels_v[int(network_matdata[11][0])-1] + G_vessels_v[int(network_matdata[12][0])-1]);

  matvalue = (-4.0*G_vessels_v[0] + 2.0*pow(G_vessels_v[0],2)/work1);
  matval1 = matvalue*v_v[0] ;

  matvalue = (2.0*G_vessels_v[0]*G_vessels_v[int(network_matdata[11][0])-1]/work1);
  matval2 = matvalue*v_v[int(network_matdata[11][0])-1] ;

  matvalue = (RCONST(2.0)*G_vessels_v[0]*G_vessels_v[int(network_matdata[12][0])-1]/work1);
  matval3 = matvalue*v_v[int(network_matdata[12][0])-1] ;

  Jy_v[0] = (matval1 + matval2 + matval3)/C_vessels_v[0];


  int k;
#pragma omp parallel for shared(G_vessels_v,C_vessels_v,Jy_v,v_v, network_matdata, data) private(k,matvalue,work1,matval1,matval2,matval3) num_threads(data->nthreads) schedule(static, 4)   
  for (k = 0; k < data->ind1_cnt; ++k) {

        work1 = (G_vessels_v[data->ind1[k]] + G_vessels_v[int(network_matdata[6][data->ind1[k]])-1] + G_vessels_v[int(network_matdata[7][data->ind1[k]])-1]);

        matvalue = (-4.0*G_vessels_v[data->ind1[k]] + 2.0*pow(G_vessels_v[data->ind1[k]],2)/work1);
        matval1 = matvalue*v_v[data->ind1[k]] ;

        matvalue = (2.0*G_vessels_v[data->ind1[k]]*G_vessels_v[int(network_matdata[6][data->ind1[k]])-1]/work1);
        matval2 = matvalue*v_v[int(network_matdata[6][data->ind1[k]])-1]; 

        matvalue = (2.0*G_vessels_v[data->ind1[k]]*G_vessels_v[int(network_matdata[7][data->ind1[k]])-1]/work1);
        matval3 = matvalue*v_v[int(network_matdata[7][data->ind1[k]])-1]; 

  	Jy_v[data->ind1[k]] = (matval1 + matval2 + matval3)/C_vessels_v[data->ind1[k]];

   };


#pragma omp parallel for shared(G_vessels_v,C_vessels_v,Jy_v,v_v, network_matdata, data) private(k,matvalue,work1,matval1,matval2,matval3,matval4) num_threads(data->nthreads) schedule(static, 4)   
  for (k = 0; k < data->ind2_cnt; ++k) {

         work1 = (G_vessels_v[data->ind2[k]] + G_vessels_v[int(network_matdata[6][data->ind2[k]])-1] + G_vessels_v[int(network_matdata[7][data->ind2[k]])-1]+
            		      G_vessels_v[int(network_matdata[8][data->ind2[k]])-1]);

         matvalue = (-4.0*G_vessels_v[data->ind2[k]] + 2.0*pow(G_vessels_v[data->ind2[k]],2)/work1);
         matval1 = matvalue*v_v[data->ind2[k]] ;

         matvalue = (2.0*G_vessels_v[data->ind2[k]]*G_vessels_v[int(network_matdata[6][data->ind2[k]])-1]/work1);
         matval2 = matvalue*v_v[int(network_matdata[6][data->ind2[k]])-1] ;

         matvalue = (2.0*G_vessels_v[data->ind2[k]]*G_vessels_v[int(network_matdata[7][data->ind2[k]])-1]/work1);
         matval3 = matvalue*v_v[int(network_matdata[7][data->ind2[k]])-1] ;
 
         matvalue = (2.0*G_vessels_v[data->ind2[k]]*G_vessels_v[int(network_matdata[8][data->ind2[k]])-1]/work1);
         matval4 = matvalue*v_v[int(network_matdata[8][data->ind2[k]])-1] ;
 
         Jy_v[data->ind2[k]] = (matval1 + matval2 + matval3 + matval4)/C_vessels_v[data->ind2[k]];



   };

#pragma omp parallel for shared(G_vessels_v,C_vessels_v,Jy_v,v_v, network_matdata, data) private(k,matvalue,work1,matval1,matval2) num_threads(data->nthreads) schedule(static, 4)   
  for (k = 0; k < data->ind3_cnt; ++k) {

         work1 = (G_vessels_v[data->ind3[k]] + G_vessels_v[int(network_matdata[6][data->ind3[k]])-1]);

         matvalue = (-4.0*G_vessels_v[data->ind3[k]] + 2.0*pow(G_vessels_v[data->ind3[k]],2)/work1);
         matval1 = matvalue*v_v[data->ind3[k]];
  
         matvalue = (2.0*G_vessels_v[data->ind3[k]]*G_vessels_v[int(network_matdata[6][data->ind3[k]])-1]/work1);
         matval2 = matvalue*v_v[int(network_matdata[6][data->ind3[k]])-1]; 
 
  	 Jy_v[data->ind3[k]] = (matval1 + matval2)/C_vessels_v[data->ind3[k]];

   };

#pragma omp parallel for shared(G_vessels_v,C_vessels_v,Jy_v,v_v, network_matdata, data) private(k,matvalue,work1,work2,matval1,matval2,matval3,matval4,matval5,matval6) num_threads(data->nthreads) schedule(static, 4)   
  for (k = 0; k < data->ind8_cnt; ++k) {

         work1 = (G_vessels_v[data->ind8[k]] + G_vessels_v[int(network_matdata[6][data->ind8[k]])-1] + G_vessels_v[int(network_matdata[7][data->ind8[k]])-1]);
         work2 = (G_vessels_v[data->ind8[k]] + G_vessels_v[int(network_matdata[11][data->ind8[k]])-1] + G_vessels_v[int(network_matdata[12][data->ind8[k]])-1] 
                  + G_vessels_v[int(network_matdata[13][data->ind8[k]])-1]);

         matvalue = (-4.0*G_vessels_v[data->ind8[k]] + 2.0*pow(G_vessels_v[data->ind8[k]],2)*(1.0/work1 + 1.0/work2));
         matval1 = matvalue*v_v[data->ind8[k]];

         matvalue = (2.0*G_vessels_v[data->ind8[k]]*G_vessels_v[int(network_matdata[6][data->ind8[k]])-1]/work1);
         matval2 = matvalue*v_v[int(network_matdata[6][data->ind8[k]])-1]; 

         matvalue = (2.0*G_vessels_v[data->ind8[k]]*G_vessels_v[int(network_matdata[7][data->ind8[k]])-1]/work1);
         matval3 = matvalue*v_v[int(network_matdata[7][data->ind8[k]])-1]; 

         matvalue = (2.0*G_vessels_v[data->ind8[k]]*G_vessels_v[int(network_matdata[11][data->ind8[k]])-1]/work2);
         matval4 = matvalue*v_v[int(network_matdata[11][data->ind8[k]])-1]; 

         matvalue = (2.0*G_vessels_v[data->ind8[k]]*G_vessels_v[int(network_matdata[12][data->ind8[k]])-1]/work2);
         matval5 = matvalue*v_v[int(network_matdata[12][data->ind8[k]])-1]; 

         matvalue = (2.0*G_vessels_v[data->ind8[k]]*G_vessels_v[int(network_matdata[13][data->ind8[k]])-1]/work2);
         matval6 = matvalue*v_v[int(network_matdata[13][data->ind8[k]])-1]; 
        
         Jy_v[data->ind8[k]] = (matval1 + matval2 + matval3 + matval4 + matval5 + matval6)/C_vessels_v[data->ind8[k]];

   };

#pragma omp parallel for shared(G_vessels_v,C_vessels_v,Jy_v,v_v, network_matdata, data) private(k,matvalue,work1,work2,matval1,matval2,matval3,matval4,matval5,matval6) num_threads(data->nthreads) schedule(static, 4)   
   for (k = 0; k < data->ind9_cnt; ++k) {

         work1  = (G_vessels_v[data->ind9[k]] + G_vessels_v[int(network_matdata[6][data->ind9[k]])-1] + G_vessels_v[int(network_matdata[7][data->ind9[k]])-1] 
                  + G_vessels_v[int(network_matdata[8][data->ind9[k]])-1]);
         work2 =  (G_vessels_v[data->ind9[k]] + G_vessels_v[int(network_matdata[11][data->ind9[k]])-1] + G_vessels_v[int(network_matdata[12][data->ind9[k]])-1]);

         matvalue = (-4.0*G_vessels_v[data->ind9[k]] + 2.0*pow(G_vessels_v[data->ind9[k]],2)*(1.0/work1 + 1.0/work2));
         matval1 = matvalue*v_v[data->ind9[k]];

         matvalue = (2.0*G_vessels_v[data->ind9[k]]*G_vessels_v[int(network_matdata[6][data->ind9[k]])-1]/work1);
         matval2 = matvalue*v_v[int(network_matdata[6][data->ind9[k]])-1];

         matvalue = (2.0*G_vessels_v[data->ind9[k]]*G_vessels_v[int(network_matdata[7][data->ind9[k]])-1]/work1);
         matval3 = matvalue*v_v[int(network_matdata[7][data->ind9[k]])-1];

         matvalue = (2.0*G_vessels_v[data->ind9[k]]*G_vessels_v[int(network_matdata[8][data->ind9[k]])-1]/work1);
         matval4 = matvalue*v_v[int(network_matdata[8][data->ind9[k]])-1];

         matvalue = (2.0*G_vessels_v[data->ind9[k]]*G_vessels_v[int(network_matdata[11][data->ind9[k]])-1]/work2);
         matval5 = matvalue*v_v[int(network_matdata[11][data->ind9[k]])-1];

         matvalue = (2.0*G_vessels_v[data->ind9[k]]*G_vessels_v[int(network_matdata[12][data->ind9[k]])-1]/work2);
         matval6 = matvalue*v_v[int(network_matdata[12][data->ind9[k]])-1];

         Jy_v[data->ind9[k]] = (matval1 + matval2 + matval3 + matval4 + matval5 + matval6)/C_vessels_v[data->ind9[k]];
  };


#pragma omp parallel for shared(G_vessels_v,C_vessels_v,Jy_v,v_v, network_matdata, data) private(k,matvalue,work1,work2,matval1,matval2,matval3,matval4,matval5) num_threads(data->nthreads) schedule(static, 4)   
  for (k = 0; k < data->ind10_cnt; ++k) {
             
         work1 = (G_vessels_v[data->ind10[k]] + G_vessels_v[int(network_matdata[6][data->ind10[k]])-1] + G_vessels_v[int(network_matdata[7][data->ind10[k]])-1]);
         work2 = (G_vessels_v[data->ind10[k]] + G_vessels_v[int(network_matdata[11][data->ind10[k]])-1] + G_vessels_v[int(network_matdata[12][data->ind10[k]])-1]);
        
         matvalue = (-4.0*G_vessels_v[data->ind10[k]] + 2.0*pow(G_vessels_v[data->ind10[k]],2)*(1.0/work1 + 1.0/work2));
         matval1 = matvalue*v_v[data->ind10[k]];

         matvalue = (2.0*G_vessels_v[data->ind10[k]]*G_vessels_v[int(network_matdata[6][data->ind10[k]])-1]/work1);
         matval2 = matvalue*v_v[int(network_matdata[6][data->ind10[k]])-1];

         matvalue = (2.0*G_vessels_v[data->ind10[k]]*G_vessels_v[int(network_matdata[7][data->ind10[k]])-1]/work1);
         matval3 = matvalue*v_v[int(network_matdata[7][data->ind10[k]])-1];

         matvalue = (2.0*G_vessels_v[data->ind10[k]]*G_vessels_v[int(network_matdata[11][data->ind10[k]])-1]/work2);
         matval4 = matvalue*v_v[int(network_matdata[11][data->ind10[k]])-1];

         matvalue = (2.0*G_vessels_v[data->ind10[k]]*G_vessels_v[int(network_matdata[12][data->ind10[k]])-1]/work2);
         matval5 = matvalue*v_v[int(network_matdata[12][data->ind10[k]])-1];

         Jy_v[data->ind10[k]] = (matval1 + matval2 + matval3 + matval4 + matval5)/C_vessels_v[data->ind10[k]];


   };
  return(0);
  
};


/*
 *-------------------------------
 * Private helper functions
 *-------------------------------
 */

static void PrintOutput(realtype t, realtype y1, realtype y2, realtype y3)
{
#if defined(SUNDIALS_EXTENDED_PRECISION)
  printf("At t = %0.4Le      y =%14.6Le  %14.6Le  %14.6Le\n", t, y1, y2, y3);
#elif defined(SUNDIALS_DOUBLE_PRECISION)
  printf("At t = %0.4e      y =%14.6e  %14.6e  %14.6e\n", t, y1, y2, y3);
#else
  printf("At t = %0.4e      y =%14.6e  %14.6e  %14.6e\n", t, y1, y2, y3);
#endif

  return;
}

/* 
 * Get and print some final statistics
 */

static void PrintFinalStats(void *cvode_mem)
{
  long int nst, nfe, nsetups, nje, nfeLS, nni, ncfn, netf, nge;
  int flag;

  flag = CVodeGetNumSteps(cvode_mem, &nst);
  check_flag(&flag, "CVodeGetNumSteps", 1);
  flag = CVodeGetNumRhsEvals(cvode_mem, &nfe);
  check_flag(&flag, "CVodeGetNumRhsEvals", 1);
  flag = CVodeGetNumLinSolvSetups(cvode_mem, &nsetups);
  check_flag(&flag, "CVodeGetNumLinSolvSetups", 1);
  flag = CVodeGetNumErrTestFails(cvode_mem, &netf);
  check_flag(&flag, "CVodeGetNumErrTestFails", 1);
  flag = CVodeGetNumNonlinSolvIters(cvode_mem, &nni);
  check_flag(&flag, "CVodeGetNumNonlinSolvIters", 1);
  flag = CVodeGetNumNonlinSolvConvFails(cvode_mem, &ncfn);
  check_flag(&flag, "CVodeGetNumNonlinSolvConvFails", 1);

  flag = CVDlsGetNumJacEvals(cvode_mem, &nje);
  check_flag(&flag, "CVDlsGetNumJacEvals", 1);
  flag = CVDlsGetNumRhsEvals(cvode_mem, &nfeLS);
  check_flag(&flag, "CVDlsGetNumRhsEvals", 1);

  flag = CVodeGetNumGEvals(cvode_mem, &nge);
  check_flag(&flag, "CVodeGetNumGEvals", 1);

  printf("\nFinal Statistics:\n");
  printf("nst = %-6ld nfe  = %-6ld nsetups = %-6ld nfeLS = %-6ld nje = %ld\n",
	 nst, nfe, nsetups, nfeLS, nje);
  printf("nni = %-6ld ncfn = %-6ld netf = %-6ld nge = %ld\n \n",
	 nni, ncfn, netf, nge);
}

/*
 * Check function return value...
 *   opt == 0 means SUNDIALS function allocates memory so check if
 *            returned NULL pointer
 *   opt == 1 means SUNDIALS function returns a flag so check if
 *            flag >= 0
 *   opt == 2 means function allocates memory so check if returned
 *            NULL pointer 
 */

static int check_flag(void *flagvalue, const char *funcname, int opt)
{
  int *errflag;

  /* Check if SUNDIALS function returned NULL pointer - no memory allocated */
  if (opt == 0 && flagvalue == NULL) {
    fprintf(stderr, "\nSUNDIALS_ERROR: %s() failed - returned NULL pointer\n\n",
	    funcname);
    return(1); }

  /* Check if flag < 0 */
  else if (opt == 1) {
    errflag = (int *) flagvalue;
    if (*errflag < 0) {
      fprintf(stderr, "\nSUNDIALS_ERROR: %s() failed with flag = %d\n\n",
	      funcname, *errflag);
      return(1); }}

  /* Check if function returned NULL pointer - no memory allocated */
  else if (opt == 2 && flagvalue == NULL) {
    fprintf(stderr, "\nMEMORY_ERROR: %s() failed - returned NULL pointer\n\n",
	    funcname);
    return(1); }

  return(0);
}


