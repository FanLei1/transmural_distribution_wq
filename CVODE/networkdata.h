#ifndef NETWORKDATA_H
#define NETWORKDATA_H

#include <gsl/gsl_errno.h>             /* access to GSL cubic spline library     */
#include <gsl/gsl_spline.h>            /* access to GSL cubic spline library     */


typedef struct {

  SUNMatrix network_mat; // Network matrix
  SUNMatrix param_mat; // Parameter matrix
  N_Vector long_dist_bp, long_dist_ap, long_dist_cp, long_dist_php; // Passive properties
  N_Vector long_dist_rhoa, long_dist_pha, long_dist_ca, long_dist_ma, long_dist_fmax, long_dist_ktau, long_dist_ka, long_dist_a;  // Active properties
  N_Vector vessel_length, deltaP, Rpt, dRpdP, G_vessels, C_vessels;
  int nvessels;
  double viscosity;
  double pa_const;
  double pv_const;

  double pt_const;
  double dptdt_const;
 
  gsl_spline *pa;
  gsl_spline *pv;
  gsl_spline *pt;
  gsl_interp_accel *acc_pa; 
  gsl_interp_accel *acc_pv; 
  gsl_interp_accel *acc_pt; 
 
} *UserData;


#endif
