#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <nvector/nvector_serial.h>    /* access to serial N_Vector            */
#include <sunmatrix/sunmatrix_dense.h> /* access to dense SUNMatrix            */

#include "readnetwork.h"


/* Read in network input deck  */
void readnetwork(const char *filename, UserData *data, int ncols)
{

  /* Extract needed problem constants from data */
  //UserData* data;
  //data = (UserData*) user_data;

  FILE *file= fopen(filename, "r");
  if(file==NULL)
  {
      printf("Error in Opening file");
      exit(EXIT_FAILURE);
  };

  int rowCount=1;
  size_t count=0;
  char *line = NULL;
  while(getline(&line, &count, file)!=-1) {rowCount=rowCount+1;};
  printf(" Total number of vessels = %d\n", rowCount);

  data->nvessels = rowCount - 1;
  data->network_mat = SUNDenseMatrix(rowCount - 1, ncols);
  data->long_dist_bp = N_VNew_Serial(rowCount - 1);

  data->long_dist_ap = N_VNew_Serial(rowCount - 1);
  data->long_dist_cp = N_VNew_Serial(rowCount - 1);
  data->long_dist_php = N_VNew_Serial(rowCount - 1);
  data->long_dist_rhoa = N_VNew_Serial(rowCount - 1); 
  data->long_dist_pha = N_VNew_Serial(rowCount - 1);
  data->long_dist_ca = N_VNew_Serial(rowCount - 1);
  data->long_dist_ma = N_VNew_Serial(rowCount - 1);
  data->long_dist_fmax = N_VNew_Serial(rowCount - 1);
  data->long_dist_ktau = N_VNew_Serial(rowCount - 1);
  data->long_dist_ka = N_VNew_Serial(rowCount - 1);
  data->long_dist_a = N_VNew_Serial(rowCount - 1);; 
  data->vessel_length = N_VNew_Serial(rowCount - 1);
  data->deltaP = N_VNew_Serial(rowCount - 1);
  data->Rpt = N_VNew_Serial(rowCount - 1);
  data->dRpdP = N_VNew_Serial(rowCount - 1);
  data->G_vessels = N_VNew_Serial(rowCount - 1);
  data->C_vessels = N_VNew_Serial(rowCount - 1);

  data->pt = N_VNew_Serial(rowCount - 1);
  data->dptdt = N_VNew_Serial(rowCount - 1);

  N_VConst(0.0, data->pt);
  N_VConst(0.0, data->dptdt);
	
  rewind(file);

  double value = 0;
  int cnt = 0;
  realtype* matdata;
  matdata = SUNDenseMatrix_Data(data->network_mat);
  while(fscanf(file, "%lg", &value)==1){

          int rowind = cnt/ncols; int colind = cnt%ncols;
          //printf("%15.8lg, %d, %d\n", value, rowind, colind);
          matdata[(colind)*(rowCount-1)+ rowind] = value;
          ++cnt;

  };

  NV_DATA_S(data->vessel_length) = SUNDenseMatrix_Column(data->network_mat, 1);

  // Set Up indices for different cases based on network matrix
  realtype **network_matdata;
  network_matdata = SUNDenseMatrix_Cols(data->network_mat);


  // First read to determine number for each case
  data->ind1_cnt = 0;
  data->ind2_cnt = 0;
  data->ind3_cnt = 0;
  data->ind8_cnt = 0;
  data->ind9_cnt = 0;
  data->ind10_cnt = 0;
  data->ind_term_vessel_cnt = 0;
  for(int k = 1; k < data->nvessels; ++k){

        /* for external arteries (case 1)*/
        if(int(network_matdata[7][k]) != 0 && int(network_matdata[8][k]) == 0 && int(network_matdata[11][k]) == 0 && int(network_matdata[12][k]) == 0){
                ++data->ind1_cnt;
        }
        else if(int(network_matdata[8][k]) != 0 && int(network_matdata[11][k]) == 0 && int(network_matdata[12][k]) == 0){ 
                ++data->ind2_cnt;
        }
        else if(int(network_matdata[7][k]) == 0 && int(network_matdata[11][k]) == 0 && int(network_matdata[12][k]) == 0){ 
                ++data->ind3_cnt;
        }
        else if(int(network_matdata[13][k]) != 0){ 
                ++data->ind8_cnt;
        }
        else if(int(network_matdata[8][k]) != 0){ 
                ++data->ind9_cnt;
        }
        else{
                ++data->ind10_cnt;
        };

	if(int(network_matdata[10][k]) == 0 && int(network_matdata[11][k]) == 0 && int(network_matdata[12][k]) == 0){
		++data->ind_term_vessel_cnt;
	};
  };

  data->ind1 = (int *) malloc(sizeof(int)*data->ind1_cnt);
  data->ind2 = (int *) malloc(sizeof(int)*data->ind2_cnt);
  data->ind3 = (int *) malloc(sizeof(int)*data->ind3_cnt);
  data->ind8 = (int *) malloc(sizeof(int)*data->ind8_cnt);
  data->ind9 = (int *) malloc(sizeof(int)*data->ind9_cnt);
  data->ind10 = (int *) malloc(sizeof(int)*data->ind10_cnt);
  data->ind_term_vessel = (int *) malloc(sizeof(int)*data->ind_term_vessel_cnt);

  int ind1_cnt = 0;
  int ind2_cnt = 0;
  int ind3_cnt = 0;
  int ind8_cnt = 0;
  int ind9_cnt = 0;
  int ind10_cnt = 0;
  int ind_term_vessel_cnt = 0;
  for(int k = 1; k < data->nvessels; ++k){
        if(int(network_matdata[7][k]) != 0 && int(network_matdata[8][k]) == 0 && int(network_matdata[11][k]) == 0 && int(network_matdata[12][k]) == 0){
                data->ind1[ind1_cnt] = k;
                ++ind1_cnt;
        }
        else if(int(network_matdata[8][k]) != 0 && int(network_matdata[11][k]) == 0 && int(network_matdata[12][k]) == 0){ 
                data->ind2[ind2_cnt] = k;
                ++ind2_cnt;
        }
        else if(int(network_matdata[7][k]) == 0 && int(network_matdata[11][k]) == 0 && int(network_matdata[12][k]) == 0){ 
                data->ind3[ind3_cnt] = k;
                ++ind3_cnt;
        }
        else if(int(network_matdata[13][k]) != 0){ 
                data->ind8[ind8_cnt] = k;
                ++ind8_cnt;
        }
        else if(int(network_matdata[8][k]) != 0){ 
                data->ind9[ind9_cnt] = k;
                ++ind9_cnt;
        }
        else{
                data->ind10[ind10_cnt] = k;
                ++ind10_cnt;
        };

	if(int(network_matdata[10][k]) == 0 && int(network_matdata[11][k]) == 0 && int(network_matdata[12][k]) == 0){
		data->ind_term_vessel[ind_term_vessel_cnt] = k;
		++ind_term_vessel_cnt;
	};
  };



  printf("Setting vessel parameters\n");
  vesselparam(data);
  //N_VPrint_Serial(data->long_dist_bp);

  
};


void vesselparam(UserData* data){

   double um2m = 1e-6;                  // micron to meter
   double um2mm = 1e-3;                 // micron to millimeter
   double mmHg2Pa = 133.32239;          // pressure in mmHg to Pascals
   double mmHg2MPa = 0.00013332239;     // pressure in mmHg to MegaPascals
   double Pa2MPa = 1e-6;                // pressure from Pascal to MegaPascal
   double dynpcm22MPa =  1.0e-7;        // pressure in dynes/cm2 to MegaPascals
   double dynpcm22Pa = 0.1;
   double mm3ps2mLpmin = 6e-2;          // flow in millimeter3/second to milliliter/minute

   double rp[7] = {3.1,   4.7,   33.12, 49.37, 81.04, 125.97, 730.5};
   double ap[7] = {3.25,  4.55,  35.02, 51.38, 85.53, 133.52, 829.6};
   double bp[7] = {1.0,   1.5,   10.9,  16.41, 32.17, 51.6+5, 401.52};
   double php[7] ={0.0,   0.0,   0.612, 1.88,  1.64,  0.96,   30.39};
   double cp[7] = {19.28, 17.24, 20.11, 14.23, 21.24, 23.54,  3.35};

   double ra[7]   = { 3.1000e+00,   4.7000e+00,   3.3100e+01,   4.9400e+01,   8.1000e+01,   1.2600e+02,   7.3050e+02};
   double rhoa[7] = { 0.0000e+00,   2.7500e+00,   2.6470e+01,   5.0960e+01,   7.7860e+01,   7.4140e+01,   0.0000e+00};
   double pha[7]  = { 0.0000e+00,   2.0000e+01,   6.9390e+01,   1.2540e+02,   1.4887e+02,   7.7260e+01,   0.0000e+00};
   double ca[7]   = { 1.0000e+01,   1.0000e+01,   4.7210e+01,   7.7330e+01,   1.3540e+02,   5.1310e+01,   2.0000e+01};
   double fmax[7] = { 0.0000e+00,   2.8000e-01,   4.3000e-01,   8.3000e-01,   1.0000e+00,   6.2000e-01,   0.0000e+00};
   double ktau[7] = { 2.0000e+02,   1.5000e+02,   1.5600e+02,   1.9950e+02,   6.7500e+01,   1.1700e+02,   2.0000e+02};
   double ka[7]   = { 1.0000e-10,   1.9800e-09,   1.9800e-09,   4.1100e-08,   3.5500e-07,   2.1500e-07,   1.0000e-20};
   double a[7]    = { 3.3330e-01,   3.3330e-01,   3.3330e-01,   5.7140e-01,   5.7140e-01,   5.7140e-01,   1.0000e-02};

   // Interpolate passive and active data with a smooth spline
   gsl_spline *Sap = gsl_spline_alloc (gsl_interp_cspline, 7);
   gsl_spline_init (Sap, rp, ap, 7);

   gsl_spline *Sbp = gsl_spline_alloc (gsl_interp_cspline, 7);
   gsl_spline_init (Sbp, rp, bp, 7);
 
   gsl_spline *Sphp = gsl_spline_alloc (gsl_interp_cspline, 7);
   gsl_spline_init (Sphp, rp, php, 7);

   gsl_spline *Scp = gsl_spline_alloc (gsl_interp_cspline, 7);
   gsl_spline_init (Scp, rp, cp, 7);
 
   gsl_spline *Srhoa = gsl_spline_alloc (gsl_interp_cspline, 7);
   gsl_spline_init (Srhoa, ra, rhoa, 7);

   gsl_spline *Spha = gsl_spline_alloc (gsl_interp_cspline, 7);
   gsl_spline_init (Spha, ra, pha, 7);

   gsl_spline *Sca = gsl_spline_alloc (gsl_interp_cspline, 7);
   gsl_spline_init (Sca, ra, ca, 7);
 
   gsl_spline *Sfmax = gsl_spline_alloc (gsl_interp_cspline, 7);
   gsl_spline_init (Sfmax, ra, fmax, 7);
  
   gsl_spline *Sktau = gsl_spline_alloc (gsl_interp_cspline, 7);
   gsl_spline_init (Sktau, ra, ktau, 7);

   gsl_spline *Ska = gsl_spline_alloc (gsl_interp_cspline, 7);
   gsl_spline_init (Ska, ra, ka, 7);
 
   gsl_spline *Sa = gsl_spline_alloc (gsl_interp_cspline, 7);
   gsl_spline_init (Sa, ra, a, 7);


   /* Linear interpolation */
   gsl_interp *Sap_lin = gsl_interp_alloc (gsl_interp_linear, 7);
   gsl_interp_init (Sap_lin, rp, ap, 7);
   gsl_interp *Sbp_lin = gsl_interp_alloc (gsl_interp_linear, 7);
   gsl_interp_init (Sbp_lin, rp, bp, 7);
   gsl_interp *Sphp_lin = gsl_interp_alloc (gsl_interp_linear, 7);
   gsl_interp_init (Sphp_lin, rp, php, 7);
   gsl_interp *Scp_lin = gsl_interp_alloc (gsl_interp_linear, 7);
   gsl_interp_init (Scp_lin, rp, cp, 7);
   gsl_interp_accel *Sap_acc_lin  = gsl_interp_accel_alloc ();
   gsl_interp_accel *Sbp_acc_lin  = gsl_interp_accel_alloc ();
   gsl_interp_accel *Sphp_acc_lin = gsl_interp_accel_alloc ();
   gsl_interp_accel *Scp_acc_lin = gsl_interp_accel_alloc ();

 

   int order_index;
   double radius;
   realtype *bp_v,  *ap_v, *cp_v, *php_v;
   realtype *rhoa_v, *pha_v, *ca_v, *ma_v, *fmax_v, *ktau_v, *ka_v, *a_v ;

   bp_v = NV_DATA_S(data->long_dist_bp);
   ap_v = NV_DATA_S(data->long_dist_ap);
   cp_v = NV_DATA_S(data->long_dist_cp);
   php_v = NV_DATA_S(data->long_dist_php);
   rhoa_v = NV_DATA_S(data->long_dist_rhoa);
   pha_v = NV_DATA_S(data->long_dist_pha);
   ca_v = NV_DATA_S(data->long_dist_ca);
   ma_v  = NV_DATA_S(data->long_dist_ma);
   fmax_v = NV_DATA_S(data->long_dist_fmax);
   ktau_v = NV_DATA_S(data->long_dist_ktau);
   ka_v  = NV_DATA_S(data->long_dist_ka);
   a_v = NV_DATA_S(data->long_dist_a);

   gsl_interp_accel *Sap_acc  = gsl_interp_accel_alloc ();
   gsl_interp_accel *Sbp_acc  = gsl_interp_accel_alloc ();
   gsl_interp_accel *Sphp_acc = gsl_interp_accel_alloc ();
   gsl_interp_accel *Scp_acc = gsl_interp_accel_alloc ();
   gsl_interp_accel *Srhoa_acc  = gsl_interp_accel_alloc ();
   gsl_interp_accel *Spha_acc  = gsl_interp_accel_alloc ();
   gsl_interp_accel *Sca_acc = gsl_interp_accel_alloc ();
   gsl_interp_accel *Sma_acc = gsl_interp_accel_alloc ();
   gsl_interp_accel *Sfmax_acc = gsl_interp_accel_alloc ();
   gsl_interp_accel *Sktau_acc = gsl_interp_accel_alloc ();
   gsl_interp_accel *Ska_acc = gsl_interp_accel_alloc ();
   gsl_interp_accel *Sa_acc = gsl_interp_accel_alloc ();
 
    
   for(int i = 0; i < (data->nvessels); ++i){

	radius = IJth(data->network_mat,i+1,3)/2.0;

        //printf("vessel number = %d, radius = %lg\n", i, radius);
        if(radius < rp[0]){
		
		//printf("Extrapolating\n");
		ap_v[i]  = ap[0] + (radius - rp[0])*(ap[1] - ap[0])/(rp[1] - rp[0]); 
		bp_v[i]  = bp[0] + (radius - rp[0])*(bp[1] - bp[0])/(rp[1] - rp[0]); 
		php_v[i]  = php[0] + (radius - rp[0])*(php[1] - php[0])/(rp[1] - rp[0]); 
		cp_v[i]  = cp[0] + (radius - rp[0])*(cp[1] - cp[0])/(rp[1] - rp[0]); 
                //printf("vessel number = %d, radius = %lg, ap = %lg, bp = %lg, php = %lg, cp = %lg\n", i, radius, ap_v[i], bp_v[i], php_v[i], cp_v[i]);

        // Linear interpolation if radius falls outside range
        }else if(radius > rp[6]){

		//printf("Extrapolating\n");
		ap_v[i]  = ap[6] + (radius - rp[6])*(ap[6] - ap[5])/(rp[6] - rp[5]); 
		bp_v[i]  = bp[6] + (radius - rp[6])*(bp[6] - bp[5])/(rp[6] - rp[5]); 
		php_v[i]  = php[6] + (radius - rp[6])*(php[6] - php[5])/(rp[6] - rp[5]); 
		cp_v[i]  = cp[6] + (radius - rp[6])*(cp[6] - cp[5])/(rp[6] - rp[5]); 
                //printf("radius = %lg, ap = %lg, bp = %lg, php = %lg, cp = %lg\n", radius, ap_v[i], bp_v[i], php_v[i], cp_v[i]);

        }else{

		//printf("Interploating\n");
		ap_v[i]  = gsl_spline_eval (Sap, radius, Sap_acc);
		bp_v[i]  = gsl_spline_eval (Sbp, radius, Sbp_acc);
		php_v[i] = gsl_spline_eval (Sphp, radius, Sphp_acc);
		cp_v[i]  = gsl_spline_eval (Scp, radius, Scp_acc);

		//Linear Interpolation
		//ap_v[i]  = gsl_interp_eval (Sap_lin, rp, ap, radius, Sap_acc_lin);
		//bp_v[i]  = gsl_interp_eval (Sbp_lin, rp, bp, radius, Sbp_acc_lin);
		//php_v[i] = gsl_interp_eval (Sphp_lin, rp, php, radius, Sphp_acc_lin);
		//cp_v[i]  = gsl_interp_eval (Scp_lin, rp, cp, radius, Scp_acc_lin);


        };

        if(radius < ra[0]){
		
		//printf("Extrapolating\n");
		rhoa_v[i]  = rhoa[0] + (radius - ra[0])*(rhoa[1] - rhoa[0])/(ra[1] - ra[0]); 
		pha_v[i]  = pha[0] + (radius - ra[0])*(pha[1] - pha[0])/(ra[1] - ra[0]); 
		ca_v[i]  = ca[0] + (radius - ra[0])*(ca[1] - ca[0])/(ra[1] - ra[0]); 
		fmax_v[i]  = fmax[0] + (radius - ra[0])*(fmax[1] - fmax[0])/(ra[1] - ra[0]); 
		ktau_v[i]  = ktau[0] + (radius - ra[0])*(ktau[1] - ktau[0])/(ra[1] - ra[0]); 
		ka_v[i]  = ka[0] + (radius - ra[0])*(ka[1] - ka[0])/(ra[1] - ra[0]); 
		a_v[i]  = a[0] + (radius - ra[0])*(a[1] - a[0])/(ra[1] - ra[0]); 
                //printf("radius = %lg, rhoa = %lg, pha = %lg, ca = %lg, fmax = %lg\n", radius, rhoa_v[i], pha_v[i], ca_v[i], fmax_v[i]);

        }else if(radius > ra[6]){

		//printf("Extrapolating\n");
		rhoa_v[i]  = rhoa[6] + (radius - ra[6])*(rhoa[6] - rhoa[5])/(ra[6] - ra[5]); 
		pha_v[i]  = pha[6] + (radius - ra[6])*(pha[6] - pha[5])/(ra[6] - ra[5]); 
		ca_v[i]  = ca[6] + (radius - ra[6])*(ca[6] - ca[5])/(ra[6] - ra[5]); 
		fmax_v[i]  = fmax[6] + (radius - ra[6])*(fmax[6] - fmax[5])/(ra[6] - ra[5]); 
		ktau_v[i]  = ktau[6] + (radius - ra[6])*(ktau[6] - ktau[5])/(ra[6] - ra[5]); 
		ka_v[i]  = ka[6] + (radius - ra[6])*(ka[6] - ka[5])/(ra[6] - ra[5]); 
		a_v[i]  = a[6] + (radius - ra[6])*(a[6] - a[5])/(ra[6] - ra[5]); 
                //printf("radius = %lg, rhoa = %lg, pha = %lg, ca = %lg, fmax = %lg\n", radius, rhoa_v[i], pha_v[i], ca_v[i], fmax_v[i]);

        }else{

		//printf("Interploating\n");
		rhoa_v[i]  = gsl_spline_eval (Srhoa, radius, Srhoa_acc);
		pha_v[i]  = gsl_spline_eval (Spha, radius, Spha_acc);
		ca_v[i] = gsl_spline_eval (Sca, radius, Sca_acc);
		fmax_v[i]  = gsl_spline_eval (Sfmax, radius, Sfmax_acc);
		ktau_v[i]  = gsl_spline_eval (Sktau, radius, Sktau_acc);
		ka_v[i]  = gsl_spline_eval (Ska, radius, Ska_acc);
		a_v[i]  = gsl_spline_eval (Sa, radius, Sa_acc);
        };

	// Scale the parameters appropriately
	ap_v[i] = ap_v[i]*um2mm;
	bp_v[i] = bp_v[i]*um2mm;
	php_v[i] = php_v[i]*mmHg2MPa;
	cp_v[i] = cp_v[i]*mmHg2MPa;
	rhoa_v[i] = rhoa_v[i]*um2mm;
	pha_v[i] = pha_v[i]*mmHg2MPa;
	ca_v[i] = ca_v[i]*mmHg2MPa;

        if(IJth(data->network_mat,i+1,3) < 1.0){
		rhoa_v[i] = 0.0			;
		pha_v[i] = 0.0			;
		ca_v[i] = 20.0*mmHg2MPa		;
		fmax_v[i] = 0.0			;
		ktau_v[i] = 1000.0*dynpcm22MPa	;		
		ka_v[i] = 1e-18			;
		a_v[i] = 1e-10			;
  	};


        if(cp_v[i] < 0) {cp_v[i] = 1.0e-4;}
        if(bp_v[i] < 0) {bp_v[i] = 0.0;}
        if(php_v[i] < 0) {php_v[i] = 0.0;}
        if(rhoa_v[i] < 0) {rhoa_v[i] = 1.0e-4;}
        if(ca_v[i] < 0) {ca_v[i] = 1.0e-4;}
        if(fmax_v[i] < 0) {fmax_v[i] = 0.0;}
        if(ka_v[i] < 0) {ka_v[i] = 1.0e-10;}
        if(a_v[i] < 0) {a_v[i] = 0.01;}


   };
   data->viscosity = 2.7e-9;

};

void getterminalvessel(UserData* data, int** outArray1, int* outLen1){

   *outLen1 = data->ind_term_vessel_cnt;
   *outArray1 = data->ind_term_vessel;
   
};



