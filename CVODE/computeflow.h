#ifndef COMPUTEFLOW_H
#define COMPUTEFLOW_H
#include <cvode/cvode.h>               /* prototypes for CVODE fcts., consts.  */
#include <nvector/nvector_serial.h>    /* access to serial N_Vector            */
#include <sunmatrix/sunmatrix_dense.h> /* access to dense SUNMatrix            */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_errno.h>             /* access to GSL cubic spline library     */
#include <gsl/gsl_spline.h>            /* access to GSL cubic spline library     */
#include "readnetwork.h"

void computeflow(UserData *data, double* inArray1, int inLen1, double* inArray2, int inLen2, double** outArray1, int* outLen1, double** outArray2, int* outLen2);
#endif 

