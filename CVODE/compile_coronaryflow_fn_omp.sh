#!/bin/sh
swig -python readnetwork_omp.i 
g++ -fPIC -O3 -ffast-math -ftree-vectorizer-verbose=2 -march=native -c readnetwork_omp.c -fopenmp -lpthread
g++ -fPIC -O3 -ffast-math -ftree-vectorizer-verbose=2 -march=native -I/usr/include/python2.7 -c readnetwork_omp_wrap.c -fopenmp -lpthread
g++ -O3 -shared -ffast-math -ftree-vectorizer-verbose=2 -march=native \
readnetwork_omp.o \
readnetwork_omp_wrap.o \
-L/usr/lib/x86_64-linux-gnu -lgsl -L/usr/local/lib -lsundials_cvode -lsundials_nvecopenmp -lcblas -fopenmp -lpthread \
-o _readnetwork_omp.so

swig -python computeflow_omp.i 
g++ -fPIC -O3 -ffast-math -ftree-vectorizer-verbose=2 -march=native -c computeflow_omp.c -fopenmp -lpthread
g++ -fPIC -O3 -ffast-math -ftree-vectorizer-verbose=2 -march=native -w -I/usr/include/python2.7 -c computeflow_omp_wrap.c -fopenmp -lpthread
g++ -O3 -shared -ffast-math -ftree-vectorizer-verbose=2 -march=native \
computeflow_omp.o \
computeflow_omp_wrap.o \
-L/usr/lib/x86_64-linux-gnu -lgsl -L/usr/local/lib -lsundials_cvode -lsundials_nvecopenmp -lcblas -fopenmp -lpthread \
-o _computeflow_omp.so


swig -python coronaryflow_asymmetric_fn_omp.i 
g++ -fPIC -O3 -ffast-math -ftree-vectorizer-verbose=2 -march=native -std=c++11 -I/usr/include/python2.7 -c coronaryflow_asymmetric_fn_omp.c   -fopenmp -lpthread
g++ -fPIC -O3 -ffast-math -ftree-vectorizer-verbose=2 -march=native -std=c++11 -w -I/usr/include/python2.7 -c coronaryflow_asymmetric_fn_omp_wrap.c    -fopenmp -lpthread
g++ -O3 -shared -ffast-math -ftree-vectorizer-verbose=2 -march=native \
coronaryflow_asymmetric_fn_omp.o \
coronaryflow_asymmetric_fn_omp_wrap.o \
-L/usr/lib/x86_64-linux-gnu -lgsl -L/usr/local/lib -lsundials_cvode -lsundials_nvecopenmp -lcblas  -L/usr/local/lib -lvtkCommonCore-8.1 -lvtkCommonDataModel-8.1 -lvtkIOCore-8.1 -lvtkIOLegacy-8.1 -lvtkFiltersCore-8.1 -lvtkCommonExecutionModel-8.1 -lvtkFiltersGeneral-8.1  -fopenmp -lpthread -o _coronaryflow_asymmetric_fn_omp.so

#
#swig -c++ -python  postprocess_flow.i 
#g++ -fPIC -O3 -ffast-math -ftree-vectorizer-verbose=2 -march=native -std=c++11 -I/usr/local/include/vtk-8.1 -c postprocess_flow.cpp  
#g++ -fPIC -O3 -ffast-math -ftree-vectorizer-verbose=2 -march=native -std=c++11 -I/usr/include/python2.7 -I/usr/local/include/vtk-8.1 -c postprocess_flow_wrap.cxx
#g++ -O3 -shared -ffast-math -ftree-vectorizer-verbose=2 -march=native \
#postprocess_flow.o \
#postprocess_flow_wrap.o \
#-L/usr/lib/x86_64-linux-gnu -lgsl -L/usr/local/lib -lsundials_cvode -lsundials_nvecparallel -lcblas  -L/usr/local/lib -lvtkCommonCore-8.1 -lvtkCommonDataModel-8.1 -lvtkIOCore-8.1 -lvtkIOLegacy-8.1 -lvtkFiltersCore-8.1 -lvtkCommonExecutionModel-8.1 -lvtkFiltersGeneral-8.1 -o _postprocess_flow.so
#



