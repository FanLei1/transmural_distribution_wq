import sys
sys.path.append("/mnt/home/fanlei1/")
import os
import numpy as np
from scipy.interpolate import interp1d
import matplotlib as mpl
if os.environ.get('DISPLAY','') == '':
    print('no display found. Using non-interactive Agg backend')
    mpl.use('Agg')
import matplotlib.pyplot as plt
import vtk_py as vtk_py
from vtk import *
from vtk.util.numpy_support import vtk_to_numpy
from vtk.util.numpy_support import numpy_to_vtk
import postprocess_flow
import time

# One can also import a serial version with omp
import coronaryflow_asymmetric_fn
import readnetwork
import computeflow

# One can also import a parallel version with omp
import coronaryflow_asymmetric_fn_omp
import readnetwork_omp
import computeflow_omp



networkfilename = "./inputfiles/Tree_8609_Vessels.txt";
networkfilename = "./inputfiles/network_matrix_test_8634vessel_23.txt"
#networkfilename = "./inputfiles/network_matrix_test_15vessel.txt"
networkfilename = "./inputfiles/network_matrix_400vessel.txt";
ncols = 23

#networkfilename = "./inputfiles/network_matrix_test_8634vessel.txt"; # This txt file using format with only 20 cols
#ncols = 20

bcfilename = "./inputfiles/input_output_pressures.txt";
ptfilename = "./inputfiles/tissue_pressures.txt";

def writedata(pdata, data, dataname, outfilename):

	# Add data to line representation of coronary tree
	vtkdata = numpy_to_vtk(np.array(data, dtype=float), deep=True, array_type=vtk.VTK_FLOAT)
	vtkdata.SetName(dataname)

	pdata.GetCellData().AddArray(vtkdata)

  	celldatatoptdata_ = vtk.vtkCellDataToPointData();
  	celldatatoptdata_.SetInputData(pdata);
  	celldatatoptdata_.SetPassCellData(1);
  	celldatatoptdata_.Update();

  	celldatatoptdata_.GetOutput().GetPointData().SetActiveScalars("TubeRadius");

        tubefilter_ = vtk.vtkTubeFilter()
  	tubefilter_.SetInputData(celldatatoptdata_.GetOutput());
  	tubefilter_.SetNumberOfSides(10);
  	tubefilter_.SetVaryRadiusToVaryRadiusByAbsoluteScalar();
  	tubefilter_.CappingOn();
  	tubefilter_.Update();

	vtk_py.writeXMLPData(tubefilter_.GetOutput(),outfilename)



def readbcfile(bcfilename):
	fdata = open(bcfilename, "r")
	lines = fdata.readlines()
	tpt = []; pin = []; pout = [];
	for line in lines:
		tpt.append(float(line.strip().rsplit("\t")[0]))
		pin.append(float(line.strip().rsplit("\t")[1]))
		pout.append(float(line.strip().rsplit("\t")[2]))

	return tpt, pin, pout

def readptfilename(ptfilename):
	fdata = open(ptfilename, "r")
	lines = fdata.readlines()
	tpt = []; pt = [];
	for line in lines:
		tpt.append(float(line.strip().rsplit("\t")[0]))
		pt.append(float(line.strip().rsplit("\t")[1]))

	return tpt, pt


def readpressure(pfilename):
	fdata = open(pfilename, "r")
	lines = fdata.readlines()
	p = [];
	for line in lines:
		p.append(float(line.strip().rsplit("\t")[0]))

	return p 



# Serial version
sim_data = readnetwork.UserData();
readnetwork.readnetwork(networkfilename, sim_data, ncols)
postprocess_flow.postprocess_flow().write_grid("./outputfiles/vessels.vtk", sim_data);
ode_obj = coronaryflow_asymmetric_fn.createODE(sim_data);




# Parallel version
nthreads = 4
sim_data = readnetwork_omp.UserData();
readnetwork_omp.readnetwork(networkfilename, sim_data, ncols, nthreads) # OMP version (need number of threads)
postprocess_flow.postprocess_flow().write_grid("./outputfiles/vessels.vtk", sim_data);
ode_obj = coronaryflow_asymmetric_fn_omp.createODE(sim_data);


termvessel_indices =  readnetwork.getterminalvessel(sim_data)

tpt, pin, pout = readbcfile(bcfilename)
pin_fn = interp1d(tpt, pin, kind='linear')
pout_fn = interp1d(tpt, pout, kind='linear')

tpt_pt, pt = readptfilename(ptfilename)
pt_fn = interp1d(tpt_pt, pt, kind='linear');

tptnew = np.linspace(0,1,501)
pin_new = pin_fn(tptnew)
pout_new = pout_fn(tptnew)
pt_new = pt_fn(tptnew)

plt.plot(tptnew, pin_new, label="interpolated Pin")
plt.plot(tpt, pin, '*', label="actual Pin")

plt.plot(tptnew, pout_new, label="interpolated Pout")
plt.plot(tpt, pout, 'x', label="actual Pout")

plt.plot(tptnew, pt_new, label="interpolated IMP")
plt.plot(tpt_pt, pt, 'x', label="actual IMP")

plt.xlabel("Time")
plt.ylabel("Pressure")


plt.savefig("./outputfiles/input.png")
plt.clf()


# Set ODE initial conditions
p0 = np.ones(sim_data.nvessels,dtype=np.double)*0.005433149918032
isdiag = 0
savedata = []
savedata.append(p0[0])

savedata_Qin = []
savedata_Qout = []
last_cyc_tpt = []
t0 = time.time()

# Generate grid 
postprocess_flow.postprocess_flow().write_grid("./outputfiles/vessels.vtk", sim_data);
pdata = vtk_py.readPData("./outputfiles/vessels.vtk")

ncycle = 5
realt = [0]
realtptnew = np.linspace(0, ncycle, ncycle*len(tptnew)-(ncycle-1))
	
ncyc = 0
pcyc = 0
BCL = 1.0
file_ = open('PT2.txt', 'wb')

file_.write("%10.5e %10.5e %10.5e %10.5e %10.5e %10.5e\n" %(0, pin_fn(0)*0.00013332239, pout_fn(0)*0.00013332239, pt_fn(0)*0.00013332239,0 ,0.005433149918032))
for p in range(1, len(realtptnew)):

	# Initialize the array at the begining of each cycle 
	if(ncyc > pcyc):
		last_cyc_tpt = []
		savedata_Qin = []
		savedata_Qout = []
		pcyc = ncyc

	# Get dt
	dt = float(realtptnew[p] - realtptnew[p-1])
	
	tcell = realtptnew[p] - ncyc*BCL
	print ncyc, tcell, dt, realtptnew[p]

	# Interpolated inlet and outplet pressure convert to MPa
	sim_data.pa_const = pin_fn(tcell)*0.00013332239; 
	sim_data.pv_const = pout_fn(tcell)*0.00013332239;

	# Interpolated IMP convert to MPa
	#sim_data.pt_const = pt_fn(tcell)*0.00013332239; 
        pt = pt_fn(tcell)*0.00013332239*np.ones(sim_data.nvessels,dtype=np.double)

	# Compute dP/dt
	#sim_data.dptdt_const = (pt_fn(tcell)  - pt_fn(tcell-dt))/dt*0.00013332239;
        dptdt = (pt_fn(tcell)  - pt_fn(tcell-dt))/dt*0.00013332239*np.ones(sim_data.nvessels,dtype=np.double)
	
	# Compute pressures in the vessels (serial version)
	#out = coronaryflow_asymmetric_fn.advanceODE(sim_data, ode_obj, realtptnew[p-1], realtptnew[p], p0, pt, dptdt)

	# Compute pressures in the vessels (parallel version)
	out = coronaryflow_asymmetric_fn_omp.advanceODE(sim_data, ode_obj, realtptnew[p-1], realtptnew[p], p0, pt, dptdt)

	# Get dPdt
        dPdt = (out - p0)/dt 

	# Compute flow in the vessels (serial version)
        #[Qin, Qout] = computeflow.computeflow(sim_data, out, dPdt)

	# Compute flow in the vessels (parallel version)
        [Qin, Qout] = computeflow_omp.computeflow(sim_data, out, dPdt)

	file_.write("%10.5e %10.5e %10.5e %10.5e %10.5e %10.5e\n" %(realtptnew[p], sim_data.pa_const, sim_data.pv_const, pt[0], Qin[0], out[149]))
	#print out

	print "tstep: ", p, \
              "time: ", realtptnew[p], \
              "Inlet P: ", sim_data.pa_const/0.0001332239, \
              "Outlet P: ", sim_data.pv_const/0.0001332239, \
              "P at 1st vessel: ", out[0]/0.0001332239,\
              "Qin at 1st vessel: ", Qin[0],\
	      "Qout at term vessels: ", sum(Qout[termvessel_indices])

	# Write flow into vtk file
	#outvtkfilename = "./outputfiles/"+"flow%03d.vtp" %(p)
	#writedata(pdata, out/0.0001332239, "P", outvtkfilename)

	## Update initial condition with current solution
	p0 = out

	## Save pressure at first vessel
	savedata.append(p0[149])
	savedata_Qin.append(Qin[0])
	savedata_Qout.append(sum(Qin[termvessel_indices]))
	realt.append(realtptnew[p])

	last_cyc_tpt.append(tcell)

	if(np.remainder(realtptnew[p],1) == 0):
		print "Total inflow = ", np.trapz(savedata_Qin, last_cyc_tpt)
		print "Total outflow = ", np.trapz(savedata_Qout, last_cyc_tpt)
		
		ncyc += 1

t1 = time.time()
print "Time elapsed = ", t1 - t0

	
plt.figure(1)
plt.plot(realtptnew, savedata, label="Output pressure")
plt.xlabel("Time")
plt.ylabel("Pressure")
plt.savefig("./outputfiles/output.png")

plt.figure(2)
plt.plot(last_cyc_tpt, savedata_Qin, label="Input flow")
plt.plot(last_cyc_tpt, savedata_Qout, label="Output flow")
plt.xlabel("Time")
plt.ylabel("flow")
plt.legend()
plt.savefig("./outputfiles/flow.png")

file_.close()
