#ifndef READNETWORK_OMP_H   
#define READNETWORK_OMP_H

#ifdef _OPENMP
#include <omp.h>                        /* added by Ce Xi */
#endif

#include <gsl/gsl_errno.h>             /* access to GSL cubic spline library     */
#include <gsl/gsl_spline.h>            /* access to GSL cubic spline library     */

typedef struct {

  SUNMatrix network_mat; // Network matrix
  SUNMatrix param_mat; // Parameter matrix
  N_Vector long_dist_bp, long_dist_ap, long_dist_cp, long_dist_php; // Passive properties
  N_Vector long_dist_rhoa, long_dist_pha, long_dist_ca, long_dist_ma, long_dist_fmax, long_dist_ktau, long_dist_ka, long_dist_a;  // Active properties
  N_Vector vessel_length, deltaP, Rpt, dRpdP, G_vessels, C_vessels;
  N_Vector flow_in, flow_out, flow_net;
  N_Vector y_prev, dydt;
  N_Vector pt, dptdt;

  int nvessels;
  int nthreads; // added by Ce Xi
  double viscosity;
  double pa_const;
  double pv_const;

  //double pt_const;
  //double dptdt_const;

  int *ind1, *ind2, *ind3, *ind8, *ind9, *ind10;
  int ind1_cnt, ind2_cnt, ind3_cnt, ind8_cnt, ind9_cnt, ind10_cnt;
 
  int *ind_term_vessel;
  int ind_term_vessel_cnt;
  
} UserData;
 

void readnetwork(const char *filename, UserData *data, int ncols, int nthreads);
void vesselparam(UserData* data);
void getterminalvessel(UserData* data, int** outArray1, int* outLen1);

#define Ith(v,i)    NV_Ith_OMP(v,i-1)         /* Ith numbers components 1..NEQ */
#define IJth(A,i,j) SM_ELEMENT_D(A,i-1,j-1) /* IJth numbers rows,cols 1..NEQ */
#define COLS(A)     SM_COLS_D(A)            /* Get Column Data of a Dense Matrix */
#define COLUMN(A)   		SM_COLUMN_D(A,j)



#endif 
