from matplotlib import pylab as plt
import numpy as np
import math
import coronaryflow_asymmetric_fn_omp
import readnetwork_omp
import computeflow_omp



#Time varying elastance function ################################
def et(t, Tmax, tau):
	if (t <= 1.5*Tmax):
		out = 0.5*(math.sin((math.pi/Tmax)*t - math.pi/2) + 1);
    	else:
		out = 0.5*math.exp((-t + (1.5*Tmax))/tau);

	return out        
#################################################################
 

cycle = 1
stop_iter = 6
dt = 2
BCL = 400

# Normal setting
Ees_la = 10;
A_la = 2.67;
B_la=  0.019;
V0_la = 10;
Tmax_la = 120;
tau_la = 25;
tdelay_la = 160;

Ees_lv = 500;
A_lv = 58.67;
B_lv = 0.029;
V0_lv = 20;
Tmax_lv = 150;
tau_lv = 25;

Cao = 0.0052
Cven = 0.28
Cad = 0.033
Vart0 = 400
Vad0 = 40
Vven0 = 3250.0
Rao = 500.0
Rven = 100.0
Rper = 7000.0
Rad = 106000
Rmv = 200.0
V_ven = 3700
V_art = 740 
V_ad = 100
V_LA = 12
V_cav = 50

# HFpEF setting
isHFpEF = False
if(isHFpEF):
	tau_lv = 100
	A_lv = 206.7
	Vven0 = 2025
	Vart0 = 590

# IMP
alpha = 0.5

tstep = 0

PLV_array = []
Pven_array = []
Part_array = []
VLV_array = []
Qlad_array = []
t_array = []

#networkfilename = "./inputfiles/Tree_8609_Vessels.txt";
#networkfilename = "./inputfiles/network_matrix_test_8634vessel_23.txt"
networkfilename = "./inputfiles/network_matrix_400vessel.txt";
ncols = 23

# Parallel version
nthreads = 1
sim_data = readnetwork_omp.UserData();
readnetwork_omp.readnetwork(networkfilename, sim_data, ncols, nthreads) # OMP version (need number of threads)
#postprocess_flow.postprocess_flow().write_grid("./outputfiles/vessels.vtk", sim_data);
ode_obj = coronaryflow_asymmetric_fn_omp.createODE(sim_data);


termvessel_indices =  readnetwork_omp.getterminalvessel(sim_data)


# Initial condition for coronary flow
Lad_P0 = np.ones(sim_data.nvessels,dtype=np.double)*0.005433149918032
PLV_prev = et(0,Tmax_lv,tau_lv)*Ees_lv*(V_cav - V0_lv) + (1 - et(0,Tmax_lv,tau_lv))*A_lv*(math.exp(B_lv*(V_cav - V0_lv)) - 1);


while(1):

    tstep = tstep + dt
    cycle = math.floor(tstep/BCL)
    t = tstep - cycle*BCL

    Part = 1.0/Cao*(V_art - Vart0);
    Pad  = 1.0/Cad*(V_ad - Vad0); 
    Pven = 1.0/Cven*(V_ven - Vven0);
    PLV = et(t,Tmax_lv,tau_lv)*Ees_lv*(V_cav - V0_lv) + (1 - et(t,Tmax_lv,tau_lv))*A_lv*(math.exp(B_lv*(V_cav - V0_lv)) - 1);


    print("Cycle number = "+str(cycle)+" cell time = "+str(t)+" tstep = "+str(tstep)+" dt = "+str(dt))

    
    if (t < BCL - tdelay_la):
    	t_la = t + tdelay_la;
    else: 
    	t_la = t - BCL + tdelay_la;

    print("t_LA = "+str(t_la)+" t_delay_LA = "+str(tdelay_la)+" BCL = "+str(BCL)+" t = "+str(t))

    PLA = et(t_la,Tmax_la,tau_la)*Ees_la*(V_LA - V0_la) + (1 - et(t_la,Tmax_la,tau_la))*A_la*(math.exp(B_la*(V_LA - V0_la)) - 1);
    ##################################################################################################################################

    print("P_ven = "+str(Pven*0.0075))
    print("P_LV = " +str(PLV*0.0075 ))
    print("P_art = "+str(Part*0.0075))
    print("P_LA = " +str(PLA*0.0075 ))

    #### conditions for Valves#######################################
    if(PLV <= Part):
         Qao = 0.0;
    else:
         Qao = 1.0/Rao*(PLV - Part);
    
    
    if(PLV >= PLA):
        Qmv = 0.0;
    else: 
        Qmv = 1.0/Rmv*(PLA - PLV);

    Qper = 1.0/Rper*(Part - Pad);
    Qad = 1.0/Rad*(Pad - Pven);
    Qla = 1.0/Rven*(Pven - PLA);

    Pin = Part/1e6
    Pout = Pven/1e6

    sim_data.pa_const = Pin 
    sim_data.pv_const = Pout

    tstep_sec = (tstep)/1000.0
    tstep_prev_sec = (tstep - dt)/1000.0
    dt_sec = dt/1000.0

    # Compute dP/dt, Pt
    pt = alpha*PLV/1e6*np.ones(sim_data.nvessels,dtype=np.double)
    dptdt = alpha*(PLV  - PLV_prev)/(1e6*dt_sec)*np.ones(sim_data.nvessels,dtype=np.double) 

    # Compute pressures in the vessels (parallel version)
    Lad_P = coronaryflow_asymmetric_fn_omp.advanceODE(sim_data, ode_obj, tstep_prev_sec, tstep_sec, Lad_P0, pt, dptdt)

    # Get dPdt
    dLad_Pdt = (Lad_P - Lad_P0)/(dt_sec)

    # Compute flow in the vessels (parallel version)
    [Qin, Qout] = computeflow_omp.computeflow(sim_data, Lad_P, dLad_Pdt) # Q is in ml/s

    Qlad = Qin[0]/1000.0 # Convert Qin to ml/ms
    Qlad_out = sum(Qout[termvessel_indices])/1000.0 # Terminal vessels outflow rate


    # Update initial condition with current solution
    Lad_P0 = Lad_P

    #Qlad = 0

    print("Q_LA = " + str(Qla ))
    print("Q_ao = " + str(Qao ))
    print("Q_per = "+ str(Qper))
    print("Q_mv = " + str(Qmv ))
    print("Qlad = " + str(Qlad ))

    V_cav = V_cav + dt*(Qmv - Qao);
    V_art = V_art + dt*(Qao - Qper- Qlad);# - Qlcx);
    V_ad  = V_ad +  dt*(Qper - Qad); 
    #V_ven = V_ven + dt*(Qad - Qla + Qlad_out);# + Qlcx 
    V_ven = V_ven + dt*(Qad - Qla + Qlad);# + Qlcx 
    V_LA  = V_LA  + dt*(Qla - Qmv);

    V_cav_prev = V_cav
    V_art_prev = V_art
    V_ven_prev = V_ven
    V_ad_prev = V_ad

    PLV_prev = PLV

    PLV_array.append(PLV*0.0075)
    Part_array.append(Part*0.0075)
    Pven_array.append(Pven*0.0075)
    VLV_array.append(V_cav)
    t_array.append(tstep)
    Qlad_array.append(Qlad*1000)

    if(cycle > stop_iter):
        break;



t_array = np.array(t_array)
PLV_array = np.array(PLV_array)
Part_array = np.array(Part_array)
Pven_array = np.array(Pven_array)
VLV_array = np.array(VLV_array)
Qlad_array = np.array(Qlad_array)
ind = np.where((t_array>(stop_iter-2)*BCL)*(t_array<(stop_iter)*BCL))[0]

total_Qlad = 0
for p in range(0, len(ind)):
	total_Qlad += Qlad_array[ind[p]]*(t_array[ind[p]] - t_array[ind[p]-1])/1000.0

print "Total coronary flow = ", total_Qlad
print "EDV = ", np.max(VLV_array[ind])
print "EDP = ", PLV_array[ind[np.argmax(VLV_array[ind])]]
print "SV = ", np.max(VLV_array[ind]) - np.min(VLV_array[ind])
print "EF = ", (np.max(VLV_array[ind]) - np.min(VLV_array[ind]))/np.max(VLV_array[ind])*100


if(not isHFpEF):
	np.savez('normal.npz', t_array = t_array[ind], \
	                       PLV_array=PLV_array[ind], \
	                       Part_array=Part_array[ind], \
	                       Pven_array=Pven_array[ind], \
	                       VLV_array = VLV_array[ind], \
	                       Qlad_array = Qlad_array[ind])

fig, (ax1, ax2, ax3) = plt.subplots(3)
fig.suptitle('Results')
ax1.plot(t_array[ind] - t_array[ind[0]], PLV_array[ind], 'b--')
ax1.plot(t_array[ind] - t_array[ind[0]], Part_array[ind], 'b--')
ax1.plot(t_array[ind] - t_array[ind[0]], Pven_array[ind], 'b--')
ax1.set_xticks([])
ax1.set_ylabel("LV Pressure (mmHg)")

ax2.plot(t_array[ind] - t_array[ind[0]], VLV_array[ind], 'b--')
ax2.set_ylabel("LV Volume (ml)")
ax2.set_xticks([])

ax3.plot(t_array[ind] - t_array[ind[0]], Qlad_array[ind], 'b--')
ax3.set_ylabel("Qlad (ml/s)")
ax3.set_xlabel("Time (ms)")

if(isHFpEF):
	data = np.load('normal.npz')
	ax1.plot(data["t_array"] - data["t_array"][0], data["PLV_array"], 'r-')
	ax1.plot(data["t_array"] - data["t_array"][0], data["Part_array"], 'r-')
	ax1.plot(data["t_array"] - data["t_array"][0], data["Pven_array"], 'r-')

	ax2.plot(data["t_array"] - data["t_array"][0], data["VLV_array"], 'r-')

	ax3.plot(data["t_array"] - data["t_array"][0], data["Qlad_array"], 'r-')

plt.savefig("./outputfiles/Result.png")

plt.figure(3)
plt.plot(VLV_array[ind], PLV_array[ind], 'b--')
if(isHFpEF):
	data = np.load('normal.npz')
	plt.plot(data["VLV_array"], data["PLV_array"], 'r-')

plt.savefig("./outputfiles/PV.png")


