#include "computeflow_omp.h"

#ifndef max
    #define max(a,b) ((a) > (b) ? (a) : (b))
#endif
#define PI     RCONST(3.141592653589793)


void computeflow(UserData *data, double* inArray1, int inLen1, double* inArray2, int inLen2, double** outArray1, int* outLen1, double** outArray2, int* outLen2){

  //double Pt = data->pt_const;
  //double dPtdt = data->dptdt_const;
  int _outLen1 = data->nvessels;
  double* _outArray1 = (double*)malloc(_outLen1*sizeof(double));
  int _outLen2 = data->nvessels;
  double* _outArray2 = (double*)malloc(_outLen1*sizeof(double));

  N_Vector y, dPdt;

  y = N_VNew_OpenMP(data->nvessels, data->nthreads);
  dPdt = N_VNew_OpenMP(data->nvessels, data->nthreads);

  /* Initialize */
  int j;
#pragma omp parallel for shared(y, dPdt) private(j) num_threads(data->nthreads) schedule(static, 4) 
  for(j = 0; j<(data->nvessels); ++j){
  	Ith(y,j+1) = inArray1[j];
  	Ith(dPdt,j+1) = inArray2[j];
  };


  // Set up nm matrix (This can be done once)
  N_Vector work1; work1 = N_VNew_OpenMP(data->nvessels, data->nthreads);
  N_Vector work2; work2 = N_VNew_OpenMP(data->nvessels, data->nthreads);
  N_VSetArrayPointer(SM_COLUMN_D(data->network_mat,4), work1);
  N_VSetArrayPointer(SM_COLUMN_D(data->network_mat,5), work2);
  int nnodes = max(int(N_VMaxNorm(work1)), int(N_VMaxNorm(work2)));

  SUNMatrix nm; 
  nm = SUNDenseMatrix(nnodes, 4); 
  realtype* nmdata;
  nmdata = SUNDenseMatrix_Data(nm);

  realtype *work1_v,  *work2_v;
  work1_v = NV_DATA_OMP(work1);
  work2_v = NV_DATA_OMP(work2);

  realtype *pt_v, *dptdt_v;
  pt_v =  NV_DATA_OMP(data->pt);
  dptdt_v =  NV_DATA_OMP(data->dptdt);


  for(int i = 0; i < nnodes; ++i){
      int cnt = 0;
      for(int j = 0; j < data->nvessels; ++j){
	if(Ith(work1,j+1) == i+1){
		nmdata[cnt*(nnodes) + i] = j+1;
		++cnt;
        };
      };
      for(int j = 0; j < data->nvessels; ++j){
	if(Ith(work2,j+1) == i+1){
		nmdata[cnt*(nnodes) + i] = j+1;
		++cnt;
        };
      };
  };
  //N_VPrint_Serial(y);


  N_Vector nodal_p, nodal_flow_in, nodal_flow_out, nodal_flow_net; 
  data->flow_in = N_VNew_OpenMP(data->nvessels, data->nthreads);
  data->flow_out = N_VNew_OpenMP(data->nvessels, data->nthreads);
  data->flow_net = N_VNew_OpenMP(data->nvessels, data->nthreads);
  dPdt = N_VNew_OpenMP(data->nvessels, data->nthreads);

  nodal_p = N_VNew_OpenMP(nnodes, data->nthreads);
  nodal_flow_in = N_VNew_OpenMP(nnodes, data->nthreads);
  nodal_flow_out = N_VNew_OpenMP(nnodes, data->nthreads);
  nodal_flow_net = N_VNew_OpenMP(nnodes, data->nthreads);

  realtype *G_vessels_v, *C_vessels_v;
  realtype *Rpt_v, *dRpdP_v, *deltaP_v, *y_v, *dPdt_v;
  realtype *bp_v, *ap_v, *cp_v, *php_v, *vessel_length_v;
  realtype *nodal_p_v, *nodal_flow_in_v, *nodal_flow_out_v, *nodal_flow_net_v;
  realtype *flow_in_v, *flow_out_v, *flow_net_v;

  G_vessels_v = NV_DATA_OMP(data->G_vessels);
  C_vessels_v = NV_DATA_OMP(data->C_vessels); 
  bp_v = NV_DATA_OMP(data->long_dist_bp);
  ap_v = NV_DATA_OMP(data->long_dist_ap);
  cp_v = NV_DATA_OMP(data->long_dist_cp);
  php_v = NV_DATA_OMP(data->long_dist_php);
  vessel_length_v = NV_DATA_OMP(data->vessel_length);
  deltaP_v = NV_DATA_OMP(data->deltaP);
  Rpt_v = NV_DATA_OMP(data->Rpt); 
  dRpdP_v = NV_DATA_OMP(data->dRpdP); 
  y_v = NV_DATA_OMP(y); 
  dPdt_v = NV_DATA_OMP(dPdt);

  nodal_p_v = NV_DATA_OMP(nodal_p);
  nodal_flow_in_v = NV_DATA_OMP(nodal_flow_in);
  nodal_flow_out_v = NV_DATA_OMP(nodal_flow_out);
  nodal_flow_net_v = NV_DATA_OMP(nodal_flow_net);

  flow_in_v = NV_DATA_OMP(data->flow_in);
  flow_out_v = NV_DATA_OMP(data->flow_out);
  flow_net_v = NV_DATA_OMP(data->flow_net);


  realtype **network_matdata;
  network_matdata = SUNDenseMatrix_Cols(data->network_mat);
 
  int i;
#pragma omp parallel for shared(deltaP_v,y_v,Rpt_v,dRpdP_v,bp_v,ap_v,cp_v,php_v,vessel_length_v,C_vessels_v,G_vessels_v, pt_v, network_matdata) private(i) num_threads(data->nthreads) schedule(static, 4)  
  for(i = 0; i < data->nvessels; ++i){
       deltaP_v[i] = y_v[i] - pt_v[i]*network_matdata[16][i]; 

       /* Compute Radius */
       Rpt_v[i] = bp_v[i] + (ap_v[i] - bp_v[i])/PI*(PI/2.0 + atan((deltaP_v[i] - php_v[i])/cp_v[i]));
       //printf("Rpt_v[%d] = %lg\n", i, Rpt_v[i]);

       dRpdP_v[i] = (ap_v[i] - bp_v[i])/(PI*cp_v[i]*(1.0 + pow((deltaP_v[i] - php_v[i])/cp_v[i], 2.0)));
       //printf("dRpdP[%d] = %lg\n", i, dRpdP_v[i]);

       /* Compute hydraulic conductance at each time point */
       G_vessels_v[i] = PI*(Rpt_v[i]*Rpt_v[i]*Rpt_v[i]*Rpt_v[i])/(8.0*data->viscosity*vessel_length_v[i]*1e-3);
       //printf("G_vessels[%d] = %lg\n", i, G_vessels_v[i]);

       /* Compute capacitance at each time point */
       C_vessels_v[i] = PI*2.0*1.0e-3*Rpt_v[i]*dRpdP_v[i]*vessel_length_v[i];
       //printf("C_vessels[%d] = %lg\n", i, C_vessels_v[i]);

  };
  //N_VPrint_Serial(data->C_vessels);
  //N_VPrint_Serial(data->G_vessels);
  //N_VPrint_Serial(y);

  double Pin = data->pa_const;
  double Pout = data->pv_const;
  nodal_p_v[0] = Pin;

#pragma omp parallel for shared(nodal_p_v) private(j) num_threads(data->nthreads) schedule(static, 4)  
  for(j = 1; j < nnodes; ++j){ 
	int nnonzero = 0;
	for(int i = 0; i < 4; ++i){
		if(IJth(nm,j+1,i+1) > 0){
			++nnonzero;
		};
	};
	if(nnonzero == 1){
		nodal_p_v[j] = Pout;
        }else if(nnonzero == 2){
		nodal_p_v[j] = (y_v[int(IJth(nm,j+1,1))-1]*G_vessels_v[int(IJth(nm,j+1,1))-1]+y_v[int(IJth(nm,j+1,2))-1]*G_vessels_v[int(IJth(nm,j+1,2))-1])/
                               (G_vessels_v[int(IJth(nm,j+1,1))-1]+G_vessels_v[int(IJth(nm,j+1,2))-1]);
		//printf("%10.5f\n", nodal_p_v[j]);
	}else if(nnonzero == 3){
		nodal_p_v[j] = (y_v[int(IJth(nm,j+1,1))-1]*G_vessels_v[int(IJth(nm,j+1,1))-1]+y_v[int(IJth(nm,j+1,2))-1]*G_vessels_v[int(IJth(nm,j+1,2))-1]+ 
                                y_v[int(IJth(nm,j+1,3))-1]*G_vessels_v[int(IJth(nm,j+1,3))-1])/(G_vessels_v[int(IJth(nm,j+1,1))-1]+
                                G_vessels_v[int(IJth(nm,j+1,2))-1]+G_vessels_v[int(IJth(nm,j+1,3))-1]);
		//printf("%10.5f\n", nodal_p_v[j]);
	}else{
		nodal_p_v[j] = (y_v[int(IJth(nm,j+1,1))-1]*G_vessels_v[int(IJth(nm,j+1,1))-1]+y_v[int(IJth(nm,j+1,2))-1]*G_vessels_v[int(IJth(nm,j+1,2))-1]+
                                y_v[int(IJth(nm,j+1,3))-1]*G_vessels_v[int(IJth(nm,j+1,3))-1]+y_v[int(IJth(nm,j+1,4))-1]*G_vessels_v[int(IJth(nm,j+1,4))-1])/ 
                                (G_vessels_v[int(IJth(nm,j+1,1))-1]+G_vessels_v[int(IJth(nm,j+1,2))-1]+
                                 G_vessels_v[int(IJth(nm,j+1,3))-1]+G_vessels_v[int(IJth(nm,j+1,4))-1]);
	};
	

  };
  //N_VPrint_Serial(nodal_p);

#pragma omp parallel for shared(nodal_flow_in_v, nodal_flow_out_v, nodal_flow_net_v) private(j) num_threads(data->nthreads) schedule(static, 4)  
  for(j = 1; j < nnodes /*3*/; ++j){ //nnodes;
	int nnonzero = 0;
	for(int i = 0; i < 4; ++i){
		if(IJth(nm,j+1,i+1) > 0){
			++nnonzero;
		};
	};
	//printf("%d %d\n", j, nnonzero);
	if(nnonzero == 1){
		nodal_flow_in_v[j] = 0.0;
		nodal_flow_out_v[j] = 0.0;
		nodal_flow_net_v[j] = 0.0;
        }else if(nnonzero == 2){
      		nodal_flow_in_v[j] = 2*(y_v[int(IJth(nm,j+1,2))-1]-nodal_p_v[j])*G_vessels_v[int(IJth(nm,j+1,2))-1];
    		nodal_flow_out_v[j] = 2*(nodal_p_v[j]-y_v[int(IJth(nm,j+1,1))-1])*G_vessels_v[int(IJth(nm,j+1,1))-1];
    		nodal_flow_net_v[j] = (nodal_flow_in_v[j] - nodal_flow_out_v[j])*100./nodal_flow_in_v[j];
	}else if(nnonzero == 3){
		nodal_flow_in_v[j] = 2*(y_v[int(IJth(nm,j+1,3))-1]-nodal_p_v[j])*G_vessels_v[int(IJth(nm,j+1,3))-1];
    		nodal_flow_out_v[j] = 2*(nodal_p_v[j]-y_v[int(IJth(nm,j+1,1))-1])*G_vessels_v[int(IJth(nm,j+1,1))-1]+
    		                      2*(nodal_p_v[j]-y_v[int(IJth(nm,j+1,2))-1])*G_vessels_v[int(IJth(nm,j+1,2))-1];
    		nodal_flow_net_v[j] = (nodal_flow_in_v[j] - nodal_flow_out_v[j])*100./nodal_flow_in_v[j];

	}else{
		nodal_flow_in_v[j] = 2*(y_v[int(IJth(nm,j+1,4))-1]-nodal_p_v[j])*G_vessels_v[int(IJth(nm,j+1,4))-1];
    		nodal_flow_out_v[j] = 2*(nodal_p_v[j]-y_v[int(IJth(nm,j+1,1))-1])*G_vessels_v[int(IJth(nm,j+1,1))-1]+
    		                      2*(nodal_p_v[j]-y_v[int(IJth(nm,j+1,2))-1])*G_vessels_v[int(IJth(nm,j+1,2))-1]+
    		                      2*(nodal_p_v[j]-y_v[int(IJth(nm,j+1,3))-1])*G_vessels_v[int(IJth(nm,j+1,3))-1];
    		nodal_flow_net_v[j] = (nodal_flow_in_v[j] - nodal_flow_out_v[j])*100./nodal_flow_in_v[j];
	};
	

  };
  //N_VPrint_Serial(nodal_flow_net);

  N_VSetArrayPointer(SM_COLUMN_D(data->network_mat,4), work1);
  N_VSetArrayPointer(SM_COLUMN_D(data->network_mat,5), work2);

#pragma omp parallel for shared(flow_in_v, _outArray1, flow_out_v, _outArray2, dptdt_v, network_matdata) private(j) num_threads(data->nthreads) schedule(static, 4)  
  for(j = 0; j < data->nvessels; ++j){ 
	flow_in_v[j]=  2*(nodal_p_v[int(Ith(work1,j+1))-1]-y_v[j])*G_vessels_v[j]; 
        _outArray1[j] = flow_in_v[j];

        flow_out_v[j] = 2*(y_v[j]-nodal_p_v[int(Ith(work2,j+1))-1])*G_vessels_v[j] + C_vessels_v[j]*(dPdt_v[j] - dptdt_v[j]*network_matdata[16][j]);
        _outArray2[j] = flow_out_v[j];

        flow_net_v[j] = flow_in_v[j] - flow_out_v[j];

  };

  // Assign outputs
  *outLen1 = _outLen1;
  *outArray1 = _outArray1;
  *outLen2 = _outLen2;
  *outArray2 = _outArray2;





};

