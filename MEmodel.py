# Electrophysiology model using Aliev Panilfov model
from dolfin import *                
import numpy as np
from nsolver import NSolver as NSolver
from oops_objects_MRC import biventricle_mesh as biv_mechanics_mesh
from oops_objects_MRC import lv_mesh as lv_mechanics_mesh
from oops_objects_MRC import PV_Elas
from oops_objects_MRC import update_mesh
from edgetypebc import *
from forms_MRC import Forms
from heArt.mesh_partitionMeshforEP_J import defCPP_Matprop, defCPP_Matprop_DIsch
from heArt.mesh_scale_create_fiberFiles import create_EDFibers

class MEmodel(object):

    def __init__(self, params, SimDet):

        self.parameters = self.default_parameters()
        self.parameters.update(params)
	self.SimDet = SimDet
        self.isLV = SimDet["isLV"]
	self.deg_me = SimDet["GiccioneParams"]["deg"]

   	if(self.isLV):
   		self.Mesh = lv_mechanics_mesh(self.parameters, SimDet) 
   	else:
   		self.Mesh = biv_mechanics_mesh(self.parameters, SimDet) 

    	f0_me_Gauss = self.Mesh.f0
    	s0_me_Gauss = self.Mesh.s0
    	n0_me_Gauss = self.Mesh.n0

    	self.mesh_me = self.Mesh.mesh
        self.facetboundaries_me = self.Mesh.facetboundaries 
        self.edgeboundaries_me = self.Mesh.edgeboundaries 

        #Project fiber to CG space as IMP calculation is unable to take f0 at Gauss point
        #self.f0_me = project(f0_me_Gauss, VectorFunctionSpace(self.mesh_me, "CG", 1))
        #self.f0_me = f0_me_Gauss/sqrt(inner(f0_me_Gauss, f0_me_Gauss))
        #self.n0_me = project(n0_me_Gauss, VectorFunctionSpace(self.mesh_me, "CG", 1))
        #self.n0_me = n0_me_Gauss/sqrt(inner(n0_me_Gauss, n0_me_Gauss))
        #self.s0_me = project(s0_me_Gauss, VectorFunctionSpace(self.mesh_me, "CG", 1))
        #self.s0_me = s0_me_Gauss/sqrt(inner(s0_me_Gauss, s0_me_Gauss))
	
        self.f0_me = f0_me_Gauss
        self.s0_me = s0_me_Gauss
        self.n0_me = n0_me_Gauss

    	self.LVCavityvol = Expression(("vol"), vol=0.0, degree=2)
    	self.RVCavityvol = Expression(("vol"), vol=0.0, degree=2)

        self.isincomp = SimDet["GiccioneParams"]["incompressible"]

        #  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - -

        Velem = VectorElement("CG", self.mesh_me.ufl_cell(), 2, quad_scheme="default")
        Qelem = FiniteElement("CG", self.mesh_me.ufl_cell(), 1, quad_scheme="default")
        Qelem._quad_scheme = 'default'
        Relem = FiniteElement("Real", self.mesh_me.ufl_cell(), 0, quad_scheme="default")
        Relem._quad_scheme = 'default'
        Quadelem = FiniteElement("Quadrature", self.mesh_me.ufl_cell(), degree=self.deg_me, quad_scheme="default")
        Quadelem._quad_scheme = 'default'
        #  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - -

        #  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - -
        Telem2 = TensorElement("Quadrature", self.mesh_me.ufl_cell(), degree=self.deg_me, shape=2*(3,), quad_scheme='default')
        Telem2._quad_scheme = 'default'
        for e in Telem2.sub_elements():
            e._quad_scheme = 'default'
        Telem4 = TensorElement("Quadrature", self.mesh_me.ufl_cell(), degree=self.deg_me, shape=4*(3,), quad_scheme='default')
        Telem4._quad_scheme = 'default'
        for e in Telem4.sub_elements():
            e._quad_scheme = 'default'
        #  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - -

        # Mixed Element for rigid body motion 
        #  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - -
        VRelem = MixedElement([Relem, Relem, Relem, Relem, Relem])
        #  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - -

        #  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - -
        if(self.isincomp): 
	    if(self.isLV):
            	self.W = FunctionSpace(self.mesh_me, MixedElement([Velem, Qelem, Relem, VRelem]))
	    else:
            	self.W = FunctionSpace(self.mesh_me, MixedElement([Velem, Qelem, Relem, Relem, VRelem]))
               #W = FunctionSpace(mesh, MixedElement([(Velem+Belem), Qelem, Relem, Relem, VRelem]))
        else:
	    if(self.isLV):
            	self.W = FunctionSpace(self.mesh_me, MixedElement([Velem, Relem, VRelem]))
	    else:
            	self.W = FunctionSpace(self.mesh_me, MixedElement([Velem, Relem, Relem, VRelem]))

        self.Quad = FunctionSpace(self.mesh_me, Quadelem)
        self.TF = FunctionSpace(self.mesh_me, Telem2)
        self.Q = FunctionSpace(self.mesh_me,'CG',1)
        #self.W = W
        #self.Q = Q
        #self.TF = TF
        # this displacement is for FHN 
        #W_n = Function(W)
        #Ve = VectorFunctionSpace(mesh, 'CG', 2)
        #self.we_n = Function(Ve)

        self.we_n = Function(self.W.sub(0).collapse())

    	self.w_me = Function(self.W)
    	self.dw_me = TrialFunction(self.W)
    	self.wtest_me = TestFunction(self.W)


	self.Ftotal, self.Jac, self.bcs = self.Problem()



    def default_parameters(self):
        return {
		"probeloc": [3.5, 0.0, -2.0]
                };

    def set_BCs(self):
        #  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - -
        # Using bubble element 
        #  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - -
        #baseconstraint = project(Expression(("0.0"), degree=2), W.sub(0).sub(2).collapse())
        #bctop = DirichletBC(W.sub(0).sub(2), baseconstraint, facetboundaries, topid)
        #  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - -

        facetboundaries = self.facetboundaries_me
        edgeboundaries = self.edgeboundaries_me
        topid = self.SimDet["topid"] 
        W = self.W
        
        bctop = DirichletBC(W.sub(0).sub(2), Expression(("0.0"), degree = 2), facetboundaries, topid)

        endoring = pick_endoring_bc(method="cpp")(edgeboundaries, 1)

        bcedge = DirichletBC(W.sub(0), Expression(("0.0", "0.0", "0.0"), degree = 0), endoring, method="pointwise")
    	if("springbc" in self.SimDet.keys() and self.SimDet["springbc"]):
        	bcs = [bctop]
	else:
        	bcs = [bctop]#, bcedge]
        #bcs = [] # changing top constraint

        return bcs 
        #  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - -


    def Problem(self):

    	GuccioneParams = self.SimDet["GiccioneParams"]
        topid = self.SimDet["topid"]
        LVendoid = self.SimDet["LVendoid"]
        RVendoid = self.SimDet["RVendoid"]
        epiid = self.SimDet["epiid"]
    	isincomp = GuccioneParams["incompressible"]
        deg_me = GuccioneParams["deg"]


    	mesh_me = self.mesh_me
	facetboundaries_me = self.facetboundaries_me
	f0_me = self.f0_me
	s0_me = self.s0_me
	n0_me = self.n0_me

    	N_me = FacetNormal (mesh_me)
    	W_me = self.W
    	Q_me = self.Q
    	TF_me = self.TF

    	w_me = self.w_me
    	dw_me = self.dw_me
    	wtest_me = self.wtest_me

    	bcs_elas = self.set_BCs() 

	if(isincomp): 
	    if(self.isLV):
	    	du, dp, dlv_pendo, dc = TrialFunctions(W_me)
	    	(u_me, p_me, lv_pendo, c_me) = split(w_me)
	    	(v_me, q_me, lv_qendo, cq) = TestFunctions(W_me)
	    	rv_pendo = []
	    	LVendo_comp = 2
	    	RVendo_comp = 1000
	
	    else:
	    	du, dp, dlv_pendo, drv_pendo, dc = TrialFunctions(W_me)
	    	(u_me, p_me, lv_pendo, rv_pendo, c_me) = split(w_me)
	    	(v_me, q_me, lv_qendo, rv_qendo, cq) = TestFunctions(W_me)
	    	LVendo_comp = 2
	    	RVendo_comp = 3
	else:
	    if(self.isLV):
		du, dlv_pendo, dc = TrialFunctions(W_me)
		(u_me, lv_pendo, c_me) = split(w_me)
		(v_me, lv_qendo, cq) = TestFunctions(W_me)
		p_me = Function(Q_me)
		rv_pendo = []
		LVendo_comp = 1
		RVendo_comp = 1000
	    else:
		du, dlv_pendo, drv_pendo, dc = TrialFunctions(W_me)
		(u_me, lv_pendo, rv_pendo, c_me) = split(w_me)
		(v_me, lv_qendo, rv_qendo, cq) = TestFunctions(W_me)
		p_me = Function(Q_me)
		LVendo_comp = 1
		RVendo_comp = 2


        self.t_a = Function(self.Quad)
        self.t_a.vector()[:] = 0

        ls0 = 1.85 
        Tact = GuccioneParams["Tmax"]
        Cparam = GuccioneParams["C_param"]
        ds_me = self.Mesh.ds
        dx_me = self.Mesh.dx

        
        params= {"mesh": mesh_me,
             "facetboundaries": facetboundaries_me,
             "facet_normal": N_me,
             "mixedfunctionspace": W_me,
             "mixedfunction": w_me,
             "displacement_variable": u_me, 
             "pressure_variable": p_me,
             "lv_volconst_variable": lv_pendo,
             "lv_constrained_vol":self.LVCavityvol,
             "rv_volconst_variable": rv_pendo,
             "rv_constrained_vol":self.RVCavityvol,
             "LVendoid": LVendoid,
             "RVendoid": RVendoid,
             "epiid": epiid,
             "LVendo_comp": LVendo_comp,
             "RVendo_comp": RVendo_comp,
             "fiber": f0_me,
             "sheet": s0_me,
             "sheet-normal": n0_me,
             "growth_tensor": None,
             "C_param": Cparam,
             "bff"  : GuccioneParams["bff"],
             "bfx"  : GuccioneParams["bfx"],
             "bxx"  : GuccioneParams["bxx"],
             "Kappa": GuccioneParams["Kappa"],
             "incompressible" : GuccioneParams["incompressible"],
            }

        uflforms = Forms(params)
	self.uflforms = uflforms

        if("ActiveModel" in self.SimDet.keys()):
            if(self.SimDet["ActiveModel"] == "Time-varying"):
        		from BurkhoffTimevarying2 import BurkhoffTimevarying as Active
            else:
        		from Guccione_MRC import Guccione as Active
        else:
        	from Guccione_MRC import Guccione as Active


        activeparams = {"mesh": mesh_me,
                        "dx":dx_me,
                        "facetboundaries": facetboundaries_me,
                        "facet_normal": N_me,
                        "displacement_variable": u_me, 
                        "pressure_variable": p_me,
                        "endoid": self.SimDet["LVendoid"],
                        "fiber": f0_me,
                        "sheet": s0_me,
                        "sheet-normal": n0_me,
                        "t_a": self.t_a, # current time 
                        "dt": self.parameters["state_obj"].dt, # not used 
                        "Tmax": Tact,
                        "mesh": mesh_me, 
                        "Threshold_Potential": 0.9,
                        "growth_tensor": None,
                        "Ca0" : GuccioneParams["Ca0"],
                        "Ca0max" : GuccioneParams["Ca0max"],
                        "B" : GuccioneParams["B"], #4.75 no strech dependency seen, 
                        "t0" : GuccioneParams["t0"], 
#                        "tau" : GuccioneParams["tau"], 
#                        "t_trans" : GuccioneParams["t_trans"], 
                        "deg" : GuccioneParams["deg"],
                        "l0" : GuccioneParams["l0"], #1.58
                        "m" : GuccioneParams["m"],
                        "b" : GuccioneParams["b"], 
                        "Tmax" : GuccioneParams["Tmax"],
                       }

        if GuccioneParams["ParamsSpecified"] == True:
            activeparams.update(GuccioneParams)


        if("ActiveModel" in self.SimDet.keys()):
        	if(self.SimDet["ActiveModel"] == "Time-varying"):
	    		activeparams.update({"tau" : GuccioneParams["tau"]});
	    		activeparams.update({"t_trans" : GuccioneParams["t_trans"]});
	


        if self.SimDet["Ischemia"] == True:
            kParam = {}
            #mId_List = SimDet["mId_List"]
            kParam["kabNormal"] = self.SimDet["kabNormal"]
            kParam["kNormal"] = self.SimDet["kNormal"]
            kappA = defCPP_Matprop(mesh = mesh, mId = bivMesh.AHAid, k = kParam)
            activeparams["Tmax"] = kappA 

        activeforms = Active(activeparams)
	self.activeforms = activeforms

        F_ED = Function(TF_me)

        Fmat = uflforms.Fmat()
        Cmat = (Fmat.T*Fmat)
        Emat = uflforms.Emat() 
        J = uflforms.J()

        n_me = J*inv(Fmat.T)*N_me

        Wp_me = uflforms.PassiveMatSEF()
        LV_Wvol = uflforms.LVV0constrainedE()
        if(not self.isLV):
        	RV_Wvol = uflforms.RVV0constrainedE()

        Pactive = activeforms.PK1StressTensor()
        #printout("Total active force = " + str(assemble(activeforms.PK1Stress()*dx_me)), comm_me) 

        X_me = SpatialCoordinate(mesh_me)
        Wrigid = inner(as_vector([c_me[0], c_me[1], 0.0]), u_me) + \
                 inner(as_vector([0.0, 0.0, c_me[2]]), cross(X_me, u_me)) + \
                 inner(as_vector([c_me[3], 0.0, 0.0]), cross(X_me, u_me)) + \
                 inner(as_vector([0.0, c_me[4], 0.0]), cross(X_me, u_me))

        Kspring = Constant(50) 
        Kspringb  = Constant(100) 

        F1 = derivative(Wp_me, w_me, wtest_me)*dx_me
        if(self.isLV):
        	F2 = derivative(LV_Wvol, w_me, wtest_me)
        else:
        	F2 = derivative(LV_Wvol + RV_Wvol, w_me, wtest_me)
        F3 = -Kspring*inner(u_me,v_me)*ds_me(epiid) 
        F4 = inner(Fmat*Pactive, grad(v_me))*dx_me
        F5 = derivative(Wrigid, w_me, wtest_me)*dx_me
        #F6 = Kspringb*inner(u_me,v_me)*ds_me(topid)
        Ftotal = F1 + F2 + F4 #+ F5 #+ F6 + F3 + 

        if("springbc" in self.SimDet.keys() and self.SimDet["springbc"]):
            Ftotal += F3
        else:
            Ftotal += F5

        Jac1 = derivative(F1, w_me, dw_me) 
        Jac2 = derivative(F2, w_me, dw_me) 
        Jac3 = derivative(F3, w_me, dw_me) 
        Jac4 = derivative(F4, w_me, dw_me) 
        Jac5 = derivative(F5, w_me, dw_me)
        #Jac6 = derivative(F6, w_me, dw_me)
        Jac = Jac1 + Jac2 + Jac4 #+ Jac5 #+ Jac6 + Jac3 + 

        if("springbc" in self.SimDet.keys() and self.SimDet["springbc"]):
            Jac += Jac3
        else:
            Jac += Jac5

	# Initialize LV cavity volume
	self.LVCavityvol.vol = uflforms.LVcavityvol()


	if(not self.isLV):
    		self.RVP_cav = uflforms.RVcavitypressure()
        	self.RVV_cav = uflforms.RVcavityvol()





        return Ftotal, Jac, bcs_elas



    def Solver(self):

        solverparams = {"Jacobian": self.Jac,
                        "F": self.Ftotal,
                        "w": self.w_me,
                        "boundary_conditions": self.bcs,
            	        "Type": 0,  
            	        "mesh": self.mesh_me,
            	        "mode": 1
            	        }

    	if("abs_tol" in self.SimDet.keys()):
    		solverparams.update({"abs_tol":self.SimDet["abs_tol"]})
    	if("rel_tol" in self.SimDet.keys()):
    		solverparams.update({"rel_tol":self.SimDet["rel_tol"]})
 
        solver_eals = NSolver(solverparams)

	return solver_eals

    def GetDisplacement(self):

   	if(self.isLV):
		u, p, lv_pendo, self.c = self.w_me.split(deepcopy=True)
		rv_pendo = []
	else:
		u, p, lv_pendo, rv_pendo, self.c = self.w_me.split(deepcopy=True)

	u.rename("u_", "u_")

	return u

    def GetP(self):

   	if(self.isLV):
		u, p, lv_pendo, self.c = self.w_me.split(deepcopy=True)
		rv_pendo = []
	else:
		u, p, lv_pendo, rv_pendo, self.c = self.w_me.split(deepcopy=True)

	p.rename("p_", "p_")

	return p


    def GetFmat(self):

	return self.uflforms.Fmat()

    def GetFiberstrain(self, F_ref):

	return self.uflforms.fiberstrain(F_ref=F_ref)

    def GetFiberstrainUL(self):

        F_Identity = Identity(self.GetDisplacement().ufl_domain().geometric_dimension())
	return self.uflforms.fiberstrain(F_ref=F_Identity)

    def GetIMP(self):

	return self.uflforms.IMP()

    def GetIMP2(self):

	return self.uflforms.IMP2()

    def Getfstress(self):

	return self.uflforms.fiberstress() + self.activeforms.fiberstress()

    def GetFiberNaturalStrain(self, F_ED, basis_dir, AHA_segments):

	F_n = self.GetFmat()
	
	return self.activeforms.CalculateFiberNaturalStrain(F_ = F_n, F_ref = F_ED, e_fiber = basis_dir, VolSeg = AHA_segments)

    def GetFiberBiotStrain(self, F_ED, basis_dir, AHA_segments):

	F_n = self.GetFmat()
	
	return self.activeforms.CalculateFiberBiotStrain(F_ = F_n, F_ref = F_ED, e_fiber = basis_dir, VolSeg = AHA_segments)

    def GetFiberGreenStrain(self, F_ED, basis_dir, AHA_segments):

	F_n = self.GetFmat()
	
	return self.activeforms.CalculateFiberGreenStrain(F_ = F_n, F_ref = F_ED, e_fiber = basis_dir, VolSeg = AHA_segments)


    def GetLVP(self):

    	return self.uflforms.LVcavitypressure()

    def GetLVV(self):

    	return self.uflforms.LVcavityvol()

    def GetRVP(self):

    	return self.uflforms.RVcavitypressure()

    def GetRVV(self):

    	return self.uflforms.RVcavityvol()

    def GetDeformedBasis(self):

        #  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - -
    	mesh_me = self.mesh_me
        facetboundaries_me = self.facetboundaries_me
	deg_me = self.deg_me
        LVendoid = self.SimDet["LVendoid"]
        RVendoid = self.SimDet["RVendoid"]
        epiid = self.SimDet["epiid"]
	isLV = self.isLV

        meshDispFunc = VectorFunctionSpace(mesh_me, "CG", 1)
        VQuadelem_me = VectorElement("Quadrature", 
                                  mesh_me.ufl_cell(), 
                                  degree=deg_me, 
                                  quad_scheme="default")
        VQuadelem_me._quad_scheme = 'default'
        fiberFS = FunctionSpace(mesh_me, VQuadelem_me)

        meshDisplacement = project(self.GetDisplacement(), meshDispFunc)
        deformedMesh, deformedBoundary = update_mesh(mesh = mesh_me, \
                                                     displacement = meshDisplacement, \
                                                     boundaries = facetboundaries_me)

        outputfolder = self.parameters[ "outputfolder"]
	folderName = self.parameters["foldername"]

        EDmeshData = {"epiid": epiid,
             "rvid": RVendoid,
             "lvid": LVendoid,
             "LVangle": [0, 0],
             "Septangle": [0, 0],
             "RVangle": [0, 0],
             "isepiflip": False,
             "isendoflip": False,
             "iscaling": False, 
             "mesh": deformedMesh,
             "facets": deformedBoundary,
             "mFileName": outputfolder + folderName + '/deformation_unloadED/' ,
             "isLV": isLV
             }

        eCC_ED, eLL_ED, eRR_ED = create_EDFibers(EDmeshData)
        
        # Copy directional field from functionspace with mesh_me to deformedmesh
        eCC = Function(fiberFS)
        eRR = Function(fiberFS)
        eLL = Function(fiberFS)
        eCC.vector()[:] = eCC_ED.vector().array()[:]
        eRR.vector()[:] = eRR_ED.vector().array()[:]
        eLL.vector()[:] = eLL_ED.vector().array()[:]

	return eCC, eRR, eLL, deformedMesh, deformedBoundary














