# Guccione2 et al. 1993: Simple time varying elastance type active contraction model 
# This modification account for nonuniform spatial activation 
# to is a function of space, FE model using gauss points 

from dolfin import *                
import math
import numpy as np

class Guccione(object):

    def __init__(self, params):

        self.parameters = self.default_parameters()
        self.parameters.update(params)
        
        deg = self.parameters["deg"] 
        bivMesh = self.parameters["mesh"]
        #mesh = bivMesh.mesh
        mesh = bivMesh     

        Quadelem = FiniteElement("Quadrature", mesh.ufl_cell(), degree=deg, quad_scheme="default")
        Quadelem._quad_scheme = 'default'
        self.Quadelem = Quadelem
        self.Quad = FunctionSpace(mesh, Quadelem)


        self.t_init = self.get_t_init()
        self.isActive = self.get_isActive()
         
        print "t_init vector is :" 
        #print self.t_init.vector().array()

        print "isActive vector is :" 
        #print self.isActive.vector().array()

        isActive_write_elem = FunctionSpace(mesh, "CG", 1)
        self.isActive_write_elem = isActive_write_elem

        t_init_write_elem = FunctionSpace(mesh, "CG", 1)
        self.t_init_write_elem = t_init_write_elem

    def get_isActive(self):

        deg = self.parameters["deg"]
        mesh = self.parameters["mesh"]
         
        Quad = self.Quad
        isActive = Function(Quad)

        isActive_array = isActive.vector().array()
        isActive_array = np.zeros(len(isActive_array))

        isActive.vector()[:] = isActive_array

        return isActive



    def update_activationTime(self, potential_n, comm):
        '''
        if V[i] >= V_thres[i] and isActivation[i] == 0 
            then t0[i] = t_a 
        ''' 

        mesh = self.parameters["mesh"]

        V_thres = self.parameters["Threshold_Potential"] 
        current_ta = self.parameters["t_a"]
        current_ta_array = interpolate(current_ta, self.Quad).vector().array()

        t_init_array = self.t_init.vector().array() 
        isActive_array = self.isActive.vector().array()

        phi_n_interp = interpolate(potential_n, self.Quad)

        V_n_array = phi_n_interp.vector().array() 
        
        tol_isActive = 1E-1
        comm.Barrier()
        for idx, (vn, isactive, tinit, cur_t) in enumerate(zip(V_n_array, isActive_array, t_init_array, current_ta_array)): 
          
            if ( abs(isactive) <= tol_isActive ) and vn >= V_thres : 
                isActive_array[idx] = 1.0 
                t_init_array[idx] = current_ta_array[idx] 

        comm.Barrier()

	# If homogeneous ---------------------------------------------------
	isHomogenousActivation = self.parameters["HomogenousActivation"]
        if isHomogenousActivation: # activation at 10 ms 
            t_init_array = 0.0*np.ones(len(self.t_init.vector().array()))
	#-------------------------------------------------------------------


        self.t_init.vector()[:] = t_init_array
        self.isActive.vector()[:] = isActive_array

    def get_t_init(self):
        
        Quad = self.Quad

        t_init = Function(Quad)

        t_init_array = t_init.vector().array()
        t_init_array = 9999.0*np.ones(len(t_init_array))

        t_init.vector()[:] = t_init_array

        return t_init 


    def restart_t_init(self):
        self.t_init = self.get_t_init()
        self.isActive = self.get_isActive()


    def Fmat(self):
    
    	 u = self.parameters["displacement_variable"]
         d = u.ufl_domain().geometric_dimension()
         I = Identity(d)
         F = I + grad(u)
         return F

    def Fe(self):
        Fg = self.parameters["growth_tensor"]
        F = self.Fmat() 

        if (Fg is None):
            Fe = F
        else:
            Fe = as_tensor(F[i,j]*inv(Fg)[j,k], (i,k))
        return Fe

    def default_parameters(self):
        return {"Ca0" : 4.35,
		"Ca0max" : 4.35,
		"B" : 4.75, #4.75 no strech dependency seen, 
		"t0" : 250, 
                "deg" : 4,
                "l0" : 1.6, #1.58
                "m" : 290,
                "b" : -275, 
        	"Tmax" : 135.7e3 
                };

    def PK1StressTensor(self):

	F = self.Fmat()
	f0 = self.parameters["fiber"]
	Mij = f0[i]*f0[j]


	Pact = self.PK1Stress()

	Pact_tensor = Pact*as_tensor(Mij, (i,j)) # is there an F here? 

	return Pact_tensor

    def PK1Stress(self):

	Ca0 = self.parameters["Ca0"]
	Tmax = self.parameters["Tmax"]

	Ct = self.Ct()
	ECa = self.ECa()

	Pact = (Tmax*Ct*Ca0**2.0)/(Ca0**2.0 + ECa**2.0)

	return Pact

    def fiberstress(self):
	
	PK1 = self.PK1StressTensor()
	#F = self.Fmat()
        F = self.Fe()
	f0 = self.parameters["fiber"]
	J = det(F)

	Tca = (1.0/J)*PK1*F.T
	Sca = inv(F)*PK1
	#f = F*f0/sqrt(inner(F*f0, F*f0))

	#return f[i]*Tca[i,j]*f[j]
	return f0[i]*Sca[i,j]*f0[j]


#    Jay's original 
    def ECa(self):

	Ca0max = self.parameters["Ca0max"]
	B = self.parameters["B"]

	ls_l0 = self.ls_l0()
	denom = sqrt(exp(B*(ls_l0)) - 1)

	ECa = Ca0max/denom
	
	return ECa

#   Ravi
#    def ECa(self):
#
#	F = self.Fmat()
#	f0 = self.parameters["fiber"]
#	Ca0max = self.parameters["Ca0max"]
#	B = self.parameters["B"]
#	l0 = self.parameters["l0"]
#	t_a = self.parameters["t_a"]
#
#	Cmat = F.T*F
#	lmbda = sqrt(dot(f0, Cmat*f0))
#	ls = lmbda*1.85
#	ls_l0 = conditional(le(ls, l0+.002), 0.002, ls - l0)
#	denom = sqrt(exp(B*(ls_l0)) - 1)
#
#	ECa = Ca0max/denom
#	
#	return ECa



    def ls_l0(self):

	lmbda = self.lmbda()
	ls = lmbda*1.85
	l0 = self.parameters["l0"]
	ls_l0 = conditional(le(ls, l0+.002), 0.002, ls - l0)

	return ls_l0
	

    def lmbda(self):

        F = self.Fe()
	f0 = self.parameters["fiber"]
	Cmat = F.T*F
	lmbda = sqrt(dot(f0, Cmat*f0))

	return lmbda


    def tr(self):
	F = self.Fmat()
	f0 = self.parameters["fiber"]
	b = self.parameters["b"]
	m = self.parameters["m"]
	Cmat = F.T*F
	lmbda = self.lmbda()
	ls = lmbda*1.85

	tr = m*ls + b

	return tr

#    Jay's original
#    def Ct(self):
#
#	#F = self.Fmat()
#        F = self.Fe()
#	f0 = self.parameters["fiber"]
#	t0 = self.parameters["t0"]
#	b = self.parameters["b"]
#	m = self.parameters["m"]
#
#
#	Cmat = F.T*F
#	lmbda = sqrt(dot(f0, Cmat*f0))
#	ls = lmbda*1.85
#
#	w1 = self.w1()
#	w2 = self.w2()
#        Ct = 0.5*(1 - cos(w1+w2))
#
#	return Ct

#   Ravi
    def Ct(self):

	F = self.Fmat()
	f0 = self.parameters["fiber"]
	t0 = self.parameters["t0"]
	b = self.parameters["b"]
	m = self.parameters["m"]
	t_a = self.parameters["t_a"]

	Cmat = F.T*F
	lmbda = sqrt(dot(f0, Cmat*f0))
	ls = lmbda*1.85

	tr = self.tr()
	xp1 = conditional(lt(t_a,t0), 1.0, 0.0)
	#w1 = xp1*pi*t_a/t0
	w1 = self.w1()
      	xp2 = conditional(le(t0,t_a), 1.0, 0.0)
	xp3 = conditional(lt(t_a,t0+tr), 1.0, 0.0)
	#w2 = xp2*xp3*pi*(t_a - t0 + tr)/tr
	w2 = self.w2()

	Ct = 0.5*(1 - cos(w1+w2))

	return Ct


    def w1(self):

	t0 = self.parameters["t0"]
	t_a = self.parameters["t_a"] # current time 

        #isHomogenousActivation = self.parameters["HomogenousActivation"]

        #if isHomogenousActivation: # activation at 10 ms 
        #    t_init = self.t_init
        #    t_init.vector()[:] = 0.0*np.ones(len(t_init.vector().array()))
        #else:
        #    t_init = self.t_init # time of activation 

        t_init = self.t_init
        t_since_activation = t_a - t_init 

	xp1 = conditional( lt(t_since_activation,t0), 1.0, 0.0 )
        xp4 = conditional( gt(t_since_activation, Constant(0.0)), 1.0, 0.0 )
        xp5 = conditional( lt(t_since_activation, Constant(9998.0)), 1.0, 0.0 )

	w1 = xp5*xp4*xp1*pi*t_since_activation/t0
	#w1 = xp1*pi*t_since_activation/t0

	return w1
	
    def w2(self):

	t0 = self.parameters["t0"]
	t_a = self.parameters["t_a"]
	tr = self.tr()

        #isHomogenousActivation = self.parameters["HomogenousActivation"]

        #if isHomogenousActivation: # activation at 10 ms 
        #    t_init = self.t_init
        #    t_init.vector()[:] = 0.0*np.ones(len(t_init.vector().array()))
        #else:
        #    t_init = self.t_init # time of activation 

        t_init = self.t_init # time of activation 
        t_since_activation = t_a - t_init 

      	xp2 = conditional(le(t0,t_since_activation), 1.0, 0.0)
	xp3 = conditional(lt(t_since_activation,t0+tr), 1.0, 0.0) 

	w2 = xp2*xp3*pi*(t_since_activation - t0 + tr)/tr

	return w2

    def lmbda_ff_ED(self, F_ref):

	#F = self.Fmat()*inv(F_ref)
        F = self.Fe()*inv(F_ref)

	f0 = self.parameters["fiber"]
	Cmat = F.T*F
	lmbda_ff_ED = sqrt(dot(f0, Cmat*f0))

	return lmbda_ff_ED

    def lmbda_ss_ED(self, F_ref):

	#F = self.Fmat()*inv(F_ref)
	F = self.Fe()*inv(F_ref)	
	s0 = self.parameters["sheet"]
	Cmat = F.T*F
	lmbda_ss_ED = sqrt(dot(s0, Cmat*s0))

	return lmbda_ss_ED


    def lmbda_nn_ED(self, F_ref):

	#F = self.Fmat()*inv(F_ref)
	F = self.Fe()*inv(F_ref)
	n0 = self.parameters["sheet-normal"]
	Cmat = F.T*F
	lmbda_nn_ED = sqrt(dot(n0, Cmat*n0))

	return lmbda_nn_ED

    '''
    def directionalWork_ff_ED(self, F_ref, e):

	F = self.Fmat()*inv(F_ref)
	Cmat = F.T*F

        #directionalTraction_e_ED = ?
	directionalWork_e_ED = sqrt(dot(e, Cmat*e))

	return directionalWork_e_ED
    ''' 

    def CalculateFiberStretch(self, F_, F_ref, e, VolSeg):
        
        I = Identity(F_.ufl_domain().geometric_dimension())       
        # Pull back to ED
        F = F_*inv(F_ref)
        # Right Cauchy Green
        C = F.T*F
        S = inner(C*e, e)

        mesh = self.parameters["mesh"]
        #bivMesh = self.parameters["mesh"]
        dx = self.parameters["dx"]

        l_BiV = [assemble(S * dx(ii), form_compiler_parameters={"representation":"uflacs"}) for ii in VolSeg]
        #l_BiV = [0.0, 0.0, 0.0]
        #l_BiV[0] = assemble(S * dx(0))/assemble(Constant(1.0) * dx(0))#Vol[0]
        #l_BiV[1] = assemble(S * dx(1))/assemble(Constant(1.0) * dx(1))#Vol[1]
        #l_BiV[2] = assemble(S * dx(2))/assemble(Constant(1.0) * dx(2))#Vol[2]

        return l_BiV, S


    def CalculateFiberGreenStrain(self, F_, F_ref, e_fiber, VolSeg):
        
        #F_ = grad(u) + I
        I = Identity(F_.ufl_domain().geometric_dimension())       

        # Pull back to ED
        F = F_*inv(F_ref)
        # Right Cauchy Green
        C = F.T*F
        # Green Lagrage
        E = 0.5*(C - I)
        
        E_fiber = inner(E*e_fiber, e_fiber)

        mesh = self.parameters["mesh"]
        #bivMesh = self.parameters["mesh"]
        dx = self.parameters["dx"]#bivMesh.dx

        E_fiber_BiV = [assemble(E_fiber * dx(ii), form_compiler_parameters={"representation":"uflacs"}) for ii in VolSeg]

        return E_fiber_BiV, E_fiber 

    def CalculateFiberBiotStrain(self, F_, F_ref, e_fiber, VolSeg):
        
        #F_ = grad(u) + I
        I = Identity(F_.ufl_domain().geometric_dimension())       

        # Pull back to ED
        F = F_*inv(F_ref)
        # Right Cauchy Green
        C = F.T*F
        
        C_fiber = inner(C*e_fiber, e_fiber)
	E_fiber = sqrt(C_fiber) - 1

        mesh = self.parameters["mesh"]
        #bivMesh = self.parameters["mesh"]
        dx = self.parameters["dx"]#bivMesh.dx

        E_fiber_BiV = [assemble(E_fiber * dx(ii), form_compiler_parameters={"representation":"uflacs"}) for ii in VolSeg]

        return E_fiber_BiV, E_fiber 

    def CalculateFiberNaturalStrain(self, F_, F_ref, e_fiber, VolSeg):
        
        #F_ = grad(u) + I
        I = Identity(F_.ufl_domain().geometric_dimension())       

        # Pull back to ED
        F = F_*inv(F_ref)
        # Right Cauchy Green
        C = F.T*F
        
        C_fiber = inner(C*e_fiber, e_fiber)
	E_fiber = 0.5*(1 - 1/C_fiber)

        mesh = self.parameters["mesh"]
        #bivMesh = self.parameters["mesh"]
        dx = self.parameters["dx"]#bivMesh.dx

        E_fiber_BiV = [assemble(E_fiber * dx(ii), form_compiler_parameters={"representation":"uflacs"}) for ii in VolSeg]

        return E_fiber_BiV, E_fiber 



    def UpdateMaxLambda(self, Lambda, maxLamdaOld):

        LambdaArray = Lambda.vector().array()
        maxLamdaOldArray = maxLamdaOld.vector().array()
		
        for idx, (ls, mls_old) in enumerate(zip(LambdaArray, maxLamdaOldArray)): 
            if ls >= mls_old: 
                maxLamdaOldArray[idx] = ls
		
        maxLamdaOld.vector()[:] = maxLamdaOldArray
        return maxLamdaOld

    def UpdateMinLambda(self, Lambda, minLamdaOld):

        LambdaArray = Lambda.vector().array()
        minLamdaOldArray = minLamdaOld.vector().array()
		
        for idx, (ls, mls_old) in enumerate(zip(LambdaArray, minLamdaOldArray)): 
            if ls < mls_old: 
                minLamdaOldArray[idx] = ls
		
        minLamdaOld.vector()[:] = minLamdaOldArray
        return minLamdaOld



