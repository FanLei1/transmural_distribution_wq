import sys, pdb
from dolfin import * 
#sys.path.append("/mnt/home"):
from heArt.BiV_TimedGuccione_MRC2_Filling_closed2_cvode import run_BiV_TimedGuccione as run_BiV_TimedGuccione

#  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - - 
IODetails = {"casename" : "ellipsoidal_baselinegeo_1",
             "directory_me" : '../../HFpEFmesh/',
             "directory_ep" : '../../HFpEFmesh/', 
             "outputfolder" : '../outputs/',
             "folderName" : 'HFpEF/',
             "caseID" : 'baseline_comb11_tv_new1v_1',
             "isLV" : True} 

contRactility = 130e3

GuccioneParams = {"ParamsSpecified" : True, 
                  "Tmax" : Constant(contRactility), # 0.6*0.5*146e3 
                  "C_param" : Constant(100.0), #20.0, # 0.6*20.0 
                  "HomogenousActivation": True,
                  "Ca0" : 4.35,
                  "Ca0max" : 4.35,
                  "B" : 4.75, #4.75 no strech dependency seen, 
                  "t0" : 275,#150.5,#200, #250, 
                  "deg" : 4,
		  "m": 1048,
                  "l0" : 1.58,#1.6, #1.58
                  "b" : -1675,#-1429, #-275, #-950, #-275, 
                  "bff"  : 29.0,
                  "bfx"  : 13.3,
                  "bxx"  : 26.6,
                  "Kappa": 1e5,
                  "incompressible" : True,
                  "tau" : 25, #290, #1048, #290,
		  "t_trans": 300
                  }

Circparam = {     "Ees_la": 10,
        	  "A_la": 2.67,
        	  "B_la": 0.019,
        	  "V0_la": 10,
        	  "Tmax_la": 120,
        	  "tau_la": 25,
		  "tdelay_la": 160,
		  "Cao": 0.0032,#0.005
		  "Cad": 0.033,#0.016 #SMS add:for distal compliance
    		  "Cven" : 0.28,
    		  "Vart0" : 360,
		  "Vad0" : 40, # #SMS add:for distal compliance
    		  "Vven0" : 3370.0,
    		  "Rao" : 500,#800
    		  "Rven" : 100.0,
    		  "Rper" : 18000,#15000.0,#25000,30000
		  "Rad": 106000,#130000 #SMS add:for distal compliance, 150000
    		  "Rmv" : 200.0,
    		  "V_ven" : 3700,
    		  "V_art" : 740,
		  "V_ad": 100,# #SMS add:for distal compliance
    		  "V_LA" : 12,
    		  "stop_iter" : 5
		  };

coronaryparam = {"filename": "../../CVODE/inputfiles/network_matrix_400vessel.txt"};

SimDetails = {    "ActiveModel": "Time-varying",
                  "diaplacementInfo_ref": False,
                  "HeartBeatLength": 800.0,
                  "dt": 1.0,
                  "writeStep": 2.0,
                  "GiccioneParams" : GuccioneParams, 
                  "nLoadSteps": 1, 
                  "DTI_EP": False, #LCL no DTI_EP
                  "DTI_ME": False, #LCL no DTI_ME
                  "d_iso": 1.5*0.01, 
                  "d_ani_factor": 4.0, 
                  "ploc": [[1.4, 1.4, -3.0, 2.0]],
		  "probepts" : [[1.4, 1.4, -3.0],[1.6, 1.6, -3.0],[1.8, 1.8, -3.0],[2.0,2.0,-3.0]],
                  "pacing_locations_1": [1],#range(51,57) + range(57,68), #range(57,68)
                  "pacing_locations_2": [],#range(108,125), 
                  "pacing_timing_1": 4.0,
                  "pacing_timing_2": 24.0, 
                  "LVEDV_Shift": 34.5, # 2*15 + 3*1.5 
                  "RVEDV_Shift": 38, # 2*15 + 3*1.0
		  "Isclosed": True,
		  "closedloopparam": Circparam,
		  "coronarynetworkparam": coronaryparam,
                  "Pao": 70,#40,
                  "Ppu": 12,
                  "Pmv": 5, 
                  "Ptr": 4,
                  "Pper_sys": 600,
                  "Pper_pul": 200,
                  "AHA_segments" :  range(0,8),#range(1,18) + range(20,27),
                  "nUnLoadSteps": 50, 
                  "Ischemia": False,
             	  "isLV" : True,
                  "topid" : 4,
                  "LVendoid" : 2,
                  "RVendoid" : 0,
                  "epiid" : 1,
		  "abs_tol" : 1e-9,
		  "rel_tol" : 5e-7,
                 }

run_BiV_TimedGuccione(IODet=IODetails, SimDet=SimDetails)
#  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - - 
    
