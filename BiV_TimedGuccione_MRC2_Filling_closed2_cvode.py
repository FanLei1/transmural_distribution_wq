import sys, shutil, pdb, math
import os as os
import numpy as np
from mpi4py import MPI as pyMPI 

from dolfin import * 
from fenicstools import *

#sys.path.append("/home/fenics/shared_home/Research/EMcode/heArt/")
#sys.path.append("/mnt/home/lclee/closed_fem_flow/heArt/")
sys.path.append("/mnt/home/fanlei1/")
#sys.path.append("/home/fenics/shared/Research/closed_fem_flow")

import vtk_py
import vtk

from forms_MRC import Forms

#from nsolver import NSolver as NSolver

from oops_objects_MRC import printout
from oops_objects_MRC import biventricle_mesh as biv_mechanics_mesh
from oops_objects_MRC import lv_mesh as lv_mechanics_mesh
from oops_objects_MRC import State_Variables
from oops_objects_MRC import Windkessel 
from oops_objects_MRC import PV_Ventricles 
from oops_objects_MRC import PV_Elas
from oops_objects_MRC import update_mesh
from oops_objects_MRC import exportfiles

from heArt.mesh_scale_create_fiberFiles import create_EDFibers
from heArt.mesh_partitionMeshforEP_J import defCPP_Matprop, defCPP_Matprop_DIsch
from heArt.mesh_partitionMeshforEP_J import defSubDomain_AHA

from EPmodel import EPmodel
from MEmodel import MEmodel

import sys
sys.path.append("/mnt/home/fanlei1/closed_fem_flow/CVODE")
#sys.path.append("/home/fenics/shared/Research/closed_fem_flow/CVODE/")
import coronaryflow_asymmetric_fn_omp
import readnetwork_omp
import computeflow_omp


#  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - -
def run_BiV_TimedGuccione(IODet, SimDet): 

    deg = 4
    flags = ["-O3", "-ffast-math", "-march=native"]
    parameters["form_compiler"]["representation"]="quadrature"
    parameters["form_compiler"]["quadrature_degree"]=deg
    #parameters["allow_extrapolation"]=True # LCL SET 


    casename = IODet["casename"]
    directory_me = IODet["directory_me"]
    directory_ep = IODet["directory_ep"]
    outputfolder = IODet["outputfolder"]
    folderName = IODet["folderName"] + IODet["caseID"] + '/' 
    isLV = SimDet["isLV"]
    
    delTat = SimDet["dt"]

    #if("ActiveModel" in SimDet.keys()):
    #    if(SimDet["ActiveModel"] == "Time-varying"):
    #		from BurkhoffTimevarying2 import BurkhoffTimevarying as Active
    #    else:
    #		from Guccione_MRC import Guccione as Active
    #else:
    #	from Guccione_MRC import Guccione as Active

    ##GuccioneParams = SimDet["GiccioneParams"]


    #  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - - 
    # Read EP data from HDF5 Files
    mesh_ep = Mesh() 
    comm_common = mesh_ep.mpi_comm()

    meshfilename_ep = directory_ep + casename + "_refine.hdf5"
    f = HDF5File(comm_common, meshfilename_ep, 'r')
    f.read(mesh_ep, casename, False)

    File(outputfolder + folderName + "mesh_ep.pvd") << mesh_ep 

    facetboundaries_ep = MeshFunction("size_t", mesh_ep, 2)
    f.read(facetboundaries_ep, casename+"/"+"facetboundaries")

    matid_ep = CellFunction('size_t', mesh_ep)
    AHAid_ep = CellFunction('size_t', mesh_ep)
    if(f.has_dataset(casename+"/"+"matid")):
    	f.read(matid_ep, casename+"/"+"matid")
    else:
	matid_ep.set_all(0)

    if(f.has_dataset(casename+"/"+"AHAid")):
    	f.read(AHAid_ep, casename+"/"+"AHAid")
    else:
	AHAid_ep.set_all(0)

    deg_ep = 4

    Quadelem_ep = FiniteElement("Quadrature", mesh_ep.ufl_cell(), degree=deg_ep, quad_scheme="default")
    Quadelem_ep._quad_scheme = 'default'
    Quad_ep = FunctionSpace(mesh_ep, Quadelem_ep)

    VQuadelem_ep = VectorElement("Quadrature", 
                                  mesh_ep.ufl_cell(), 
                                  degree=deg_ep, 
                                  quad_scheme="default")
    VQuadelem_ep._quad_scheme = 'default'
    
    fiberFS_ep = FunctionSpace(mesh_ep, VQuadelem_ep)

    f0_ep = Function(fiberFS_ep)
    s0_ep = Function(fiberFS_ep)
    n0_ep = Function(fiberFS_ep)

    
    if SimDet["DTI_EP"] is True : 
        f.read(f0_ep, casename+"/"+"eF_DTI")
        f.read(s0_ep, casename+"/"+"eS_DTI")
        f.read(n0_ep, casename+"/"+"eN_DTI")
    else: 
        f.read(f0_ep, casename+"/"+"eF")
        f.read(s0_ep, casename+"/"+"eS")
        f.read(n0_ep, casename+"/"+"eN") 
    
    f.close()

    comm_ep = mesh_ep.mpi_comm()

    # Set up export class
    export = exportfiles(comm_ep, IODet, SimDet)
    export.exportVTKobj("facetboundaries_ep.pvd", facetboundaries_ep) 

    # Define state variables
    #  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - - 
    state_obj = State_Variables(comm_ep, SimDet) 
    state_obj.dt.dt = delTat
    #  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - - 

    EPparams = {"EPmesh": mesh_ep,\
                "deg": 4,\
		"matid": matid_ep,\
		"facetboundaries": facetboundaries_ep,\
		"f0": f0_ep,\
		"s0": s0_ep,\
		"n0": n0_ep,\
		"state_obj": state_obj,\
                "d_iso": SimDet["d_iso"],\
                "d_ani_factor": SimDet["d_ani_factor"],\
	        "AHAid": AHAid_ep,\
                "matid": matid_ep}

    if("ploc" in SimDet.keys()):
	EPparams.update({"ploc": SimDet["ploc"]})
    if("Ischemia" in SimDet.keys()):
	EPparams.update({"Ischemia": SimDet["Ischemia"]})

    # Define EP model and solver
    EPmodel_ = EPmodel(EPparams)
    EpiBCid_ep = EPmodel_.MarkStimulus()

    export.exportVTKobj("EpiBCid_ep.pvd", EpiBCid_ep) 
	
    solver_FHN = EPmodel_.Solver()
    #  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - - 
    # Mechanics Mesh 

    mesh_me = Mesh() 
    mesh_me_params = {"directory" : directory_me, 
                   "casename" : casename, 
                   "fibre_quad_degree" : 4, 
                   "outputfolder" : outputfolder,
                   "foldername" : folderName,
		   "state_obj": state_obj,
                   "common_communicator": comm_common,
                   "MEmesh": mesh_me}

    MEmodel_ = MEmodel(mesh_me_params, SimDet)
    solver_elas = MEmodel_.Solver()

    export.exportVTKobj("mesh_me.pvd", MEmodel_.mesh_me) 

    F_ED = Function(MEmodel_.TF)

    AHA_segments = SimDet["AHA_segments"]

    export.displacementED_file << MEmodel_.GetDisplacement()

    nloadstep = SimDet["nLoadSteps"]
    for lmbda_value in range(0, nloadstep):

	if(lmbda_value <= 15):
        	MEmodel_.LVCavityvol.vol += 2.0
        	MEmodel_.RVCavityvol.vol += 2.0#2.0 
	else:
		MEmodel_.LVCavityvol.vol += 1.5
        	MEmodel_.RVCavityvol.vol += 1.0#1.5 

        #  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - -
        solver_elas.solvenonlinear() 
        #  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - -

	comm_me = MEmodel_.mesh_me.mpi_comm()
        printout("Loading phase step = " + str(lmbda_value), comm_me)
                            	
	export.writePV(MEmodel_, 0);
    	export.displacementED_file << MEmodel_.GetDisplacement()

        F_ED.vector()[:] = project(MEmodel_.GetFmat(), MEmodel_.TF).vector().array()[:] # this is a bug 

    #  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - -

    # Declare communicator based on mpi4py 
    comm_me_ = comm_me.tompi4py()

    eCC, eRR, eLL, deformedMesh, deformedBoundary = MEmodel_.GetDeformedBasis()

    fStrain = MEmodel_.GetFiberstrain(F_ED)
    fStrain_uL = MEmodel_.GetFiberstrainUL()
    #  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - -

    # Closed-loop phase
    stop_iter = SimDet["closedloopparam"]["stop_iter"]
    Cao = SimDet["closedloopparam"]["Cao"]
    Cad = SimDet["closedloopparam"]["Cad"] ###SMS add: for distal aortic compliance
    Cven = SimDet["closedloopparam"]["Cven"]
    Vart0 = SimDet["closedloopparam"]["Vart0"]
    Vad0 = SimDet["closedloopparam"]["Vad0"] ###SMS add: for distal aortic compliance 
    Vven0 = SimDet["closedloopparam"]["Vven0"]
    Rao =  SimDet["closedloopparam"]["Rao"]
    Rven = SimDet["closedloopparam"]["Rven"]
    Rper = SimDet["closedloopparam"]["Rper"]
    Rad = SimDet["closedloopparam"]["Rad"] ###SMS add: for distal aortic compliance
    Rmv = SimDet["closedloopparam"]["Rmv"]
    V_ven = SimDet["closedloopparam"]["V_ven"]
    V_art = SimDet["closedloopparam"]["V_art"]
    V_ad = SimDet["closedloopparam"]["V_ad"] ###SMS add: for distal aortic compliance
    V_LA = SimDet["closedloopparam"]["V_LA"]

    isrestart = 0
    prev_cycle = 0
    cnt = 0
    Qao = 0 
    Qmv = 0 
    Qper = 0 
    Qla = 0
    Qlad = 0
    Qlcx = 0

    potential_me = Function(FunctionSpace(MEmodel_.mesh_me,'CG',1))

    # Set up coronary flow
    nthreads = 4; ncols = 23
    sim_data = readnetwork_omp.UserData();
    networkfilename = SimDet["coronarynetworkparam"]["filename"]
    readnetwork_omp.readnetwork(networkfilename, sim_data, ncols, nthreads) # OMP version (need number of threads)
    ode_obj = coronaryflow_asymmetric_fn_omp.createODE(sim_data);
    Lad_P0_array = [np.ones(sim_data.nvessels,dtype=np.double)*0.005433149918032 for p in range(0, len(np.array(SimDet["probepts"])))]
    Qlad_array = np.zeros(len(np.array(SimDet["probepts"])))
    imp_array_prev = np.zeros(len(np.array(SimDet["probepts"])))
    alpha = 1.0


    while(1):

        if(state_obj.cycle > stop_iter):
            	break;


	#Time varying elastance function fro LA and RA ##################
	def et(t, Tmax, tau):
		if (t <= 1.5*Tmax):
			out = 0.5*(math.sin((math.pi/Tmax)*t - math.pi/2) + 1);
      		else:
			out = 0.5*math.exp((-t + (1.5*Tmax))/tau);

		return out        
	#################################################################
	p_cav = MEmodel_.GetLVP()
        V_cav = MEmodel_.GetLVV()

        state_obj.tstep = state_obj.tstep + state_obj.dt.dt
        state_obj.cycle = math.floor(state_obj.tstep/state_obj.BCL)
        state_obj.t = state_obj.tstep - state_obj.cycle*state_obj.BCL

	# Update deformation at F_ED
	if(state_obj.cycle > prev_cycle):
        	F_ED.vector()[:] = project(MEmodel_.GetFmat(), MEmodel_.TF).vector().array()[:] 

	prev_cycle = state_obj.cycle

        MEmodel_.t_a.vector()[:] = state_obj.t

	Part = 1.0/Cao*(V_art - Vart0);
	Pad = 1.0/Cad*(V_ad - Vad0);  ###SMS add: for distal aortic compliance
    	Pven = 1.0/Cven*(V_ven - Vven0);
    	PLV = p_cav;


        printout("Cycle number = "+str(state_obj.cycle)+" cell time = "+str(state_obj.t)+" tstep = "+str(state_obj.tstep)+" dt = "+str(state_obj.dt.dt), comm_me)

	
	#### For Calculating P_LA ######################################## 
        Ees_la = SimDet["closedloopparam"]["Ees_la"]#60;
        A_la = SimDet["closedloopparam"]["A_la"];#58.67;
        B_la = SimDet["closedloopparam"]["B_la"];#0.049;
        V0_la = SimDet["closedloopparam"]["V0_la"];#10;
        Tmax_la = SimDet["closedloopparam"]["Tmax_la"];#120;
        tau_la = SimDet["closedloopparam"]["tau_la"];#25;
        tdelay_la = SimDet["closedloopparam"]["tdelay_la"];#200;
	
	if (state_obj.t < SimDet["HeartBeatLength"] - tdelay_la):
		t_la = state_obj.t + tdelay_la;
        else: 
		t_la = state_obj.t - state_obj.BCL + tdelay_la;

        printout("t_LA = "+str(t_la)+" t_delay_LA = "+str(tdelay_la)+" BCL = "+str(state_obj.BCL)+" t = "+str(state_obj.t), comm_me)

        PLA = et(t_la,Tmax_la,tau_la)*Ees_la*(V_LA - V0_la) + (1 - et(t_la,Tmax_la,tau_la))*A_la*(math.exp(B_la*(V_LA - V0_la)) - 1);
	##################################################################################################################################

        printout("P_ven = "+str(Pven), comm_me)
        printout("P_LV = " +str(PLV ), comm_me)
        printout("P_art = "+str(Part), comm_me)
        printout("P_LA = " +str(PLA ), comm_me)


	#### conditions for Valves#######################################
    	if(PLV <= Part):
    	     Qao = 0.0;
    	else:
    	     Qao = 1.0/Rao*(PLV - Part);
    	

    	if(PLV >= PLA):
    	    Qmv = 0.0;
    	else: 
    	    Qmv = 1.0/Rmv*(PLA - PLV);

	H = (Part - PLV)*0.0075;	
	
    	Qper = 1.0/Rper*(Part - Pad);
	Qad = 1.0/Rad*(Pad - Pven); ###SMS add: for distal aortic compliance
	Qla = 1.0/Rven*(Pven - PLA);

        #printout("Q_LA = " + str(Qla ), comm_me)
        #printout("Q_ao = " + str(Qao ), comm_me)
        #printout("Q_per = "+ str(Qper), comm_me)
        #printout("Q_mv = " + str(Qmv ), comm_me)

	V_cav_prev = V_cav
	V_art_prev = V_art
	V_ad_prev = V_ad
	V_ven_prev = V_ven
	p_cav_prev = p_cav

        imp = project( MEmodel_.GetIMP(), FunctionSpace(MEmodel_.mesh_me,'DG',1), form_compiler_parameters={"representation":"uflacs"})
        imp.rename("imp","imp")

        imp2 = project( MEmodel_.GetIMP2(), FunctionSpace(MEmodel_.mesh_me,'DG',1), form_compiler_parameters={"representation":"uflacs"})
        imp2.rename("imp2","imp2")

        if("probepts" in SimDet.keys()):
                x = np.array(SimDet["probepts"])
                probesIMP = Probes(x.flatten(), FunctionSpace(MEmodel_.mesh_me,'DG',1))
                probesIMP(imp)

                probesIMP2 = Probes(x.flatten(), FunctionSpace(MEmodel_.mesh_me,'DG',1))
                probesIMP2(imp2)

                probesIMP3 = Probes(x.flatten(), FunctionSpace(MEmodel_.mesh_me,'CG',1))
                probesIMP3(MEmodel_.GetP())


      		# broadcast from proc 0 to other processes
		rank = comm_me_.Get_rank()
        	imp_array = probesIMP3.array()  ## probe will only send to rank =0
        	if(not rank == 0):
			imp_array = np.empty(len(x))

        	comm_me_.Bcast(imp_array, root=0)

                
                #a = probesIMP3.array()
               # if comm.Get_rank() == 0:
                #print "IMP = ", imp_array
########################################	      
    	Pin = Part/1e6
    	Pout = Pven/1e6

    	sim_data.pa_const = Pin 
    	sim_data.pv_const = Pout

    	tstep_sec = (state_obj.tstep)/1000.0
    	tstep_prev_sec = (state_obj.tstep - state_obj.dt.dt)/1000.0
    	dt_sec = state_obj.dt.dt/1000.0

    	# Compute dP/dt, Pt (take absolute value of IMP)
	cnt_CF = 0
	Lad_P_array = []
	imp_array = abs(imp_array)
       
	for (imp_, imp_prev_, Lad_P0) in zip(imp_array, imp_array_prev, Lad_P0_array):

    		pt = alpha*imp_/1e6*np.ones(sim_data.nvessels,dtype=np.double)
    		dptdt = alpha*(imp_  - imp_prev_)/(1e6*dt_sec)*np.ones(sim_data.nvessels,dtype=np.double) 

    		# Compute pressures in the vessels (parallel version)
    		Lad_P = coronaryflow_asymmetric_fn_omp.advanceODE(sim_data, ode_obj, tstep_prev_sec, tstep_sec, Lad_P0, pt, dptdt)

    		# Get dPdt
    		dLad_Pdt = (Lad_P - Lad_P0)/(dt_sec)

    		# Compute flow in the vessels (parallel version)
    		[Qin, Qout] = computeflow_omp.computeflow(sim_data, Lad_P, dLad_Pdt) # Q is in ml/s
    		Qlad_ = Qin[0]/1000.0 # Convert Qin to ml/ms

		Qlad_array[cnt_CF] = Qlad_
		Lad_P_array.append(Lad_P)

		cnt_CF += 1

    	# Update initial condition with current solution
	Qlad = np.sum(Qlad_array)
	

        #eng = matlab.engine.start_matlab()
        #eng.cd(r'../flow_passive',nargout=0)
        #p_lump0_LAD = [0.0]*400
        #p_lump0_LCX = p_lump0_LAD
       ## if MPI.COMM_WORLD.Get_rank() == 0:
        #c = sum(a)/2
        #b = c.tolist()
        #IMP_LAD = b
        #print "imp = ", IMP_LAD
        #
        #IMP_LCX = IMP_LAD
        #IMP0_LAD = 0.0
        #IMP0_LCX = IMP0_LAD
        #myo_lad = 0.0
        #myo_lcx = myo_lad
        #dssr_lad = 0.0
        #dssr_lcx = dssr_lad
        #t1 = (state_obj.t-state_obj.dt.dt)/1000
        #t2 = (state_obj.t)/1000
        #i = cnt + 1
        #print "i= ", i;
        #print "t2= ", t2;
        #PART = Part/1000000
        #PVEN = Pven/1000000
        #print "PLV = ", PLV 
        #[flow_LAD, p_lump_LAD, Qcor_LAD] = eng.FlowControl_vessel_LAD(PART, PVEN, t1, t2, p_lump0_LAD, IMP_LAD, IMP0_LAD, myo_lad, dssr_lad, i, nargout = 3);
        #print "qcor =", Qcor_LAD
        #[flow_LCX, p_lump_LCX, Qcor_LCX] = eng.FlowControl_vessel(PART, PVEN, t1, t2, p_lump0_LCX, IMP_LCX, IMP0_LCX, myo_lcx, dssr_lcx, i, nargout = 3);
        #Qlad = Qcor_LAD
        #Qlcx = Qcor_LCX
        #Qlad = 0.0
        Qlcx = 0.0
########################################
        printout("Q_LA = " + str(Qla ), comm_me)
        printout("Q_ao = " + str(Qao ), comm_me)
        printout("Q_per = "+ str(Qper), comm_me)
        printout("Q_ad = " + str(Qad ), comm_me)
        printout("Q_mv = " + str(Qmv ), comm_me)
        printout("Q_LAD = " + str(Qlad), comm_me)
        printout("Q_LCX = " + str(Qlcx), comm_me)

	V_cav = V_cav + state_obj.dt.dt*(Qmv - Qao);
	V_art = V_art + state_obj.dt.dt*(Qao - Qper - Qlad - Qlcx);
	V_ad = V_ad + state_obj.dt.dt*(Qper - Qad); ###SMS add: for distal aortic compliance
    	V_ven = V_ven + state_obj.dt.dt*(Qad + Qlad + Qlcx - Qla);
	V_LA = V_LA + state_obj.dt.dt*(Qla - Qmv);

	MEmodel_.LVCavityvol.vol = V_cav

        printout("V_ven = " + str(V_ven), comm_me)
        printout("V_LV = "  + str(V_cav), comm_me)
        printout("V_art = " + str(V_art), comm_me)
        printout("V_ad = " + str(V_ad), comm_me)
        printout("V_LA = "  + str(V_LA ),  comm_me)
       
	printout("Solving elasticity", comm_me)
	try:
		solver_elas.solvenonlinear()

		isrestart = 0
		state_obj.dt.dt	= delTat

	except RuntimeError:
		printout("Restart time step ********************************************* ", comm_me)
		V_cav = V_cav_prev
		V_art = V_art_prev
		V_ven = V_ven_prev
		V_ad = V_ad_prev
		p_cav = p_cav_prev
		state_obj.tstep = state_obj.tstep - state_obj.dt.dt
		state_obj.dt.dt += 1.0 #Increase time step 
		isrestart = 1

	if(isrestart == 0):
    		Lad_P0_array = Lad_P_array
		imp_array_prev = imp_array
		export.writeP(MEmodel_, [Pven, PLV, Part, PLA], state_obj.tstep)
		export.writePV(MEmodel_, state_obj.tstep)
		export.writeQ(MEmodel_, np.concatenate((np.concatenate((np.array([Qao, Qmv, Qper, Qla]), Qlad_array)), np.array([Qlcx]))), state_obj.tstep)
		#export.writeQ(MEmodel_, [Qao, Qmv, Qper, Qla, Qlad_array[0], Qlad_array[1], Qlcx], state_obj.tstep)



	# Reset phi and r in EP at end of diastole
	if state_obj.t < state_obj.dt.dt:
		EPmodel_.reset();

        if state_obj.t >= SimDet['pacing_timing_1'] and ( state_obj.t <= SimDet['pacing_timing_1']+20.0 ): 
            EPmodel_.f_1.iStim = 0.3
        else:
            EPmodel_.f_1.iStim = 0.0


        if state_obj.t >= SimDet['pacing_timing_2'] and ( state_obj.t <= SimDet['pacing_timing_2']+20.0 ):
            EPmodel_.f_2.iStim = 0.3
        else:
            EPmodel_.f_2.iStim = 0.0


	printout("Solving FHN", comm_me)
        solver_FHN.solvenonlinear()
	EPmodel_.UpdateVar()

	# Interpolate phi to mechanics mesh
        potential_ref = EPmodel_.interpolate_potential_ep2me_phi(V_me = Function(FunctionSpace(MEmodel_.mesh_me,'CG',1)))
        potential_ref.rename("v_ref", "v_ref")

        potential_me.vector()[:] = potential_ref.vector().array()[:]


        #  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - -
        if(MPI.rank(comm_ep) == 0):
            print 'UPdating isActiveField and tInitiationField'
        MEmodel_.activeforms.update_activationTime(potential_n = potential_me, comm = comm_me)

        F_n = MEmodel_.GetFmat() 

        #imp = project( MEmodel_.GetIMP(), FunctionSpace(MEmodel_.mesh_me,'DG',1), form_compiler_parameters={"representation":"uflacs"})
        #imp.rename("imp","imp")

        #imp2 = project( MEmodel_.GetIMP2(), FunctionSpace(MEmodel_.mesh_me,'DG',1), form_compiler_parameters={"representation":"uflacs"})
        #imp2.rename("imp2","imp2")

    	#if("probepts" in SimDet.keys()):
	#	x = np.array(SimDet["probepts"])
	#	probesIMP = Probes(x.flatten(), FunctionSpace(MEmodel_.mesh_me,'DG',1))
	#	probesIMP(imp)

	#	probesIMP2 = Probes(x.flatten(), FunctionSpace(MEmodel_.mesh_me,'DG',1))
	#	probesIMP2(imp2)

	#	probesIMP3 = Probes(x.flatten(), FunctionSpace(MEmodel_.mesh_me,'CG',1))
	#	probesIMP3(MEmodel_.GetP())


	fstress_DG = project(MEmodel_.Getfstress(), FunctionSpace(MEmodel_.mesh_me,'DG', 1) , form_compiler_parameters={"representation":"uflacs"})
	fstress_DG.rename("fstress", "fstress")
    	if("probepts" in SimDet.keys()):
		probesfstress = Probes(x.flatten(), FunctionSpace(MEmodel_.mesh_me,'DG',1))
		probesfstress(fstress_DG)

	Eul_fiber_BiV_DG = project(fStrain_uL, FunctionSpace(MEmodel_.mesh_me,'DG', 1) , form_compiler_parameters={"representation":"uflacs"})
	Eul_fiber_BiV_DG.rename("Eff", "Eff")
    	if("probepts" in SimDet.keys()):
		probesEul_fiber = Probes(x.flatten(), FunctionSpace(MEmodel_.mesh_me,'DG',1))
		probesEul_fiber(Eul_fiber_BiV_DG)

		probesE_circ_BiV = Probes(x.flatten(), FunctionSpace(MEmodel_.mesh_me,'DG',1))
		probesE_long_BiV = Probes(x.flatten(), FunctionSpace(MEmodel_.mesh_me,'DG',1))
		probesE_radi_BiV = Probes(x.flatten(), FunctionSpace(MEmodel_.mesh_me,'DG',1))


	# postprocess and write 
        #  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - -

	## ----------------- Compute Natural Strain -----------------------------------------------------------------------------
        E_circ_BiV, E_circ_BiV_ = MEmodel_.GetFiberNaturalStrain(F_ED, eCC, AHA_segments)
        E_long_BiV, E_long_BiV_ = MEmodel_.GetFiberNaturalStrain(F_ED, eLL, AHA_segments)
        E_radi_BiV, E_radi_BiV_ = MEmodel_.GetFiberNaturalStrain(F_ED, eRR, AHA_segments)
	## --------------------------------------------------------------------------------------------------------------------



	E_circ_BiV_DG = project(E_circ_BiV_, FunctionSpace(MEmodel_.mesh_me,'DG', 0) , form_compiler_parameters={"representation":"uflacs"})
	E_circ_BiV_DG.rename("Ecc", "Ecc")
    	if("probepts" in SimDet.keys()):
		probesE_circ_BiV(E_circ_BiV_DG)

    	E_long_BiV_DG = project(E_long_BiV_, FunctionSpace(MEmodel_.mesh_me,'DG', 0) , form_compiler_parameters={"representation":"uflacs"})
	E_long_BiV_DG.rename("Ell", "Ell")
    	if("probepts" in SimDet.keys()):
		probesE_long_BiV(E_long_BiV_DG)

	E_radi_BiV_DG = project(E_radi_BiV_, FunctionSpace(MEmodel_.mesh_me,'DG', 0) , form_compiler_parameters={"representation":"uflacs"})
	E_radi_BiV_DG.rename("Err", "Err")
    	if("probepts" in SimDet.keys()):
		probesE_radi_BiV(E_radi_BiV_DG)


        if(cnt % SimDet["writeStep"] == 0.0):

	    export.writetpt(MEmodel_, state_obj.tstep);
            export.displacementfile << MEmodel_.GetDisplacement()
            export.vtkfile_phi << EPmodel_.getphivar()
            export.vtkfile_r << EPmodel_.getrvar()
            export.vtkfile_phi_me << potential_me
            export.vtkfile_phi_ref << potential_ref
	    export.vtkfile_Ecc << E_circ_BiV_DG
	    export.vtkfile_Ell << E_long_BiV_DG
	    export.vtkfile_Err << E_radi_BiV_DG
	    export.vtkfile_Efiber << Eul_fiber_BiV_DG
	    export.vtkfile_fstress << fstress_DG
            export.vtkfile_IMP << imp
            export.vtkfile_IMP2 << imp2
            export.vtkfile_IMP_Constraint << MEmodel_.GetP()

    	if("probepts" in SimDet.keys()):
		fIMP = probesIMP.array()
		fIMP2 = probesIMP2.array()
		fIMP3 = imp_array
		fStress = probesfstress.array()
		fStrain_vals = probesEul_fiber.array()
		E_circ_BiV = probesE_circ_BiV.array()
		E_long_BiV = probesE_long_BiV.array()
		E_radi_BiV = probesE_radi_BiV.array()

		print fStress

		export.writeIMP(MEmodel_, state_obj.tstep, fIMP)
		export.writeIMP2(MEmodel_, state_obj.tstep, fIMP2)
		export.writeIMP3(MEmodel_, state_obj.tstep, fIMP3)
		export.writefStress(MEmodel_, state_obj.tstep, fStress)
		export.writefStrain(MEmodel_, state_obj.tstep, fStrain_vals)
		export.writeCStrain(MEmodel_, state_obj.tstep, E_circ_BiV)
		export.writeLStrain(MEmodel_, state_obj.tstep, E_long_BiV)
		export.writeRStrain(MEmodel_, state_obj.tstep, E_radi_BiV)

        cnt += 1


#  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - - 
if __name__ == "__main__":

    print 'Testing...'
    run_BiV_TimedGuccione(IODet=IODetails, SimDet=SimDetails)

#  - - - - - - - - - - - -- - - - - - - - - - - - - - - -- - - - - - - 
